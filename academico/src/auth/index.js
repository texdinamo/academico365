export const signup = (user) => {
    return fetch("http://vps-1924366-x.dattaweb.com:8080/api/auth/signup",{
            method: "POST",
            headers: {
                Accept: 'application/json',
                "Content-type": "application/json"
            },
            body: JSON.stringify(user)
        })
        .then(response => {
            return response
        })
        .catch(err => {
            console.log(err)
        })
    }
    
    
    export const signin = (user) => {
        return fetch("http://vps-1924366-x.dattaweb.com:8080/api/auth/signin",{
                method: "POST",
                headers: {
                    Accept: 'application/json',
                    "Content-type": "application/json"
                },
                body: JSON.stringify(user)
            })
            .then(response => {
                console.log(response)
                return response
            })
            .catch(err => {
                console.log(err)
            })
        }



    export const resetPassword = (user) => {
        return fetch("http://vps-1924366-x.dattaweb.com:8080/api/auth/recovery",{
                method: "POST",
                headers: {
                    Accept: 'application/json',
                    "Content-type": "application/json"
                },
                body: JSON.stringify(user)
            })
            .then(response => {
                console.log(response)
                return response
            })
            .catch(err => {
                console.log(err)
            })
        }

        export const authenticate = (data,next) => {
            if(typeof window !== 'undefined'){
                localStorage.setItem('jwt',JSON.stringify(data))
                next()
            }
        
        
        }

        