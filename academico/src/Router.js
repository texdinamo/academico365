import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import "./styles/app.scss";
import Signin from "../src/components/user/Signin";
import Signup from "../src/components/user/Signup";
import Escritorio from '../src/components/Escritorio/Escritorio';
import Alumnos from '../src/components/Alumnos/Alumnos';
import Alumno from '../src/components/Alumnos/Alumno';
import Profesores from '../src/components/Profesores/Profesores';
import Profesor from '../src/components/Profesores/Profesor';
import Tutores from '../src/components/Tutores/Tutores';
import Tutor from '../src/components/Tutores/Tutor';
import Curso from "../src/components/Cursos/Curso";
import Cursos from "../src/components/Cursos/Cursos";
import Materias from "../src/components/Materias/Materias";
import Materia from '../src/components/Materias/Materia';
import PasswordReset from "../src/components/PasswordReset";
import Slidebar from '../src/components/estructura/Slidebar';
import Planes from '../src/components/administracion/Planes';
import Plan from '../src/components/administracion/Plan';
import User from '../src/components/user/User';
import Footer from '../src/components/Footer';
import Cuotas from '../src/components/administracion/Cuotas';
import MediosPagos from'../src/components/MediosDePagos/MediosPagos';
import MedioPago from '../src/components/MediosDePagos/MedioPago';
import PuntosVentas from '../src/components/PuntosDeVentas/PuntosVentas';
import PuntoVenta from '../src/components/PuntosDeVentas/PuntoVenta';
import CuentasContables from '../src/components/CuentasContables/CuentasContables';
import CuentaContable from '../src/components/CuentasContables/CuentaContable';
import CentrosCostos from '../src/components/CentroDeCostos/CentrosCostos';
import CentroCosto from '../src/components/CentroDeCostos/CentroCosto';
import Proveedores from "../src/components/Proveedores/Proveedores";
import Proveedor from "../src/components/Proveedores/Proveedor";
import GenerarCuotas from '../src/components/administracion/GenerarCuotas';

const Router = (props) => {
  return (
    <>
      <BrowserRouter>
        <Slidebar />
        <Switch>
          <Route path="/crear-cuenta" exact component={Signup} />
          <Route path="/reset-password" exact component={PasswordReset} />
          <Route path="/" exact component={Signin} />
          <Route path="/perfil" exact component={User} />
          <Route path="/administracion/planes" exact component={Planes} />
          <Route path="/administracion/planes/plan" exact component={Plan} />
          <Route path="/administracion/cuotas" exact component={Cuotas} />
          <Route path="/administracion/generar-cuotas" exact component={GenerarCuotas} />
          <Route path="/dashboard" exact component={Escritorio} />
          <Route path="/alumnos" exact component={Alumnos} />
          <Route path="/alumnos/alumno" exact component={Alumno} />
          <Route path="/docentes" exact component={Profesores} />
          <Route path="/docentes/docente" exact component={Profesor} />
          <Route path="/tutores" exact component={Tutores} />
          <Route path="/tutores/tutor" exact component={Tutor} />
          <Route path="/cursos" exact component={Cursos} />
          <Route path="/cursos/curso" exact component={Curso} />
          <Route path="/materias" exact component={Materias} />
          <Route path="/materias/materia" exact component={Materia} />
          <Route path="/administracion/medios-pagos" exact component={MediosPagos} />
          <Route path="/administracion/medios-pagos/medio-pago" exact component={MedioPago} />
          <Route path="/administracion/puntos-ventas" exact component={PuntosVentas} />
          <Route path="/administracion/puntos-ventas/punto-venta" exact component={PuntoVenta} />
          <Route path="/administracion/cuentas-contables" exact component={CuentasContables} />
          <Route path="/administracion/cuentas-contables/cuenta-contable" exact component={CuentaContable} />
          <Route path="/administracion/centros-costos" exact component={CentrosCostos} />
          <Route path="/administracion/centros-costos/centro-costo" exact component={CentroCosto} />
          <Route path="/administracion/proveedores" exact component={Proveedores} />
          <Route path="/administracion/proveedores/proveedor" exact component={Proveedor} />     
                             
        </Switch>
        <Footer />
      </BrowserRouter>
    </>
  );
};

export default Router;
