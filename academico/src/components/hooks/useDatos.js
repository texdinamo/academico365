import { useState } from "react";

export function useDatos() {
  const [datos, setDatos] = useState(false);
  return {
    datos,
    setDatos
  };
}
