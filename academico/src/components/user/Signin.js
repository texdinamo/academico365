import React, { useEffect, useState } from "react";
import { Link , Redirect , useHistory} from "react-router-dom";
import { Spinner } from 'react-bootstrap'
import logo from "../../img/logo aca365.svg";
// import { API } from "../../config";
import { signin, authenticate } from "../../auth";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

const Signin = (props) => {


  let history = useHistory()



  const [values, setValues] = useState({
    email: "",
    password: "",
    error: "",
    loading: false,
    redireccionar: false,
  });

  const [validateEmail, setValidateEmail] = useState(false);

  const [redireccion,setRedireccion] = useState(false);
  const [spinner,setSpinner] = useState(false)

  const [validatePassword, setValidatePassword] = useState(false);

  const { email, password } = values;

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });

    if (values.email === "") {
      setValidateEmail(false);
    }
    if (values.password === "") {
      setValidatePassword(false);
    }
  };

  const clickSubmit = (event) => {
    event.preventDefault();


    if (!values.email) {
      setValidateEmail(true);
    }

    if (!values.password) {
      setValidatePassword(true);
    }

    if (values.email && values.password) {

      setSpinner(true)
      setValues({ ...values, error: false, loading: true });
      // console.log('aca van los values',values)

      let val = {
        "username": values.email,
        "password": values.password
        }

        console.log('dddd',val)

      signin(val).then((data) => {
        console.log("signinn respuesta", data);

        if(data.ok === true){

          setSpinner(false)
          toast.success('Sesion iniciada')

          setValues({
                      ...values,
                      email: '',
                      password: ''
                      })
      

        // history.push('/dashboard')
        history.push({
          pathname: '/dashboard',
          state: { dashboard: true }

        });
    
         setRedireccion(true)

        }

        if(data.ok === false){

          setSpinner(false)
          toast.error("los datos son incorrecto")
        }

      });
    }
  };

  useEffect(() => {


    console.log(props.location.state)

  if(props.location.state !== undefined){

    if(props.location.state.mensaje === true){

      toast.success('Usuario creado inicie sesion')
    
      history.push({
        state: { mensaje: false }
  
      });
    
    
    }

    if(props.location.state.sendEmail === true){
      toast.success(`Email enviado! verifique su correo`)
      history.push({
        state: { sendEmail: false }
  
      });
    

    
    }


    
    
    console.log(props.location.state)

  }


  }, []);

  return (
    <>
    <ToastContainer/>
    <div className="login  d-flex ">
      <div className="col-4  login__form ">
        <img src={logo} />

        <form className="login__form--form">
          <label>Usuario</label>
          <input
            className={
              validateEmail
                ? "input form-control is-invalid"
                : " input form-control"
            }
            onChange={handleChange("email")}
            value={email}
            placeholder="Ej: miusuario"
          />
          <label>Contrasena</label>
          <input
            className={
              validatePassword
                ? "input form-control is-invalid"
                : " input form-control"
            }
            onChange={handleChange("password")}
            value={password}
            type="password"
            placeholder="Contrasena"
          />
        </form>

        <button onClick={clickSubmit} className={values.email && values.password  ? "boton boton--login  " :  "boton boton--login btn-opacity "} disabled={spinner} >
          
          {spinner ? (

<Spinner animation="border" variant="light" className="h6 mt-1" />


          ) : "Ingresar" }
  
        </button>


        <button className="boton boton--google">Ingresar con Google</button>

        <button className="boton boton--facebook">Ingresar con Facebook</button>

        <Link to="/reset-password" className="link">
          Olvidé mi contraseña
        </Link>

        <div className="login__form--register">
          <p>No Tienes Cuenta?</p>
          <Link to="/crear-cuenta">
            <button className="boton-cuenta">Crear cuenta</button>
          </Link>
        </div>
      </div>
      <div className="col-8 login__img d-flex justify-content-center">
        <div className="login__img--content">
          <h3 className="title">
            Sistema premium <br /> de Educación
          </h3>
          <p className="text">
            Es un producto de Académico365 destinado a brindarte la tecnología
            que necesitas para tener tu propia academia online de una forma tan
            sencilla que no lo podrás creer. Hace realmente simple el
            administrar, vender y cobrar tus cursos y te libera tiempo.
          </p>
        </div>
      </div>
    </div>
    </>
  );
};

export default Signin;
