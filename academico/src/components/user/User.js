import React, { useState, useEffect } from 'react';
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import Moment from 'react-moment'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Spinner } from 'react-bootstrap'
import 'react-tabs/style/react-tabs.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import { URL } from '../../valores'
import {
  AgregarFormularioAlumno, ButtonCrear, ButtonCancelar,
  TextFormulario, TextInfo, ContainerMenu, SubTextInfo, Linea
} from '../styled-component/Navbar'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import { useDatos } from '../hooks/useDatos'
import styled from 'styled-components'
import { Container3 } from '../styled-component/slidebar'
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { ToastContainer, toast } from 'react-toastify'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import 'react-toastify/dist/ReactToastify.min.css'
import moment from 'moment'
import logo from "../../img/user.svg";


const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ButtonAgregar = styled.button`

width: 143px;
height: 42px;
border-radius: 6px;
background-color: #06adff;
font-size: 11px;
line-height: 15px;
color: #ffffff;
font-weight: 900;
font-family: "Lato";
cursor: pointer;
align-self: center;
`
const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`;


const User = (props) => {
  const [alumno, setAlumno] = useState();

  const [alumnoId, setAlumnoId] = useState();

  const [values, setValues] = useState();
  const [toggleModificar,setModificar] = useState(false)


  const changeToggleModificar = () => setModificar(prevLogin => !prevLogin)


  const handleChange = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value });
  };

  useEffect(() => {
      
    axios
      .get(
        `${process.env.REACT_APP_STUDENT_URL}/1}`
      )
      .then((res) => {
        //// aca esta la respuesta en la consola del navegador
        console.log("respuesta de la endpoint!!", res);
        setAlumno(res.data);
      });


      

  }, []);

  console.log("allumno", alumno);


  const clickSubmit = (event) => {
    event.preventDefault();

    toast.success("Usuario Modificado");
    changeToggleModificar()
  };


  return (


    <>
    <Navbar datos={ alumno ? `${alumno.name} ${alumno.surname}`  : 'User'  } />
    <div className="dashboard">

<ToastContainer/>



{toggleModificar && <AgregarShadow onClick={changeToggleModificar} /> } 

{
  toggleModificar && 

<>
<AgregarFormularioAlumno >
<div className="formulario__head">
            <div>
              <TextFormulario>Modificar usuario</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggleModificar}>
              <i class="fas fa-times"></i>
            </div>
          </div>

          <div className="formulario__body ">
            <div>
              <form className="input-form">
                <p className="mt-2 subtitulo">Datos personales</p>
                <Linea />
                <label>Nombre</label>
                <input
                  onChange={handleChange}
                  defaultValue={alumno.name}
                  type="text"
                  className="input"
                  placeholder="Nombre"
                />
                <label>Apellido</label>
                <input
                  onChange={handleChange("surname")}
                  defaultValue={alumno.surname}
                  type="text"
                  className="input"
                  placeholder="Apellido"
                />
                <label>Dni</label>
                <input
                  onChange={handleChange("document")}
                  defaultValue={alumno.document}
                  type="number"
                  className="input"
                  placeholder="Dni"
                />
                <label>Fecha de nacimiento</label>
                <input
                  onChange={handleChange("birthDate")}
                  defaultValue={alumno.birthDate}
                  type="date"
                  className="input"
                  placeholder="Cuil"
                />

                <label>Sexo</label>
                <div className="radio d-flex justify-content-center">
                  <label>Masculino</label>
                  <input
                    type="radio"
                    checked={alumno.gender === "masculino"}
                    name="gender"
                    value="masculino"
                    onChange={handleChange("gender")}
                  />

                  <label>Femenino</label>

                  <input
                    type="radio"
                    checked={alumno.gender === "femenino"}
                    name="gender"
                    value="femenino"
                    onChange={handleChange("gender")}
                  />
                </div>

                <label>Nacionalidad</label>
                <input
                  onChange={handleChange("nationality")}
                  defaultValue={alumno.nationality}
                  type="text"
                  className="input"
                  placeholder="Apellido"
                />

                <label className="mb-3">Tipo de sangre</label>
                <Select
                  name="bloodtype"
                  defaultValue={alumno.bloodtype}
                  onChange={handleChange("bloodtype")}
                >
                  <option value="">-- Seleccione --</option>
                  <option value="No sabe">NO SABE</option>
                  <option value="O NEGATIVO">O NEGATIVO</option>
                  <option value="O POSITIVO"> O POSITIVO</option>

                  <option value="AB NEGATIVO">AB NEGATIVO</option>
                  <option value="AB POSITIVO">AB POSITIVO</option>
                </Select>

                <p className="mt-4 subtitulo">Contactos</p>
                <Linea />

                <label>Domicilio</label>
                <input
                  onChange={handleChange("address")}
                  defaultValue={alumno.address}
                  type="text"
                  className="input"
                  placeholder="Domicilio"
                />

                <label>Barrio</label>
                <input
                  onChange={handleChange("neighborhood")}
                  defaultValue={alumno.neighborhood}
                  type="text"
                  className="input"
                  placeholder="Barrio"
                />

                <label>Ciudad</label>
                <input
                  onChange={handleChange("city")}
                  defaultValue={alumno.city}
                  type="text"
                  className="input"
                  placeholder="Ciudad"
                />

                <label className="mb-3">Pais</label>
                <Select
                  name="country"
                  defaultValue={alumno.country}
                  onChange={handleChange("country")}
                >
                  <option value="">-- Seleccione pais--</option>
                  <option value="argentina">Argentina</option>
                  <option value="bolivia">Bolivia</option>
                  <option value="chile">Chile</option>
                  <option value="peru">Perú</option>
                  <option value="venezuela">Venezuela</option>
                </Select>

                <label>Provincia</label>
                <input
                  onChange={handleChange("province")}
                  defaultValue={alumno.province}
                  type="text"
                  className="input"
                  placeholder="Provincia"
                />

                <label>Codigo postal</label>
                <input
                  onChange={handleChange("zipCode")}
                  defaultValue={alumno.zipCode}
                  type="text"
                  className="input"
                  placeholder="Cod postal"
                />

                <label>Telefono</label>
                <input
                  onChange={handleChange("phoneNumber")}
                  defaultValue={alumno.phoneNumber}
                  type="number"
                  className="input"
                  placeholder="Telefono"
                />

                <label>Email</label>
                <input
                  onChange={handleChange("email")}
                  defaultValue={alumno.email}
                  type="email"
                  className="input"
                  placeholder="Email"
                />

                <p className="mt-4 subtitulo">Escolares</p>
                <Linea />

                <label>Legajo</label>
                <input
                  onChange={handleChange("docket")}
                  defaultValue={alumno.docket}
                  type="text"
                  className="input"
                  placeholder="Legajo"
                />

                <label className="mb-3">Nivel de estudio</label>
                <Select
                  name="studyLevel"
                  defaultValue={alumno.studyLevel}
                  onChange={handleChange("studyLevel")}
                >
                  <option value="">-- Seleccione nivel --</option>
                  <option value="primario">Primario</option>
                  <option value="secundario">Secundario</option>

                  <option value="universitario">Universitario</option>
                </Select>

                <p className="mt-4 subtitulo">Facturacion</p>
                <Linea />

                <label>Domicilio de facturacion</label>
                <input
                  onChange={handleChange("billingAddress")}
                  defaultValue={alumno.billingAddress}
                  type="text"
                  className="input"
                  placeholder="Domicilio"
                />

                <label>Ciudad de facturacion</label>
                <input
                  onChange={handleChange("billingCity")}
                  defaultValue={alumno.billingCity}
                  type="text"
                  className="input"
                  placeholder="Ciudad"
                />

                <label>Provincia de facturacion</label>
                <input
                  onChange={handleChange("billingProvince")}
                  defaultValue={alumno.billingProvince}
                  type="text"
                  className="input"
                  placeholder="Provincia"
                />

                <label>Codigo postal</label>
                <input
                  onChange={handleChange("billingZip")}
                  defaultValue={alumno.billingZip}
                  type="text"
                  className="input"
                  placeholder="Cod postal"
                />
              </form>
            </div>

            <div className="formulario__footer">
              <ButtonCancelar onClick={changeToggleModificar}>Cancelar</ButtonCancelar>

              <ButtonCrear onClick={clickSubmit} >Modificar</ButtonCrear>
            </div>
          </div>
        </AgregarFormularioAlumno> 
 </>

}
 

      {alumno ? (
        <Tabs>
          <TabList>
            <Tab>Informacion Personal</Tab>

          </TabList>
          <TabPanel className="alumno">

          <div className="header-descripcion d-flex justify-content-between">
  <p>Informacion personal</p>

  <ButtonAgregar onClick={changeToggleModificar} >Modificar datos</ButtonAgregar>

</div>

            <div className="datos-usuario d-flex">


<div className="userInfo__img ml-4 mr-5">
                <img src={logo} />
              </div>
              <div className="datos">
                <div className="d-flex">
                  <h5 className="info-descripcion">Legajo:</h5>{" "}
                  <p>{alumno.docket}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Nombre:</h5>
                  <p>{alumno.name}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Apellido:</h5>
                  <p>{alumno.surname}</p>
                </div>

                <div className="d-flex mb-3">
                  <h5 className="info-descripcion">Dni:</h5>
                  <p>{alumno.document}</p>
                </div>

                <div className="d-flex mb-3">
                  <h5 className="info-descripcion">Fecha de nacimiento:</h5>

                  <Moment format="YYYY/MM/DD">
                    <p>{alumno.birthDate}</p>
                  </Moment>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Sexo:</h5>
                  <p>{alumno.gender}</p>
                </div>
              </div>

              <div className="datos ml-4">
                <div className="d-flex">
                  <h5 className="info-descripcion mb-3">Nacionalidad:</h5>
                  <p>{alumno.nationality}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion mb-3">Tipo de sangre:</h5>
                  <p>{alumno.bloodtype}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Domiciolio:</h5>
                  <p>{alumno.address}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Barrio:</h5>
                  <p>{alumno.neighborhood}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Ciudad:</h5>
                  <p>{alumno.city}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Pais:</h5>
                  <p>{alumno.country}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Barrio:</h5>
                  <p>{alumno.neighborhood}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Provincia:</h5>
                  <p>{alumno.province}</p>
                </div>
              </div>

              <div className="datos ml-4">
                <div className="d-flex mb-3">
                  <h5 className="info-descripcion">Codigo postal:</h5>
                  <p>{alumno.zipCode}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">email:</h5>
                  <p>{alumno.email}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Telefono:</h5>
                  <p>{alumno.phoneNumber}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Nivel de estudio:</h5>
                  <p>{alumno.studyLevel}</p>
                </div>
              </div>

              <div></div>
            </div>
          </TabPanel>

          
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>
      )}
      </div>
    </>
  );
};

export default withRouter(User);
