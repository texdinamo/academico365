import React, { useEffect, useState } from "react";
import { Link , useHistory } from "react-router-dom";
import logo from "../../img/logo aca365.svg";
import { API } from "../../config";
import styled from "styled-components";
import { signup, authenticate } from "../../auth";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { Spinner } from 'react-bootstrap'


const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const Signup = () => {

  let history = useHistory();

  const [values, setValues] = useState({
    username: "",
    email: "",
    password: "",
    role: "",
    error: "",
    success: false,
  });

  const [validateUsername, setValidateUsername] = useState(false);
  const [validateEmail, setValidateEmail] = useState(false);
  const [validatePassword, setValidatePassword] = useState(false);
  const [validateRole, setValidateRole] = useState(false);
  const [spinner,setSpinner] = useState(false)


  const { username, email, role, password, error, success } = values;

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });

    if (values.username === "") {
      setValidateUsername(false);
    }
    if (values.email === "") {
      setValidateEmail(false);
    }
    if (values.password === "") {
      setValidatePassword(false);
    }
    if (values.role === "") {
      setValidateRole(false);
    }
  };

  let arrRole = [];

  const clickSubmit = (event) => {
    event.preventDefault();
    
    if (!values.username) {
      setValidateUsername(true);
    }
    if (!values.email) {
      setValidateEmail(true);
    }
    if (!values.password) {
      setValidatePassword(true);
    }
    if (!values.role) {
      setValidateRole(true);
    }

    if (values.username && values.email && values.password) {
      setSpinner(true)
      setValues({ ...values, error: false });

      let val = {
        "username": values.username,
        "password": values.password,
        "email": values.password
        }
      signup(val).then((data) => {
        console.log("respuesta signup", data);

        if(data.ok === true){

          setSpinner(false)
            toast.success('Usuario creado! inicie sesion')

            setValues({
                        ...values,
                        username: '',
                        email: '',
                        password: '',
                        role: '',
                       
                        })

            
                        history.push({
                          pathname: '/',
                          state: { mensaje: true }
                
                        });

        }
        if(data.ok === false){

          setSpinner(false)
          toast.error('Error al registrar usuario')
        }

        
      });
    }
  };

  useEffect(() => {}, []);

  return (
    <>
     <ToastContainer />

    <div className="login  d-flex ">
      <div className="col-4  login__form ">
      <Link to="/">
      <i class="fas fa-arrow-left"></i>
      </Link>
        <img src={logo} />

        <form className="login__form--form">
          <label>username</label>
          <input
            className={
              validateUsername
                ? "input form-control is-invalid"
                : " input form-control"
            }
            onChange={handleChange("username")}
            value={username}
            type="text"
            placeholder="username"
          />

          <label>Email</label>
          <input
            className={
              validateEmail
                ? "input form-control is-invalid"
                : " input form-control"
            }
            onChange={handleChange("email")}
            value={email}
            placeholder="Ej: email@email.com"
          />

          <label>Tipo de usuario</label>
          <Select
            name="role"
            onChange={handleChange("role")}
            value={role}
            className={
              validateRole
                ? "mb-3 form-control is-invalid"
                : " mb-3 form-control"
            }
          >
            <option value="">-- Seleccione --</option>
            <option value="user">user</option>
            <option value="mod">mod</option>
            <option value="admin">admin</option>
          </Select>

          <label>Contrasena</label>
          <input
            className={
              validatePassword
                ? "input form-control is-invalid"
                : " input form-control"
            }
            onChange={handleChange("password")}
            value={password}
            placeholder="Contrasena"
            type="password"
          />
        </form>

        <button onClick={clickSubmit} className={values.username && values.email && values.password && values.role ? "boton boton--login  " :  "boton boton--login btn-opacity"}  disabled={spinner}>

        {spinner ? (

<Spinner animation="border" variant="light" className="h6 mt-1" />


          ) : "Crear cuenta" }
  

        </button>

        <div className="login__form--register">
          <p>Tienes una Cuenta?</p>

          <Link to="/">
            <button className="boton-cuenta">Iniciar sesion</button>
          </Link>
        </div>
      </div>
      <div className="col-8 login__img d-flex justify-content-center">
        <div className="login__img--content">
          <h3 className="title">
            Sistema premium <br /> de Educación
          </h3>
          <p className="text">
            Es un producto de Académico365 destinado a brindarte la tecnología
            que necesitas para tener tu propia academia online de una forma tan
            sencilla que no lo podrás creer. Hace realmente simple el
            administrar, vender y cobrar tus cursos y te libera tiempo.
          </p>
        </div>
      </div>
    </div>
    </>
  );
};

export default Signup;
