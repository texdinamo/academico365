export const create = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/salesPoint/create`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const updatePuntoDeVenta = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/salesPoint/update`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const deletePuntoDeVenta = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/salesPoint/remove`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}

export const getPuntosDeVentas = () => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/salesPoint`, {
        method: "GET"
    })
        .then(response => {            
            return response.json()
        })
        .catch(err => console.log(err))
}

export const getPuntoDeVentaId = (id) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/salesPoint/${id}`, {
        method: "GET"
    })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .catch(err => console.log(err))
}