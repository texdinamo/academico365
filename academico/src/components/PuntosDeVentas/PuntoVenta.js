import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import Moment from "react-moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
import { URL } from "../../valores";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { useDatos } from "../hooks/useDatos";
import { getPuntoDeVentaId } from './apiPuntosDeVentas'
import styled from "styled-components";

const ButtonAgregar = styled.button`
  width: 143px;
  height: 42px;
  border-radius: 6px;
  background-color:  #009DDC;
  font-size: 11px;
  line-height: 15px;
  color: #ffffff;
  font-weight: 900;
  font-family: "Lato";
  cursor: pointer;
  align-self: center;
`;

const PuntoVenta = (props) => {
const [puntoVenta, setPuntoVenta] = useState();

  useEffect(() => {
    getPuntoDeVentaId(props.location.state.fromDashboard).then(data => {     
     setPuntoVenta(data)
    })
  }, []);
    
  return (
    <>
    <Navbar/>
    <div className="dashboard">
      {puntoVenta ? (
        <Tabs>
          <TabList>
            <Tab>Información</Tab>           
          </TabList>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Información</div>
            <div className="datos-usuario d-flex">
              <div className="datos">
                <div className="d-flex">
                  <h5 className="info-descripcion">Nombre del Punto de Venta:</h5>
                  <p>{puntoVenta.name}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Punto de Venta Fiscal:</h5>
                  <p>{puntoVenta.fiscal === false && "NO"}</p>
                  <p>{puntoVenta.fiscal === true && "SI"}</p>
                </div>                
                <div className="d-flex">
                  <h5 className="info-descripcion">Razón Social:</h5>
                  <p>{puntoVenta.issuerName}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Dirección:</h5>
                  <p>{puntoVenta.issuerAddress}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Ciudad:</h5>
                  <p>{puntoVenta.city}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Provincia:</h5>
                  <p>{puntoVenta.province}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Código Postal:</h5>
                  <p>{puntoVenta.zipCode}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Tipo de Responsable:</h5>
                  <p>{puntoVenta.afipType === "1" && "Responsable Inscripto"}</p>
                  <p>{puntoVenta.afipType === "4" && "Exento"}</p>
                  <p>{puntoVenta.afipType === "5" && "Consumidor Final"}</p>
                  <p>{puntoVenta.afipType === "6" && "Responsable Monotributo"}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Nro. CUIT:</h5>
                  <p>{puntoVenta.afipIdCompany}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Nro. Ingresos Brutos:</h5>
                  <p>{puntoVenta.ibbNumber}</p>
                </div>       
                <div className="d-flex">
                  <h5 className="info-descripcion">Fecha de Inicio de Actividades:</h5>                  
                    <p>{puntoVenta.startDate}</p>                  
                </div>
              </div>              
            </div>
          </TabPanel>          
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>
      )}
      </div>
    </>
  );
};

export default withRouter(PuntoVenta);