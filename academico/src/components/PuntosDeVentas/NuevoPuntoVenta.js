import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { create } from './apiPuntosDeVentas'
import moment from 'moment'


import {
    AgregarShadow,
    AgregarFormulario,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    Select,
    Linea,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    AgregarFormularioAlumno,
  } from "../styled-component/Navbar";

const NuevoPuntoVenta = ({changeToggle}) => { 
   
    const [validateAfipIdCompany, setValidateAfipIdCompany] = useState(false);
    const [validateAfipSalePointNumber, setValidateAfipSalePointNumber] = useState(false);
    const [validateAfipType, setValidateAfipType] = useState(false);
    const [validateCity, setValidateCity] = useState(false);
    const [validateFiscal, setValidateFiscal] = useState(false);  
    const [validateIbbNumber, setValidateIbbnumber] = useState(false);
    const [validateIssuerAddress, setValidateIssuerAddress] = useState(false);
    const [validateIssuerName, setValidateIssuerName] = useState(false);
    const [validateName, setValidateName] = useState(false);
    const [validateProvince, setValidateProvince] = useState(false);
    const [validateStartDate, setValidateStartDate] = useState(false);
    const [validateZipCode, setValidateZipCode] = useState(false);     
    const [puntoVenta, setPuntoVenta] = useState({               
        afipIdCompany: "",        
        afipSalePointNumber: "",
        afipType: "",        
        city: "",
        fiscal: "",
        ibbNumber:"",        
        issuerAddress:"",
        issuerName:"",
        name:"",
        province:"",
        startDate: "",
        zipCode:""   
    });

    const handleChangePuntoVenta = (name) => (event) => {
        setPuntoVenta({ ...puntoVenta, [name]: event.target.value });    
                
        if (name === "afipIdDocument") {
            setValidateAfipIdCompany(false);
        }        
        if (name === "afipSalePointNumber") {
            setValidateAfipSalePointNumber(false);
        }
        if (name === "afipTye") {
            setValidateAfipType(false);
        }
        if (name === "city") {
            setValidateCity(false);
        }
        if (name === "fiscal") {
            setValidateFiscal(false);            
        }
        if (name === "ibbNumber") {
            setValidateIbbnumber(false);
        }
        if (name === "issuerAddress") {
            setValidateIssuerAddress(false);
        }
        if (name === "issuerName") {
            setValidateIssuerName(false);
        }
        if (name === "name") {
            setValidateName(false);
        }
        if (name === "province") {
            setValidateProvince(false);
        }
        if (name === "startDate") {
          setValidateStartDate(false);         
        }
        if (name === "zipCode") {
            setValidateZipCode(false);
        }     
      };

    const clickSubmitPuntoVenta = (event) => {
        event.preventDefault(event);    
        
        if (!puntoVenta.afipIdCompany) {
            setValidateAfipIdCompany(true);
        }
        if (!puntoVenta.afipSalePointNumber) {
            setValidateAfipSalePointNumber(true);
        }
        if (!puntoVenta.afipType) {
            setValidateAfipType(true);
        }
        if (!puntoVenta.city) {
            setValidateCity(true);
        }
        if (!puntoVenta.fiscal) {
            setValidateFiscal(true);
        }
        if (!puntoVenta.ibbNumber) {
            setValidateIbbnumber(true);
        }
        if (!puntoVenta.issuerAddress) {
            setValidateIssuerAddress(true);
        }
        if (!puntoVenta.issuerName) {
            setValidateIssuerName(true);
        }
        if (!puntoVenta.name) {
            setValidateName(true);
        }
        if (!puntoVenta.province) {
            setValidateProvince(true);
        }
        if (!puntoVenta.startDate) {
            setValidateStartDate(true);
        }
        if (!puntoVenta.zipCode ) {
            setValidateCity(true);
        }
        if (
          puntoVenta.afipIdCompany &&
          puntoVenta.afipSalePointNumber &&
          puntoVenta.afipType &&
          puntoVenta.city &&
          puntoVenta.fiscal &&
          puntoVenta.ibbNumber &&
          puntoVenta.issuerAddress &&
          puntoVenta.issuerName &&
          puntoVenta.name &&
          puntoVenta.province &&
          puntoVenta.startDate &&
          puntoVenta.zipCode                     
        ) {       

          const val =  {           
          "idInstitution": 1,
          "afipDocument": "",
          "afipDocumentType" : 80, 
          "afipIdCompany":  puntoVenta.afipIdCompany,
          "afipSalePointNumber":  puntoVenta.afipSalePointNumber,
          "afipType": puntoVenta.afipType,
          "city": puntoVenta.city,
          "fiscal": puntoVenta.fiscal, 
          "ibbNumber":  puntoVenta.ibbNumber,
          "issuerAddress":  puntoVenta.issuerAddress,
          "issuerName": puntoVenta.issuerName,
          "name": puntoVenta.name,
          "province": puntoVenta.province,
          "startDate": puntoVenta.startDate,
          "zipCode": puntoVenta.zipCode,
          "apiToken": ""
          }          
          console.log(val)
          create(val)
          .then(data => {  
            if(data){              
              if(data.ok === true){    
                toast.success("Punto de Venta creada");
                changeToggle();
              }
              if(data.ok === false){                
                toast.error("Error al crear");
                changeToggle();
              }    
            }else{
              toast.error("Error en la api");
              changeToggle();            
            }           
          })
        }
      };    
    
    return ( 
        <>
                <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Crear un nuevo Punto de Venta</TextFormulario>
            </div>
            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre del punto de venta</label>
              <input
                onChange={handleChangePuntoVenta("name")}
                value={puntoVenta.name}
                type="text"
                className={
                  validateName ? "form-control is-invalid" : "form-control"
                }
                placeholder="Nombre del punto de venta"
              />
              {validateName && (
                <p className="text-danger my-1">El nombre es obligatorio</p>
              )}             
              <label className="mb-3">Punto de Venta Fiscal</label>
              <Select
                  className={
                  validateFiscal ? "form-control is-invalid" : "form-control"
                }
                  value={puntoVenta.fiscal}
                  onChange={handleChangePuntoVenta("fiscal")}
              >                 
                  <option value="">-- Seleccione Estado --</option>                  
                  <option value={true}>SI</option>
                  <option value={false}>NO</option>
                </Select>
                {validateFiscal && (
                <p className="text-danger my-1">El punto de venta fiscal es obligatorio</p>
              )}
              <label>Razón Social</label>
              <input
                onChange={handleChangePuntoVenta("issuerName")}
                value={puntoVenta.issuerName}
                type="text"
                className={
                  validateIssuerName ? "form-control is-invalid" : "form-control"
                }
                placeholder="Razón social"
              />
              {validateIssuerName && (
                <p className="text-danger my-1">La razón social es obligatorio</p>
              )}
              <label>Dirección</label>
              <input
                onChange={handleChangePuntoVenta("issuerAddress")}
                value={puntoVenta.issuerAddress}
                type="text"
                className={
                  validateIssuerAddress ? "form-control is-invalid" : "form-control"
                }
                placeholder="Calle y Número"
              />
              {validateIssuerAddress && (
                <p className="text-danger my-1">La dirección es obligatorio</p>
              )}
              <label>Ciudad</label>
              <input
                onChange={handleChangePuntoVenta("city")}
                value={puntoVenta.city}
                type="text"
                className={
                  validateCity ? "form-control is-invalid" : "form-control"
                }
                placeholder="Ciudad"
              />
              {validateCity && (
                <p className="text-danger my-1">La ciudad es obligatorio</p>
              )}
              <label>Provincia</label>
              <input
                onChange={handleChangePuntoVenta("province")}
                value={puntoVenta.province}
                type="text"
                className={
                  validateProvince ? "form-control is-invalid" : "form-control"
                }
                placeholder="Ciudad"
              />
              {validateProvince && (
                <p className="text-danger my-1">La provincia es obligatorio</p>
              )}
              <label>Código Postal</label>
              <input
                onChange={handleChangePuntoVenta("zipCode")}
                value={puntoVenta.zipCode}
                type="text"
                className={
                  validateZipCode ? "form-control is-invalid" : "form-control"
                }
                placeholder="Código Postal"
              />
              {validateZipCode && (
                <p className="text-danger my-1">El código postal es obligatorio</p>
              )}
              <label className="mb-3">Tipo de responsable</label>
              <Select
                  className={
                  validateAfipType ? "form-control is-invalid" : "form-control"
                }
                  value={puntoVenta.afipType}
                  onChange={handleChangePuntoVenta("afipType")}
              >                  
                  <option value="">-- Seleccione el tipo de responsable --</option>
                  <option value="1">Responsable Inscripto</option>
                  <option value="6">Responsable Monotributo</option>
                  <option value="4">Exento</option>
                  <option value="5">Consumidor Final</option>
                </Select>
                {validateAfipType && (
                <p className="text-danger my-1">El tipo de plan es obligatorio</p>
              )}
            <label>Número de  CUIT</label>
              <input
                onChange={handleChangePuntoVenta("afipIdCompany")}
                value={puntoVenta.afipIdCompany}
                type="number"
                className={
                  validateAfipIdCompany ? "form-control is-invalid" : "form-control"
                }
                placeholder="CUIT"
              />
              {validateAfipIdCompany && (
                <p className="text-danger my-1">El CUIT es obligatorio</p>
              )}
            <label>Número de Ingresos Brutos</label>
              <input
                onChange={handleChangePuntoVenta("ibbNumber")}
                value={puntoVenta.ibbNumber}
                type="number"
                className={
                  validateIbbNumber ? "form-control is-invalid" : "form-control"
                }
                placeholder="Nro de Ingresos Brutos"
              />
              {validateIbbNumber && (
                <p className="text-danger my-1">El codigo postal es obligatorio</p>
              )}
              <label>Número de Punto de Venta en AFIP</label>
              <input
                onChange={handleChangePuntoVenta("afipSalePointNumber")}
                value={puntoVenta.afipSalePointNumber}
                type="number"
                className={
                  validateAfipSalePointNumber ? "form-control is-invalid" : "form-control"
                }
                placeholder="Nro de punto de venta AFIP"
              />
              {validateAfipSalePointNumber && (
                <p className="text-danger my-1">Este campo es obligatorio</p>
              )}
              <label>Fecha de Inicio de Actividades</label>
              <input
                onChange={handleChangePuntoVenta("startDate")}
                value={puntoVenta.startDate}
                type="date"
                className={
                  validateStartDate
                    ? "form-control is-invalid"
                    : "form-control"
                }
                placeholder="Fecha de Inicio de las actividades"
              />
              {validateStartDate && (
                <p className="text-danger my-1">
                  La fecha de Inicio es obligatoria
                </p>
              )}            
              </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmitPuntoVenta}>Crear</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default NuevoPuntoVenta;