import {useState,useEffect} from 'react'
import { updatePuntoDeVenta } from './apiPuntosDeVentas'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import moment from 'moment'
import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'

const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarPuntosVentas = ({changeToggle,puntoVenta,PuntoVentaId}) => {
    const [puntos, setPuntos] = useState({
       
        afipIdCompany: "",        
        afipSalePointNumber: "",
        afipType: "",        
        city: "",
        fiscal: "",
        ibbNumber:"",        
        issuerAddress:"",
        issuerName:"",
        name:"",
        province:"",
        startDate: "",
        zipCode:"",
        id:"",
        idInstitution:""  
      });

  useEffect(()=>{

    if(puntoVenta){    

      setPuntos({
        afipIdCompany: puntoVenta.afipIdCompany,        
        afipSalePointNumber: puntoVenta.afipSalePointNumber,
        afipType: puntoVenta.afipType,        
        city: puntoVenta.city,
        fiscal: puntoVenta.fiscal,
        ibbNumber:  puntoVenta.ibbNumber,        
        issuerAddress:  puntoVenta.issuerAddress,
        issuerName: puntoVenta.issuerName,
        name: puntoVenta.name,
        province: puntoVenta.province,
        startDate: puntoVenta.startDate,
        zipCode: puntoVenta.zipCode,
        id: puntoVenta.id,
        idInstitution: puntoVenta.idInstitution  
      });    
    }

  },[puntoVenta])

    const clickSubmit = () => {
      const val =  {        
        "id" : puntos.id,
        "idInstitution": puntos.idInstitution,         
        "afipIdCompany":  puntos.afipIdCompany,
        "afipSalePointNumber":  puntos.afipSalePointNumber,
        "afipType": puntos.afipType,
        "city": puntos.city,
        "fiscal": puntos.fiscal, 
        "ibbNumber":  puntos.ibbNumber,
        "issuerAddress":  puntos.issuerAddress,
        "issuerName": puntos.issuerName,
        "name": puntos.name,
        "province": puntos.province,
        "startDate": puntos.startDate,
        "zipCode": puntos.zipCode,       
        }

      updatePuntoDeVenta(val)
      .then(data => {      
        if(data){
        if(data.ok === true){
          toast.success("Punto de Venta modificada");
          changeToggle();
        }
        if(data.ok === false){
          toast.error("Error al modificar");
          changeToggle();
        }
      }
      else{
        toast.error("Problema con la api");
        changeToggle();
      }
      })
    }

    const handleChange = name => e => {
      setPuntos({...puntos, [name]: e.target.value})
  }

    return ( 
        <>
           <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Modificar un Punto de Venta</TextFormulario>
            </div>
            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre del punto de venta</label>
              <input
                onChange={handleChange("name")}
                defaultValue={puntos.name}
                type="text"
                className="input"
                placeholder="Nombre del punto de venta"
              />
              <label className="mb-3">Punto de Venta Fiscal</label>
              <Select
                  className="select"                  
                  onChange={handleChange("fiscal")}
                  defaultValue={puntos.fiscal}  >                   
                  <option value="">-- Seleccione Estado --</option>                  
                  <option value={true}>SI</option>
                  <option value={false}>NO</option>
                </Select>               
              <label>Razón Social</label>
              <input
                onChange={handleChange("issuerName")}
                defaultValue={puntos.issuerName}
                type="text"
                className="input"
                placeholder="Razón Social"
              />
              <label>Dirección</label>
              <input
                onChange={handleChange("issuerAddress")}
                defaultValue={puntos.issuerAddress}
                type="text"
                className="input"
                placeholder="Dirección"
              />
               <label>Provincia</label>
              <input
                onChange={handleChange("province")}
                defaultValue={puntos.province}
                type="text"
                className="input"
                placeholder="Provincia"
              />
                  <label>Ciudad</label>
              <input
                onChange={handleChange("city")}
                defaultValue={puntos.city}
                type="text"
                className="input"
                placeholder="Ciudad"
              />
               <label>Código Postal</label>
              <input
                onChange={handleChange("zipCode")}
                defaultValue={puntos.zipCode}
                type="text"
                className="input"
                placeholder="Código Postal"
              />
              <label className="mb-3">Tipo de responsable</label>
                <Select
                  className="select"                  
                  onChange={handleChange("afipType")}
                  defaultValue={puntos.afipType}  >                   
                  <option value="">-- Seleccione el tipo de responsable --</option>
                  <option value="1">Responsable Inscripto</option>
                  <option value="4">Exento</option>
                  <option value="5">Consumidor Final</option>
                  <option value="6">Responsable Monotributo</option>
                </Select>
              <label>Número de CUIT</label>
              <input
                onChange={handleChange("afipIdCompany")}
                defaultValue={puntos.afipIdCompany}
                type="number"
                className="input"
                placeholder="Nro de CUIT"
              />
              <label>Número de Ingresos Brutos</label>
              <input
                onChange={handleChange("ibbNumber")}
                defaultValue={puntos.ibbNumber}
                type="number"
                className="input"
                placeholder="Nro de Ingresos Brutos"
              />
              <label>Número de Punto de Venta en AFIP</label>
              <input
                onChange={handleChange("afipSalePointNumber")}
                defaultValue={puntos.afipSalePointNumber}
                type="number"
                className="input"
                placeholder="Nro Punto"
              />
              <label>Fecha de Inicio de Actividades</label>
              <input
                onChange={handleChange("startDate")}
                defaultValue={puntos.startDate}
                type="date"
                className="input"
                placeholder="Fecha de inicio de las actividades"
              />      
              </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmit}>Guardar</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>    
        </>
     );
}
 
export default ModificarPuntosVentas;