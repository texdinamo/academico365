import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { create } from './apiCentroCostos'
import moment from 'moment'

import {
    AgregarShadow,
    AgregarFormulario,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    Select,
    Linea,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    AgregarFormularioAlumno,
  } from "../styled-component/Navbar";

const NuevoCentroCosto = ({changeToggle}) => {
    const [validateCenterName, setValidateCenterName] = useState(false);
    const [validateCenterCode, setValidateCenterCode] = useState(false);
    const [validateNote, setValidateNote] = useState(false);    
    const [centro, setCentro] = useState({
        centerName: "",
        centerCode: "",
        note: ""              
      });
      console.log(centro)
    const handleChangeCentroCosto = (name) => (event) => {
        setCentro({ ...centro, [name]: event.target.value });    
        if (name === "centerName") {
          setValidateCenterName(false);
        }    
        if (name === "centerCode") {
          setValidateCenterCode(false);
        }         
    };

    const clickSubmitCentroCosto = (event) => {
        event.preventDefault(event);
        if (!centro.centerName) {
          setValidateCenterName(true);
        }
        if (!centro.centerCode) {
            setValidateCenterCode(true);
          }               
        if (
          centro.centerName &&
          centro.centerCode &&
          centro.note         
        ) {
          console.log('centro...',centro)

    const val =  {           
            "idInstitution": 1,
            "centerName": centro.centerName,
            "centerCode": centro.centerCode,            
            "note": centro.note            
    }  
     create(val)
          .then(data => {  
            if(data){
              console.log(data)
              if(data.ok === true){    
                toast.success("Cuenta Contable creada");
                changeToggle();
              }
              if(data.ok === false){                
                toast.error("Error al crear");
                changeToggle();
              }    
            }else{
              toast.error("Error en la api");
              changeToggle();            
            }           
          })
        }
    };
        
    return ( 
        <>
                <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Crear un nuevo Centro de Costo</TextFormulario>
            </div>
            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre del centro de costo</label>
              <input
                onChange={handleChangeCentroCosto("centerName")}
                value={centro.centerName}
                type="text"
                className={
                  validateCenterName ? "form-control is-invalid" : "form-control"
                }
                placeholder="Nombre del centro de costo"
              />
              {validateCenterName && (
                <p className="text-danger my-1">El nombre del centro de costo es obligatorio</p>
              )}
              <label>Número de centro de costo</label>
              <input
                onChange={handleChangeCentroCosto("centerCode")}
                value={centro.centerCode}
                type="number"
                className={
                  validateCenterCode ? "form-control is-invalid" : "form-control"
                }
                placeholder="Número de centro de costo"
              />
              {validateCenterCode && (
                <p className="text-danger my-1">El número de centro de costo es obligatorio</p>
              )}
               <label>Nota</label>
                <input                 
                 value={centro.note}
                  onChange={handleChangeCentroCosto("note")}
                  type="text"
                  className= {  "form-control"}                    
                  placeholder="Nota"
                />             
            </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmitCentroCosto}>Crear</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default NuevoCentroCosto;