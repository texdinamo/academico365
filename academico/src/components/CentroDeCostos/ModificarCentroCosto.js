import {useState,useEffect} from 'react'
import { updateCentroCosto } from './apiCentroCostos'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'

const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarCentroCosto = ({changeToggle,centroCosto,centroCostoId}) => {
    const [centro, setCentro] = useState({
        centerName: "",
        note: "",
        centerCode: "",
        idInstitution: ""        
      });

  useEffect(()=>{
    if(centroCosto){
      console.log('Soy el centro', centro)       
      setCentro({
        centerCode: centroCosto.centerCode,
        note: centroCosto.note,        
        id: centroCosto.id,        
        centerName: centroCosto.centerName,
        idInstitution: centroCosto.idInstitution
      });    
    }
  },[centroCosto])
console.log('ss',centro)
    const clickSubmit = () => {
      const val =  {        
        "id" : centro.id,
        "centerName": centro.centerName,
        "note": centro.note,
        "centerCode": centro.centerCode,        
        "idInstitution": centro.idInstitution
      }
      updateCentroCosto(val)
      .then(data => {
        console.log(data)
        if(data){
        if(data.ok === true){
          toast.success("Centro de costo modificada");
          changeToggle();
        }
        if(data.ok === false){
          toast.error("Error al modificar");
          changeToggle();
        }
      }
      else{
        toast.error("Problema con la api");
        changeToggle();
        }
      })
    }
    const handleChange = name => e => {
      setCentro({...centro, [name]: e.target.value})    
    }
    
    return ( 
        <>
           <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Modificar un Centro de Costo</TextFormulario>
            </div>
            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre</label>
              <input
                onChange={handleChange("centerName")}
                defaultValue={centro.centerName}
                type="text"
                className="input"
                placeholder="Nombre"
              />
               <label>Número de Centro Costo</label>
              <input
                onChange={handleChange("centerCode")}
                defaultValue={centro.centerCode}
                type="number"
                className="input"
                placeholder="Número de Centro de Costo"
              />              
              <label>Nota</label>
              <input
                onChange={handleChange("note")}
                defaultValue={centro.note}
                type="text"
                className="input"
                placeholder="Notas"
              />              
            </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmit}>Guardar</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>    
        </>
     );
}
 
export default ModificarCentroCosto;