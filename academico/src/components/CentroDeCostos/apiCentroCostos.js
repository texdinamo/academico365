export const create = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/costcenter/create`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const updateCentroCosto = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/costcenter/update`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const deleteCentroCosto = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/costcenter/remove`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}

export const getCentrosCostos = () => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/costcenter`, {
        method: "GET"
    })
        .then(response => {            
            return response.json()
        })
        .catch(err => console.log(err))
}

export const getCentroCostoId = (id) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/costcenter/${id}`, {
        method: "GET"
    })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .catch(err => console.log(err))
}