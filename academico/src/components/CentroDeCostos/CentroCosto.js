import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import Moment from "react-moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
import { URL } from "../../valores";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { useDatos } from "../hooks/useDatos";
import { getCentroCostoId } from './apiCentroCostos'
import styled from "styled-components";

const CentroCosto = (props) => {
const [centro, setCentro] = useState();

  useEffect(() => {  
    getCentroCostoId(props.location.state.fromDashboard).then(data => {
      console.log("esto es la data" + data)
     setCentro(data)
    })
  }, []);

  console.log("medios", centro);

  return (
    <>
    <Navbar/>
    <div className="dashboard">
      {centro ? (
        <Tabs>
          <TabList>
            <Tab>Información</Tab>            
          </TabList>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Información</div>
            <div className="datos-usuario d-flex">
              <div className="datos">
                <div className="d-flex">
                  <h5 className="info-descripcion">Nombre de Centro de Costo:</h5>
                  <p>{centro.centerName}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Número de Centro de Costo:</h5>
                  <p>{centro.centerCode}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Nota:</h5>
                  <p>{centro.note}</p>
                </div>             
              </div>                
            </div>              
          </TabPanel>         
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>
      )}
      </div>
    </>
  );
};

export default withRouter(CentroCosto);