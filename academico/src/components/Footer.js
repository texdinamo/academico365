import React from 'react';
import styled from 'styled-components'

import {  withRouter } from "react-router-dom";
import logo from '../img/powerby.svg'

const LogoFooter = styled.img`
padding: 10px 35px;
`
const Footer = (props) => {
    return ( 
    
<>
{  props.location.pathname === '/' || props.location.pathname === '/crear-cuenta' || props.location.pathname === '/reset-password' ?
(null) :
(
      <div className="col-12  d-flex justify-content-end foot">
           <LogoFooter src={logo} />
         </div>   
   )      
     
}
</>
    )
}
export default withRouter(Footer);