import React,{useState,useEffect} from 'react';
import Navbar from '../estructura/Navbar'
import {Link} from 'react-router-dom'
import axios from 'axios'
import Moment from 'react-moment'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {Spinner} from 'react-bootstrap'
import 'react-tabs/style/react-tabs.css';
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import {URL} from '../../valores'
import {getTutorId} from './apiTutores'

const Tutor = (props) => {

    const [Tutor,setTutor] = useState()

    useEffect(()=>{

        console.log('captura id de tutor',props)

    getTutorId(props.location.state.fromDashboard).then(data => {
        console.log(data)
        setTutor(data.user)
   
       })




    },[])


    const columns = [

        {dataField: "", text: 'Legajo'},
        {dataField: "", text: 'Nombre'},
            {   
        headerClasses: 'fas fa-arrow-down'
    },
    
        {dataField: "", text: 'Apellido'},
        {dataField: "", text: 'Dni'},
         {
            dataField: 'link',
            text: 'ACTION',
            formatter: (rowContent, row) => {
              return (   
                <>
                <i class="fas fa-pencil-alt mr-3 text-success"></i>
              
            <Link to="/Tutores/1"  className="link-i">
            <i class="far fa-eye"></i>
            </Link>
               
                <i class="far fa-trash-alt ml-3 text-danger" ></i>
                </>
                )
            }
        }
      ]
    

    return ( 
<>
<Navbar/>
<div  className="dashboard">

        {Tutor ?
        
        <Tabs>
<TabList>

<Tab>Informacion Personal</Tab>

<Tab>Alumnos a cargo</Tab>
</TabList>

<TabPanel className="Tutor">

    <div className="datos-usuario d-flex">

    <div>
    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">id:</h5>  <p >{Tutor.id}</p>
    </div>

    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Nombre:</h5>
    <p>{Tutor.name}</p>

    </div>

    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Apellido:</h5>
    <p>{Tutor.surname}</p>

    </div>


    <div className="d-flex" >

    <h5 className="info-descripcion pt-1">Dni:</h5>
    <p>{Tutor.documentation}</p>

    </div>



    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Fecha de nacimiento:</h5>
   
    <Moment format="YYYY/MM/DD">
    <p>{Tutor.birthDate}</p>
         </Moment>
    </div>

    <div className="d-flex mt-3" >
    <h5 className="info-descripcion pt-1">Sexo:</h5>
    <p>{Tutor.gender}</p>

    </div>
    </div>

    <div className="mr-4 ml-4">
    
    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Nacionalidad:</h5>
    <p>{Tutor.nacionality}</p>

    </div>


    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Domicilio:</h5>
    <p>{Tutor.address}</p>

    </div>



    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Barrio:</h5>
    <p>{Tutor.neighborhood}</p>

    </div>

    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Ciudad:</h5>
    <p>{Tutor.city}</p>

    </div>



    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Pais:</h5>
    <p>{Tutor.country}</p>

    </div>




    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Provincia:</h5>
    <p>{Tutor.province}</p>

    </div>
    </div>


    <div>
    
    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Codigo postal:</h5>
    <p>{Tutor.zipCode}</p>

    </div>


    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">email:</h5>
    <p>{Tutor.email}</p>

    </div>


    <div className="d-flex" >
    <h5 className="info-descripcion pt-1">Telefono:</h5>
    <p>{Tutor.phone}</p>

    </div>



    
    </div>

    </div>
















</TabPanel >

<TabPanel className="Tutor">
<div className="datos-usuario">
<p>Cursos y materias asignadas</p>



</div>
</TabPanel>

</Tabs>

        
        : 

        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">


        <Spinner animation="grow" variant="primary" size="xl"/>
        
        </div>
        
        }
        
        </div>
    </>
     );
}
 
export default Tutor;