export const create = (post) => {
    return fetch(`${process.env.REACT_APP_TUTOR_URL}`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const updateTutor = (post) => {
    return fetch(`${process.env.REACT_APP_TUTOR_URL}/update`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}

export const deleteTutores = (post) => {
    return fetch(`${process.env.REACT_APP_TUTOR_URL}/remove`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}

export const getTutores = () => {
    return fetch(`${process.env.REACT_APP_TUTOR_URL}`, {
        method: "GET"
    })
        .then(response => {
            return response.json()
        })
        .catch(err => console.log(err))
}

export const getTutorId = (id) => {
    return fetch(`${process.env.REACT_APP_TUTOR_URL}/${id}`, {
        method: "GET"
    })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .catch(err => console.log(err))
}