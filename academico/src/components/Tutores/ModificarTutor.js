import {useState,useEffect} from 'react'
import { updateTutor } from './apiTutores'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'



const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarTutor = ({changeToggle,alumnoId,alumno}) => {
  const [validarEmail,setValidarEmail] = useState(false)
    const [values, setValues] = useState({
        id: "",
        name: "",
        surname: "",
        documentation: "",
        birthDate: "",
        gender: "",
        nationality: "",
        address: "",
        neighborhood: "",
        city: "",
        province: "",
        country: "",
        zipCode: "",
        phoneNumber: "",
        email: "",
        createdAt: ""
       
      });


  useEffect(()=>{

    if(alumnoId){
      

      console.log('soy values',values)
      console.log('soy alumno',alumnoId)

      setValues({
        id: alumnoId.id,
        name: alumnoId.name,
        surname: alumnoId.surname,
        documentation: alumnoId.documentation,
        birthDate: alumnoId.birthDate,
        gender: alumnoId.gender,
        nationality: alumnoId.nacionality,
        address: alumnoId.address,
        neighborhood: alumnoId.neighborhood,
        city: alumnoId.city,
        province: alumnoId.province,
        country: alumnoId.country,
        zipCode: alumnoId.zipCode,
        phoneNumber: alumnoId.phoneNumber,
        email: alumnoId.email,
        createdAt: alumnoId.createdAt
      });


    
    }


console.log(values)
  },[])


  console.log('soy alumno',alumnoId)

    const clickSubmit = () => {

      if(validarEmail === false){


        let val = {

          "createdAt": values.createdAt,
          "userType": "TUTOR",
          "user": {
            "id": values.id,
            "name": values.name,
            "surname": values.surname,
            "documentation": values.documentation,
            "birthDate": values.birthDate,
            "gender": values.gender,
            "nacionality": values.nationality,
            "address": values.address,
            "neighborhood": values.neighborhood,
            "city": values.city,
            "province": values.province,
            "country": values.country,
            "zipCode": values.zipCode,
            "phone": values.phone,
            "email": values.email,
            "metadata": null,
            "createdAt": values.createdAt,
          }
        }

      updateTutor(val)
      .then(data => {
        console.log(data)

        if(data){
        if(data.ok === true){

          toast.success("Tutor modificado");
          changeToggle();
        }
        if(data.ok === false){

          toast.error("Error al modificar");
          changeToggle();
        }
      }
      else{
        toast.error("Problema con la api");
        changeToggle();
      }

      })
    }

    }

    const handleChange = name => e => {
      setValues({...values, [name]: e.target.value})
      if(name === "email"){

        if(e.target.value.indexOf('@') !== -1 &&  e.target.value.indexOf('.') !== -1 ){
          setValidarEmail(false)
       }else{
        setValidarEmail(true)
      }
    }


    if(name === "name"){
      console.log(e.target.value)
    }

    if(name === "documentation"){
      console.log(e.target.value)
    }

  }


    return ( 
        <>
   <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Modificar Tutor</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>

          <div className="formulario__body ">
            <div>
              <form className="input-form">
                <p className="mt-2 subtitulo">Datos personales</p>
                <Linea />
                <label>Nombre</label>
                <input
                  onChange={handleChange("name")}
                  defaultValue={alumnoId ? alumnoId.name : false}
                  type="text"
                  className="input"
                  placeholder="Nombre"
                />
                <label>Apellido</label>
                <input
                  onChange={handleChange("surname")}
                  defaultValue={ alumnoId ?  alumnoId.surname : false}
                  type="text"
                  className="input"
                  placeholder="Apellido"
                />
                <label>Dni</label>
                <input
                  onChange={handleChange("documentation")}
                  defaultValue={ alumnoId ?  alumnoId.documentation : false}
                  type="number"
                  className="input"
                  placeholder="Dni"
                />
                <label>Fecha de nacimiento</label>
                <input
                  onChange={handleChange("birthDate")}
                  defaultValue={ alumnoId ?  alumnoId.birthDate : false}
                  type="date"
                  className="input"
                  placeholder="Cuil"
                />

                <label>Sexo</label>
                <div className="radio d-flex justify-content-center">
                  <label>Masculino</label>
                  <input
                    type="radio"
                    checked={alumnoId ?  alumnoId.gender === "masculino" : false}
                    name="gender"
                    value="masculino"
                    onChange={handleChange("gender")}
                  />

                  <label>Femenino</label>

                  <input
                    type="radio"
                    checked={alumnoId ? alumnoId.gender === "femenino" : false}
                    name="gender"
                    value="femenino"
                    onChange={handleChange("gender")}
                  />
                </div>

                <label>Nacionalidad</label>
                <input
                  onChange={handleChange("nationality")}
                  defaultValue={alumnoId ? alumnoId.nacionality : false}
                  type="text"
                  className="input"
                  placeholder="Apellido"
                />

                <p className="mt-4 subtitulo">Contactos</p>
                <Linea />

                <label>Domicilio</label>
                <input
                  onChange={handleChange("address")}
                  defaultValue={alumnoId ?  alumnoId.address : false}
                  type="text"
                  className="input"
                  placeholder="Domicilio"
                />

                <label>Barrio</label>
                <input
                  onChange={handleChange("neighborhood")}
                  defaultValue={ alumnoId ? alumnoId.neighborhood : false}
                  type="text"
                  className="input"
                  placeholder="Barrio"
                />

                <label>Ciudad</label>
                <input
                  onChange={handleChange("city")}
                  defaultValue={ alumnoId ?  alumnoId.city : false}
                  type="text"
                  className="input"
                  placeholder="Ciudad"
                />

                <label className="mb-3">Pais</label>
                <Select
                  name="country"
                  defaultValue={ alumnoId ? alumnoId.country : false}
                  onChange={handleChange("country")}
                >
                  <option value="">-- Seleccione pais--</option>
                  <option value="argentina">Argentina</option>
                  <option value="bolivia">Bolivia</option>
                  <option value="chile">Chile</option>
                  <option value="peru">Perú</option>
                  <option value="venezuela">Venezuela</option>
                </Select>

                <label>Provincia</label>
                <input
                  onChange={handleChange("province")}
                  defaultValue={ alumnoId ?  alumnoId.province : false}
                  type="text"
                  className="input"
                  placeholder="Provincia"
                />

                <label>Codigo postal</label>
                <input
                  onChange={handleChange("zipCode")}
                  defaultValue={ alumnoId ? alumnoId.zipCode : false}
                  type="text"
                  className="input"
                  placeholder="Cod postal"
                />

                <label>Telefono</label>
                <input
                  onChange={handleChange("phoneNumber")}
                  defaultValue={alumnoId ?  alumnoId.phone : false}
                  type="number"
                  className="input"
                  placeholder="Telefono"
                />

                <label>Email</label>
                <input
                  onChange={handleChange("email")}
                  defaultValue={alumnoId ? alumnoId.email : false}
                  type="email"
                  className="input"
                  placeholder="Email"
                />

{validarEmail && (
                <p className="text-danger my-1">El Email debe contener @ y .</p>
              )}

              </form>
            </div>

            <div className="formulario__footer">
              <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>

              <ButtonCrear onClick={clickSubmit}>Modificar</ButtonCrear>
            </div>
          </div>
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default ModificarTutor;