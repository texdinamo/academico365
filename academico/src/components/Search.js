import React,{useState,useEffect} from 'react';
import { withRouter,Link} from 'react-router-dom';
import BootstrapTable from 'react-bootstrap-table-next'
import {Spinner} from 'react-bootstrap'
import axios from 'axios'


const Search = (props) => {

    const [alumnos,setAlumnos] = useState([])



useEffect(()=>{
    axios.get(`${process.env.REACT_APP_STUDENT_URL}`)
    .then(res => {
  //// aca esta la respuesta en la consola del navegador
        console.log('respuesta de la endpoint!!',res)
        setAlumnos(res.data)

    })

  },[])


  const columns = [

    {dataField: "docket", text: 'Legajo'},
    {dataField: "name", text: 'Nombre'},
    {dataField: "surname", text: 'Apellido'},
    {dataField: "document", text: 'Dni'},
     {
        dataField: 'link',
        text: 'ACTION',
        formatter: (rowContent, row) => {
          return (   
            <>
            <i class="fas fa-pencil-alt mr-3 text-success"></i>
          
        <Link to="/dashboard/alumnos/1234"  className="link-i">
        <i class="far fa-eye"></i>
        </Link>
           
            <i class="far fa-trash-alt ml-3 text-danger" ></i>
            </>
            )
        }
    }
  ]

  let buscar = props.location.state.valor.buscar
  let resultado =  alumnos.filter((alumno)=> {
    return   alumno.name.toLowerCase() === buscar.toLowerCase() || alumno.docket === parseInt(buscar,10) || alumno.surname.toLowerCase() === buscar.toLowerCase() || alumno.email === buscar || alumno.document === parseInt(buscar,10);
    
});

console.log('resultado',resultado)
  
  return ( 
        <div className="search">

        
        <h5 className="container">Resultado de busqueda:</h5>

        { alumnos.length === 0  && 
        
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">


        <Spinner animation="grow" variant="primary" size="xl"/>
        
        </div>
        
        }
        

        {
        
        resultado.length > 0
        


        &&
        <BootstrapTable
        keyField="docket"
        data={resultado}
        columns={columns}
        

        />
        
        }

      
        </div>
     );
}
 
export default withRouter(Search);