import React,{useEffect,useState} from 'react';
import {  Link ,useHistory } from 'react-router-dom';
import logo from '../img/logo aca365.svg'
import {API} from '../config'
import { resetPassword } from '../auth/index'
import { ToastContainer, toast } from "react-toastify";
import { Spinner } from 'react-bootstrap'
import "react-toastify/dist/ReactToastify.min.css";


const PasswordReset = () => {


    let history = useHistory()

    const [values,setValues] = useState({
        email: '',
        error: '',
        loading: false,
        redireccionar: false
    })

    const [validateEmail,setValidateEmail] = useState(false)
    const [spinner,setSpinner] = useState(false)
    const [validarEmail,setValidarEmail] = useState(false)
    

     const {email,password} = values

    const handleChange = name => event => 
    {
        setValues({...values,error:false,[name]:event.target.value})

        if (values.email === ''){
            setValidateEmail(false)
        }

        if(event.target.value.indexOf('@') !== -1 &&  event.target.value.indexOf('.') !== -1 ){
            setValidarEmail(false)
         }else{
          setValidarEmail(true)
        }
    
       


    }





const clickSubmit = (event) => {
            event.preventDefault()


            if(!values.email){
                setValidateEmail(true)
            }

           

            if(values.email && validarEmail === false){

                setSpinner(true)

        setValues({...values,error:false,loading: true})
        

        let val = {
            "email": values.email
            }
          resetPassword(val).then((data) => {
            console.log("respuesta", data);


            if(data){
    
                setSpinner(false)
            if(data.ok === true){

          setSpinner(false)
                
                setValues({
                            email: ''
                           
                            })

             history.push({
                 pathname: '/',
                 state: {
                     sendEmail : true
                 }
             })

            }
            if(data.ok === false){

                setSpinner(false)
              toast.error('El email no se encuentra registrado')
            }
        }else{

            setSpinner(false)
            toast.error('Error en el server')
        }
    
            
          });
                

            }


            

}


useEffect(()=>{

},[])


    return ( 
        <>
        <ToastContainer/>
        <div className="login  d-flex ">
        <div className="col-4  login__form ">
        <Link to="/">
      <i class="fas fa-arrow-left"></i>
      </Link>
        <img src={logo} />

    <form className="login__form--form">
        <label>Email</label>
        <input className={validateEmail ? 'input form-control is-invalid'  : ' input form-control'   }    onChange={handleChange('email')} value={email} placeholder="Ej: email@email.com"/>
        {validarEmail && (
                <p className="text-danger my-1">El Email debe contener @ y .</p>
              )}
    </form>

    <button onClick={clickSubmit} className={values.email ? "boton boton--login  " :  "boton boton--login btn-opacity"}  disabled={spinner} >
    {spinner ? (

<Spinner animation="border" variant="light" className="h6 mt-1" />


          ) : "Enviar email" }
    </button>
        

       

</div>
        <div className="col-8 login__img d-flex justify-content-center"> 

            <div className="login__img--content">

                <h3 className="title">Sistema premium <br/> de Educación</h3>
                <p className="text">
                Es un producto de Académico365 destinado a brindarte la tecnología que necesitas para tener tu propia academia online de una forma tan sencilla que no lo podrás creer. Hace realmente simple el administrar, vender y cobrar tus cursos y te libera tiempo.
                </p>
            </div>

        </div>


        </div>
        </>
     );
}
 
export default PasswordReset;