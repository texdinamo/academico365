export const create = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        return response
    })
    .catch(err => console.log(err))
}

export const addStudent = (idDivition,idStudent) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition/${idDivition}/students/${idStudent}`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        }
    })
    .then(response =>{
        return response
    })
    .catch(err => console.log(err))
}


export const addMaterias = (idDivition,idStudent) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition/${idDivition}/content/${idStudent}`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        }
    })
    .then(response =>{
        console.log(response)
        return response
    })
    .catch(err => console.log(err))
}

export const addDocentes = (idDivition,idStudent) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition/${idDivition}/teachers/${idStudent}`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        }
    })
    .then(response =>{
        return response
    })
    .catch(err => console.log(err))
}


export const updateCurso = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition/update`,{
        method: "POST",
        headers: {
            Accept: 'application/json',
           "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        console.log(response)
        return response
    })
    .catch(err => console.log(err))
}

export const deleteCursos = (post /*,token*/) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition/remove`,{
        method: "POST",
        headers: {
            Accept: 'application/json',
           "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        return response
    })
    .catch(err => console.log(err))
}


export const getCursos = () => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition`,{
         method: "GET"
     })
     .then(response => {
         return response.json()
     })
     .catch(err => console.log(err))
 }

 export const getCursoId = (id) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition/${id}`,{
         method: "GET"
     })
     .then(response => {
         return response.json()
     })
     .catch(err => console.log(err))
 }