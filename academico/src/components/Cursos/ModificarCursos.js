import { useState, useEffect } from 'react'
import { updateCurso } from './apiCursos'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import {
  AgregarFormularioAlumno,
  // ButtonAgregar,
  ButtonCrear,
  ButtonCancelar,
  TextFormulario,
  // TextInfo,
  // ContainerMenu,
  // SubTextInfo,
  Linea,
} from "../styled-component/Navbar";
import styled from 'styled-components'



const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarCursos = ({ changeToggle, alumno, alumnoId }) => {
    const [values, setValues] = useState({
    
      name: "",
      shortName: "",
      year: "",
      fechaInicio: "",
      fechaFinal: "",
      horarioInicio: "",
      horarioFinal: "",
      valorMatricular: "",
      valorPrimeraCuota: "",
      valorRestanteCuotas: "",
      valorDerechoExamen: "",
      cupos: "",
      textoAyuda: "",
      dia: "",
      price: "",
      textoAyudaMatricula: "",
    textoAyudaExamen: "",
    docentes: [],
    materias: [],
    alumnos: [],
    idInstitution: ""
        
      });

      
    

  useEffect(() => {

    if (alumno) {


      console.log('soy values', values)
      console.log('soy alumno', alumno)

      setValues({
        name: alumno.name,
        shortName: alumno.shortName,
        year: alumno.year,
        fechaInicio: alumno.schedules[0].dateSince,
        fechaFinal: alumno.schedules[0].dateUntil,
        horarioInicio: alumno.schedules[0].hourFrom,
        horarioFinal: alumno.schedules[0].hourTo,
        valorMatricular: alumno.valorMatricula,
        valorDerechoExamen: alumno.valorDerechoExamen,
        cupos: alumno.cupo,
        textoAyuda: alumno.help_price,
        dia: alumno.schedules[0].dayOfWeek,
        price: alumno.price,
      textoAyudaMatricula: alumno.help_enrollment,
      textoAyudaExamen: alumno.help_exam,
      docentes: alumno.docentes,
      materias: alumno.materias,
      alumnos: alumno.alumnos,
      idInstitution: alumno.idInstitution
      });



    }

  }, [alumno])

  console.log('soy alumnoid....',alumnoId)


  console.log('soy values', values)

  const clickSubmit = () => {

    let val = {
      "cupo": parseInt(values.cupos,10),
      "help_enrollment": "Aqui debes cargar el Precio de Lista de la Matricula",
    "help_exam": "Aqui debes cargar el Precio de Lista del Derecho de Examen",
    "help_price": "Aqui debes cargar el Precio de Lista del Curso Completo si lo vas a cobrar con un plan del tipo \"Monto Unico Financiado\" o el Precio de Lista de la Cuota si lo vas a cobrar con un plan del tipo \"Montos Periodico\"",
      
      "idEspecialidad": 0,
      "alumnos": values.alumnos,
      "docentes": values.docentes,
      "materias" : values.materias,
      "idSeccion": 1,
      "id_divition": alumnoId.id_divition,
      "idInstitution": values.idInstitution,
      "name": values.name,
      "price": parseFloat(parseFloat(values.price).toFixed(2)),
      "schedules": [
        {
          "dateSince": values.fechaInicio,
          "dateUntil": values.fechaFinal,
          "dayOfWeek": values.dia,
          "hourFrom": values.horarioInicio,
          "hourTo": values.horarioFinal
        }
      ],
      "shortName":values.shortName,
      "valorDerechoExamen": parseFloat(parseFloat(values.valorDerechoExamen).toFixed(2)),
      "valorMatricula": parseFloat(parseFloat(values.valorMatricular).toFixed(2)),
      "year": parseInt(values.year,10)
    }


    

      console.log(val)

    updateCurso(val)
      .then(data => {
        console.log(data)
        if (data) {
          if (data.ok === true) {
            toast.success("Alumno modificado");
            changeToggle();
          }
          if (data.ok === false) {
            toast.error("Error al modificar");
            changeToggle();
          }
        }
        else {
          toast.error("Problema con la api");
          changeToggle();
        }
      })

  }

  const handleChange = name => e => {
    setValues({ ...values, [name]: e.target.value })

    if (name === "price") {


      let valor = e.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setValues({ ...values, ["price"]: valorParse });

    }


    if (name === "valorMatricular") {


      let valor = e.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setValues({ ...values, ["valorMatricular"]: valorParse });

    }
    
    if (name === "valorDerechoExamen") {
 
      let valor = e.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setValues({ ...values, ["valorDerechoExamen"]: valorParse });

      


    }

  }


  return (
    <>
   <AgregarFormularioAlumno>
        <div className="formulario__head">
          <div>
            <TextFormulario>Modificar Curso</TextFormulario>
          </div>

          <div className="delete" onClick={changeToggle}>
            <i class="fas fa-times"></i>
          </div>
        </div>

        {alumno ?
        
            <>
        <div className="formulario__body ">
          <form className="input-form">
            <label>Nombre</label>
            <input
              onChange={handleChange("name")}
              defaultValue={values.name}
              type="text"
              className="form-control"
              placeholder="Nombre"
            />
            

          <label>Nombre corto</label>
            <input
              onChange={handleChange("shortName")}
              defaultValue={values.shortName}
              type="text"
              className="form-control"
            
              placeholder="Nombre corto"
            />
            

            <label>Año</label>
            <input
              onChange={handleChange("year")}
              value={values.year}
              type="number"
              className="form-control"
              
              placeholder="Año"
            />
            


<label>Dia del curso</label>

<Select
              onChange={handleChange("dia")}
              value={values.dia}
              >
                <option value="">Seleccione un dia</option>
                <option value="MONDAY">Lunes</option>
                <option value="TUESDAY">Martes</option>
                <option value="WEDNESDAY">Miercoles</option>
                <option value="THURSDAY">Jueves</option>
                <option value="FRIDAY">Viernes</option>
                <option value="SATURDAY">Sabado</option>
                <option value="SUNDAY">Domingo</option>
              </Select>

            


            <label>Fecha de inicio</label>
            <input
              onChange={handleChange("fechaInicio")}
              defaultValue={values.fechaInicio}
              type="date"
              className="form-control"
              
              placeholder="Fecha de inicio"
            />
            

<label>Fecha final</label>
            <input
              onChange={handleChange("fechaFinal")}
              defaultValue={values.fechaFinal}
              type="date"
              className="form-control"
        
              placeholder="Fecha final"
            />
        

            <label>Hora de inicio</label>
            <input
              onChange={handleChange("horarioInicio")}
              defaultValue={values.horarioInicio}
              type="time"
              className="form-control"
              
              placeholder="Hora de inicio"
            />
                        <label>Hora final</label>
            <input
              onChange={handleChange("horarioFinal")}
              defaultValue={values.horarioFinal}
              type="time"
              className="form-control"
              
              placeholder="Hora Final"
            />



            <label>Precio</label>
            <input
              onChange={handleChange("price")}
              value={values.price}
              type="number"
              className="form-control"
            
              placeholder="Precio"
            />
            





            <label>Valor matricula</label>
            <input
              onChange={handleChange("valorMatricular")}
              value={values.valorMatricular}
              type="number"
              className="form-control"
            
              placeholder="Valor de matricula"
            />
           
                       <label>Valor derecho de examen</label>
            <input
              onChange={handleChange("valorDerechoExamen")}
              value={values.valorDerechoExamen}
              type="number"
              className="form-control"
            
              placeholder="Valor derecho a examen"
            />



            <label>Cantidad de cupos</label>
            <input
              onChange={handleChange("cupos")}
              defaultValue={values.cupos}
              type="number"
              className="form-control"
            
              placeholder="Cupos"
            />
                       
          </form>
        </div>

        <div className="formulario__footer">
          <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
          <ButtonCrear onClick={clickSubmit}>Modificar</ButtonCrear>
        </div>
        </>
        : 
        
            <h1 className="text-danger mt-5">{`Error al cargar datos del curso ${alumnoId}`}</h1>
        
        }

        
      </AgregarFormularioAlumno>
    </>
  );
}

export default ModificarCursos;