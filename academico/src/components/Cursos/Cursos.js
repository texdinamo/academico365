import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link,withRouter } from "react-router-dom";
import SweetAlert from "react-bootstrap-sweetalert";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import axios from "axios";
import { URL } from "../../valores";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import {
  AgregarFormularioAlumno,
  ButtonAgregar,
  ButtonCrear,
  ButtonCancelar,
  TextFormulario,
  TextInfo,
  ContainerMenu,
  SubTextInfo,
  Linea,
} from "../styled-component/Navbar";

import styled from "styled-components";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { val } from "./cursosExample";
import {getCursos, deleteCursos,getCursoId} from './apiCursos'

import ModificarCursos from './ModificarCursos'


const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

export const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`;

const Cursos = (props) => {
  const { SearchBar } = Search;

  const [confirmar, setConfirmar] = useState(false);

  const [eliminar, setEliminar] = useState(false);
  const [values, setValues] = useState();
  const [toggle, setToggle] = useState(false);
  const [alumnos, setAlumnos] = useState([]);
  const [alumno, setAlumno] = useState();
  const [alumnoId, setAlumnoId] = useState();
  const [confirmarError, setConfirmarError] = useState(false);


  const changeToggle = () => setToggle((prevLogin) => !prevLogin);

  const changeEliminar = () => setEliminar((prevLogin) => !prevLogin);

  const changeConfirmar = () => setConfirmar((prevLogin) => !prevLogin);

  

  const cambio = async () => {
    setEliminar(false);


    let post = {
        "id_divition": alumnoId.id_divition
    }

    deleteCursos(post).then(data => {
        
      console.log(data)

        if(data){

        
        if(data.ok === true){

          setConfirmar(true);
        
        }

        if(data.ok === false){
            setConfirmarError(true)
        }
      }else{
        setConfirmarError(true)
      }
      })
      
  
  };


  useEffect(() => {

      console.log(alumnoId);
    getCursos().then(data => {
        console.log(data)
        setAlumnos(data.errors ? [] : data)
        })

        /*
        getCursoId(alumnoId).then(data => {
          setAlumno(data)
          })
  
*/

if(alumnoId){
  getCursos().then(data => {
    console.log(data)
    setAlumnos(data.errors ? [] : data)
    })


  axios
      .get(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition/${alumnoId.id_divition}`)
      .then((res) => {
        //// aca esta la respuesta en la consola del navegador
        console.log("respuesta de la endpoint!!", res);
        setAlumno(res.data);
      });


}
  


      

  }, [confirmar,alumnoId]);

  const columns = [
    {
      dataField: "id_divition",
      text: "Id",
      sort: true,
      searchable: true,
      sortCaret: (order, column) => {
        if (!order)
          return (
            <span className="inicio-arrow">
              {" "}
              <i class="fas fa-caret-down"></i> <i class="fas fa-caret-up"></i>
            </span>
          );
        else if (order === "asc")
          return (
            <span>
              {" "}
              <i class="fas fa-caret-down"></i>{" "}
              <font color="red">
                <i class="fas fa-caret-up color"></i>
              </font>
            </span>
          );
        else if (order === "desc")
          return (
            <span>
              {" "}
              <font color="red">
                {" "}
                <i class="fas fa-caret-down color"></i>
              </font>{" "}
              <i class="fas fa-caret-up"></i>
            </span>
          );
        return null;
      },
    },
    {
      dataField: "name",
      text: "Nombre",
      sort: true,
      searchable: true,
      sortCaret: (order, column) => {
        if (!order)
          return (
            <span className="inicio-arrow">
              {" "}
              <i class="fas fa-caret-down"></i> <i class="fas fa-caret-up"></i>
            </span>
          );
        else if (order === "asc")
          return (
            <span>
              {" "}
              <i class="fas fa-caret-down"></i>{" "}
              <font color="red">
                <i class="fas fa-caret-up color"></i>
              </font>
            </span>
          );
        else if (order === "desc")
          return (
            <span>
              {" "}
              <font color="red">
                {" "}
                <i class="fas fa-caret-down color"></i>
              </font>{" "}
              <i class="fas fa-caret-up"></i>
            </span>
          );
        return null;
      },
    },

    {
      dataField: "year",
      text: "Año",
      sort: true,
      searchable: true,
      sortCaret: (order, column) => {
        if (!order)
          return (
            <span className="inicio-arrow">
              {" "}
              <i class="fas fa-caret-down"></i> <i class="fas fa-caret-up"></i>
            </span>
          );
        else if (order === "asc")
          return (
            <span>
              {" "}
              <i class="fas fa-caret-down"></i>{" "}
              <font color="red">
                <i class="fas fa-caret-up color"></i>
              </font>
            </span>
          );
        else if (order === "desc")
          return (
            <span>
              {" "}
              <font color="red">
                {" "}
                <i class="fas fa-caret-down color"></i>
              </font>{" "}
              <i class="fas fa-caret-up"></i>
            </span>
          );
        return null;
      },
    },
    {
      dataField: "idSeccion",
      text: "Seccion",
      sort: true,
      searchable: true,
      sortCaret: (order, column) => {
        if (!order)
          return (
            <span className="inicio-arrow">
              {" "}
              <i class="fas fa-caret-down"></i> <i class="fas fa-caret-up"></i>
            </span>
          );
        else if (order === "asc")
          return (
            <span>
              {" "}
              <i class="fas fa-caret-down"></i>{" "}
              <font color="red">
                <i class="fas fa-caret-up color"></i>
              </font>
            </span>
          );
        else if (order === "desc")
          return (
            <span>
              {" "}
              <font color="red">
                {" "}
                <i class="fas fa-caret-down color"></i>
              </font>{" "}
              <i class="fas fa-caret-up"></i>
            </span>
          );
        return null;
      },
    },
    {
      dataField: "link",
      text: "Accion",
      formatter: (rowContent, row) => {
        return (
          <>
            <i
              class="fas fa-pencil-alt mr-3 text-success"
              onClick={changeToggle}
            ></i>

            <Link
              to={{
                pathname: "/cursos/curso",
                state: { fromDashboard: row.id_divition },
              }}
              className="link-i"
            >
              <i class="far fa-eye"></i>
            </Link>

            <Link>
              <i class="fas fa-clone"></i>
            </Link>

            <i
              class="far fa-trash-alt ml-3 text-danger"
              onClick={changeEliminar}
            ></i>
          </>
        );
      },
    },
  ];

  const options = {
    paginationSize: 3,
    pageStartIndex: 0,
    withFirstAndLast: false,
    firstPageText: "First",
    prePageText: "Anterior",
    nextPageText: "Siguiente",
    showTotal: false,
    disablePageTitle: true,
  };

  const rowEvents = {
    onClick: (e, row, rowIndex) => {
      console.log(row)
      setAlumnoId(row);
    },
    onMouseEnter: (e, row, rowIndex) => {
      setAlumnoId(row);
      console.log(row)
    },
  };


  return (
    <>
    <Navbar/>
    <div className="dashboard">
      <ToastContainer />
      <ToolkitProvider keyField="id" data={alumnos} columns={columns} search>
        {(props) => (
          <>
            <div className="barra">
              <i class="fas fa-search ml-3 mt-2"></i>
              <SearchBar placeholder="Buscar cursos" {...props.searchProps} />
            </div>
            <BootstrapTable
              rowEvents={rowEvents}
              {...props.baseProps}
              pagination={paginationFactory(options)}
            />
          </>
        )}
      </ToolkitProvider>

      {toggle && <AgregarShadow onClick={changeToggle} />}

      {eliminar && (
        <SweetAlert
          primary
          showCancel
          cancelBtnBsStyle="secondary"
          style={{
            fontSize: "12px",
            fontWeight: "700",
            width: "500px",
            height: "240px",
          }}
          onConfirm={() => cambio()}
          onCancel={() => setEliminar(false)}
          confirmBtnText="Aceptar"
          title="Estas seguro de realizar esta accion?"
          focusCancelBtn
        >
          <p style={{ fontSize: "14px", margin: "5px" }}>
            Esta apunto de realizar una accion irreversible
          </p>

          <p style={{ fontSize: "14px", marginBottom: "20px" }}>
            Confirma tu accion
          </p>
        </SweetAlert>
      )}

      {confirmar && (
        <SweetAlert
          success
          title="Eliminado"
          onConfirm={() => changeConfirmar()}
        ></SweetAlert>
      )}

{confirmarError && (
        <SweetAlert
          danger
          title="Error"
          onConfirm={() => setConfirmarError(false)}
        ></SweetAlert>
      )}


      {toggle && (
      
      <ModificarCursos changeToggle={changeToggle} alumnoId={alumnoId}  alumno={alumno} />
      )}
    </div>
    </>
  );
};

export default withRouter(Cursos);
