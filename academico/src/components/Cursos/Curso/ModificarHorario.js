import { useState, useEffect } from 'react'
import { updateCurso } from './apiCursos'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import {
  AgregarFormularioAlumno,
  // ButtonAgregar,
  ButtonCrear,
  ButtonCancelar,
  TextFormulario,
  // TextInfo,
  // ContainerMenu,
  // SubTextInfo,
  Linea,
} from "../styled-component/Navbar";
import styled from 'styled-components'



const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarHorario = ({ changeToggle, alumno, alumnoId }) => {
    const [values, setValues] = useState({
    
      name: "",
      shortName: "",
      year: "",
      fechaInicio: "",
      fechaFinal: "",
      horarioInicio: "",
      horarioFinal: "",
      valorMatricular: "",
      valorPrimeraCuota: "",
      valorRestanteCuotas: "",
      valorDerechoExamen: "",
      cupos: "",
      textoAyuda: "",
      dia: "",
      price: "",
      textoAyudaMatricula: "",
    textoAyudaExamen: ""
        
      });

      
    

  useEffect(() => {

    if (alumno) {


      console.log('soy values', values)
      console.log('soy alumno', alumno)

      setValues({
        name: alumno.name,
        shortName: alumno.shortName,
        year: alumno.year,
        fechaInicio: alumno.schedules[0].dateSince,
        fechaFinal: alumno.schedules[0].dateUntil,
        horarioInicio: alumno.schedules[0].hourFrom,
        horarioFinal: alumno.schedules[0].hourTo,
        valorMatricular: alumno.valorMatricula,
        valorDerechoExamen: alumno.valorDerechoExamen,
        cupos: alumno.cupo,
        textoAyuda: alumno.help_price,
        dia: alumno.schedules[0].dayOfWeek,
        price: alumno.price,
      textoAyudaMatricula: alumno.help_enrollment,
      textoAyudaExamen: alumno.help_exam
      });



    }

  }, [alumno])


  console.log('soy values', values)

  const clickSubmit = () => {

    let val = {
      "cupo": parseInt(values.cupos,10),
      "help_price": values.textoAyuda,
      "help_enrollment": values.textoAyudaMatricula,
      "help_exam": values.textoAyudaExamen,
      "idAlumnos": [
        0
      ],
      "idEspecialidad": 0,
      "idMaterias": [
        0
      ],
      "idProfesores": [
        0
      ],
      "idSeccion": 1,
      "id_divition": alumnoId.id_divition,
      "name": values.name,
      "price": parseFloat(parseFloat(values.price).toFixed(2)),
      "schedules": [
        {
          "dateSince": values.fechaInicio,
          "dateUntil": values.fechaFinal,
          "dayOfWeek": values.dia,
          "hourFrom": values.horarioInicio,
          "hourTo": values.horarioFinal
        }
      ],
      "shortName":values.shortName,
      "valorDerechoExamen": parseFloat(parseFloat(values.valorDerechoExamen).toFixed(2)),
      "valorMatricula": parseFloat(parseFloat(values.valorMatricular).toFixed(2)),
      "year": parseInt(values.year,10)
    }


      console.log(val)

    updateCurso(val)
      .then(data => {
        console.log(data)
        if (data) {
          if (data.ok === true) {
            toast.success("Alumno modificado");
            changeToggle();
          }
          if (data.ok === false) {
            toast.error("Error al modificar");
            changeToggle();
          }
        }
        else {
          toast.error("Problema con la api");
          changeToggle();
        }
      })

  }

  const handleChange = name => e => {
    setValues({ ...values, [name]: e.target.value })

    if (name === "price") {


      let valor = e.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setValues({ ...values, ["price"]: valorParse });

    }


    if (name === "valorMatricular") {


      let valor = e.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setValues({ ...values, ["valorMatricular"]: valorParse });

    }
    
    if (name === "valorDerechoExamen") {
 
      let valor = e.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setValues({ ...values, ["valorDerechoExamen"]: valorParse });

      


    }

  }


  return (
    <>
   <AgregarFormularioAlumno >
              <div className="formulario__head">
                <div>
                  <TextFormulario>Modificar Precio</TextFormulario>
                </div>
                <div className="delete" onClick={handleChange}><i class="fas fa-times"></i></div>
              </div>
              <div className="formulario__body ">
                <div>
                  <form className="input-form">
                  <label>Precio del curso</label>
                    <input defaultValue={alumno.price} type="number" className="input" placeholder="precio del curso" />
                   
                    <label>Matricula</label>
                    <input defaultValue={alumno.valorMatricula} type="number" className="input" placeholder="valor de matricula" />
                    <label>Derecho de examen</label>
                    <input defaultValue={alumno.valorDerechoExamen} type="number" className="input" placeholder="valor derecho de examen" />
                  </form>
                </div>
                <div className="formulario__footer">
                  <ButtonCancelar onClick={handleChange}>
                    Cancelar
                  </ButtonCancelar>
                  <ButtonCrear onClick={clickSubmit} >
                    Modificar
                  </ButtonCrear>
                </div>
              </div>
            </AgregarFormularioAlumno>
    </>
  );
}

export default ModificarHorario;