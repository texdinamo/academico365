import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { create } from './apiCursos'

import {
  AgregarShadow,
  AgregarFormulario,
  ButtonAgregar,
  ButtonCrear,
  ButtonCancelar,
  Select,
  Linea,
  TextFormulario,
  TextInfo,
  ContainerMenu,
  SubTextInfo,
  AgregarFormularioAlumno,
} from "../styled-component/Navbar";



const NuevoCurso = ({ changeToggle }) => {
  const [validateIdSeccion, setValidateIdSeccion] = useState(false);
  const [validateYear, setValidateYear] = useState(false);
  const [validatePrice, setValidatePrice] = useState(false);
  const [validateShortName, setValidateShortName] = useState(false);

  const [validateNameCurso, setValidateNameCurso] = useState(false);
  const [validateFechaInicio, setValidateFechaInicio] = useState(false);
  const [validateHorario, setValidateHorario] = useState(false);
  const [validateFechaFinal, setValidateFechaFinal] = useState(false);
  const [validateValorMatricula, setValidateValorMatricula] = useState(false);
  const [validatePrimeraCuota, setValidatePrimeraCuota] = useState(false);
  const [validateRestanteCuotas, setValidateRestanteCuotas] = useState(false);
  const [validateDerechoExamen, setValidateDerechoExamen] = useState(false);
  const [validateCupos, setValidateCupos] = useState(false);
  const [validateHorarioFinal, setValidateHorarioFinal] = useState(false);
  const [validateAyuda,setValidateAyuda] = useState(false)
  const [validateAyudaMatricula,setValidateAyudaMatricula] = useState(false)
  const [validateAyudaExamen,setValidateAyudaExamen] = useState(false)
  const [validateDia,setValidateDia] = useState(false)
  const [curso, setCurso] = useState({
    name: "",
    shortName: "",
    year: "",
    fechaInicio: "",
    fechaFinal: "",
    horarioInicio: "",
    horarioFinal: "",
    valorMatricular: "",
    valorPrimeraCuota: "",
    valorRestanteCuotas: "",
    valorDerechoExamen: "",
    cupos: "",
    textoAyuda: "",
    dia: "",
    price: "",
    textoAyudaMatricula: "",
  textoAyudaExamen: ""
    
  });


    const numberFormat = value =>
    new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "ARS"
    }).format(value);




  const handleChangeCurso = (name) => (event) => {
    setCurso({ ...curso, [name]: event.target.value });


    if (name === "name") {
      setValidateNameCurso(false);
    }

    if (name === "dia") {
      setValidateDia(false);
    }

    if (name === "textoAyuda") {
      setValidateAyuda(false);
    }

    if (name === "shortName") {
      setValidateShortName(false);
    }
    if (name === "year") {




      setValidateYear(false);
    }
    if (name === "idSeccion") {
      setValidateIdSeccion(false);
    }

    

    if (name === "fechaInicio") {
      setValidateFechaInicio(false);
    }
    if (name === "fechaFinal") {
      setValidateFechaFinal(false);
    }

    if (name === "price") {


      let valor = event.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setCurso({ ...curso, ["price"]: valorParse });

      setValidatePrice(false);
    }


    if (name === "horarioInicio") {
      setValidateHorario(false);
    }

    if (name === "valorMatricular") {


      let valor = event.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setCurso({ ...curso, ["valorMatricular"]: valorParse });

      setValidateValorMatricula(false);
    }
    if (name === "valorPrimeraCuota") {
      setValidatePrimeraCuota(false);
    }
    if (name === "valorRestanteCuotas") {
      setValidateRestanteCuotas(false);
    }
    if (name === "valorDerechoExamen") {
 
      let valor = event.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setCurso({ ...curso, ["valorDerechoExamen"]: valorParse });

      
      setValidateDerechoExamen(false);


    }

    if (name === "textoAyudaExamen") {
      setValidateAyudaExamen(false);
    }
    if (name === "textoAyudaMatricula") {
      setValidateAyudaMatricula(false);
    }


    if (name === "cupos") {
      setValidateCupos(false);
    }

    if (name === "horarioFinal") {
      setValidateHorarioFinal(false);
    }

  };


  const clickSubmitCurso = (event) => {
    event.preventDefault();

    if (!curso.name) {
      setValidateNameCurso(true);
    }
    if (!curso.year) {
      setValidateYear(true);
    }
    if (!curso.fechaFinal) {
      setValidateFechaFinal(true);
    }

    if (!curso.price) {
      setValidatePrice(true);
    }

    if (!curso.textoAyudaMatricula) {
      setValidateAyudaMatricula(true);
    }



    if (!curso.textoAyudaExamen) {
      setValidateAyudaExamen(true);
    }


    if (!curso.dia) {
      setValidateDia(true);
    }

    if (!curso.shortName) {
      setValidateShortName(true);
    }

    if (!curso.textoAyuda) {
      setValidateAyuda(true);
    }

    
    if (!curso.horarioInicio) {
      setValidateHorario(true);
    }

    if (!curso.valorMatricular) {
      setValidateValorMatricula(true);
    }
    if (!curso.valorPrimeraCuota) {
      setValidatePrimeraCuota(true);
    }
    if (!curso.valorRestanteCuotas) {
      setValidateRestanteCuotas(true);
    }
    if (!curso.valorDerechoExamen) {
      setValidateDerechoExamen(true);
    }

    if (!curso.cupos) {
      setValidateCupos(true);
    }

    if (!curso.horarioFinal) {
      setValidateHorarioFinal(true);
    }

    if (!curso.fechaInicio) {
      setValidateFechaInicio(true);
    }

    if (
      curso.name &&
      curso.shortName &&
      curso.year &&
      curso.valorMatricular &&
      curso.valorDerechoExamen &&
      curso.cupos &&
      curso.dia &&
      curso.horarioInicio &&
      curso.fechaInicio &&
      curso.fechaFinal &&
      curso.horarioFinal &&
      curso.price 
    ) {


      let val = {
        "cupo": parseInt(curso.cupos,10),
        "help_price": '',
        "help_enrollment": '',
        "help_exam": '',
        "idAlumnos": [
          0
        ],
        "idEspecialidad": 0,
        "idMaterias": [
          0
        ],
        "idProfesores": [
          0
        ],
        "idSeccion": 1,
        "id_divition": 0,
        "name": curso.name,
        "price": parseFloat(parseFloat(curso.price).toFixed(2)),
        "schedules": [
          {
            "dateSince": curso.fechaInicio,
            "dateUntil": curso.fechaFinal,
            "dayOfWeek": curso.dia,
            "hourFrom": curso.horarioInicio,
            "hourTo": curso.horarioFinal
          }
        ],
        "shortName": curso.shortName,
        "valorDerechoExamen": parseFloat(parseFloat(curso.valorDerechoExamen).toFixed(2)),
        "valorMatricula": parseFloat(parseFloat(curso.valorMatricular).toFixed(2)),
        "year": parseInt(curso.year,10)
      }

console.log(val)
      create(val)
        .then(data => {
          console.log(data)

          if(data){
            if (data.ok === false) {

              toast.error("Error al agregar");
              changeToggle();
            }
            if (data.ok === true) {

              toast.success("Curso agregado");
              changeToggle();
            }   
          }else{


            toast.error("Error en la api");
            changeToggle();
          }
        
        

        })

    }

  };


  return (
    <>

      <AgregarFormularioAlumno>
        <div className="formulario__head">
          <div>
            <TextFormulario>Crear un nuevo Curso</TextFormulario>
          </div>

          <div className="delete" onClick={changeToggle}>
            <i class="fas fa-times"></i>
          </div>
        </div>

        <div className="formulario__body ">
          <form className="input-form">
            <label>Nombre</label>
            <input
              onChange={handleChangeCurso("name")}
              value={curso.name}
              type="text"
              className={
                validateNameCurso ? "form-control is-invalid" : "form-control"
              }
              placeholder="Nombre"
            />
            {validateNameCurso && (
              <p className="text-danger my-1">El nombre es obligatorio</p>
            )}

          <label>Nombre corto</label>
            <input
              onChange={handleChangeCurso("shortName")}
              value={curso.shortName}
              type="text"
              className={
                validateShortName ? "form-control is-invalid" : "form-control"
              }
              placeholder="Nombre corto"
            />
            {validateShortName && (
              <p className="text-danger my-1">El nombre es obligatorio</p>
            )}

            <label>Año</label>
            <input
              onChange={handleChangeCurso("year")}
              value={curso.year <  9999 && curso.year}
              type="number"
              className={
                validateYear ? "form-control is-invalid" : "form-control"
              }
              min="1" max="999"
              placeholder="Año"
            />
            {validateYear && (
              <p className="text-danger my-1">El año es obligatorio</p>
            )}


<label>Dia del curso</label>

<Select
              onChange={handleChangeCurso("dia")}
              value={curso.dia}
                style={{ borderColor: validateDia ? "red" : "" }}
              >
                <option value="">Seleccione un dia</option>
                <option value="MONDAY">Lunes</option>
                <option value="TUESDAY">Martes</option>
                <option value="WEDNESDAY">Miercoles</option>
                <option value="THURSDAY">Jueves</option>
                <option value="FRIDAY">Viernes</option>
                <option value="SATURDAY">Sabado</option>
                <option value="SUNDAY">Domingo</option>
              </Select>

            {validateDia && (
              <p className="text-danger my-1">El Dia es obligatorio</p>
            )}


            <label>Fecha de inicio</label>
            <input
              onChange={handleChangeCurso("fechaInicio")}
              value={curso.fechaInicio}
              type="date"
              className={
                validateFechaInicio ? "form-control is-invalid" : "form-control"
              }
              placeholder="Fecha de inicio"
            />
            {validateFechaInicio && (
              <p className="text-danger my-1">
                La fecha de inicio es obligatorio
              </p>
            )}

<label>Fecha final</label>
            <input
              onChange={handleChangeCurso("fechaFinal")}
              value={curso.fechaFinal}
              type="date"
              className={
                validateFechaFinal ? "form-control is-invalid" : "form-control"
              }
              placeholder="Fecha final"
            />
            {validateFechaFinal && (
              <p className="text-danger my-1">
                La fecha de inicio es obligatorio
              </p>
            )}


            <label>Hora de inicio</label>
            <input
              onChange={handleChangeCurso("horarioInicio")}
              value={curso.horario}
              type="time"
              className={
                validateHorario ? "form-control is-invalid" : "form-control"
              }
              placeholder="Hora de inicio"
            />
            {validateHorario && (
              <p className="text-danger my-1">
                El valor de la matricula obligatorio
              </p>
            )}
            <label>Hora final</label>
            <input
              onChange={handleChangeCurso("horarioFinal")}
              value={curso.horarioFinal}
              type="time"
              className={
                validateHorarioFinal
                  ? "form-control is-invalid"
                  : "form-control"
              }
              placeholder="Hora Final"
            />
            {validateHorarioFinal && (
              <p className="text-danger my-1">
                El valor de la matricula obligatorio
              </p>
            )}



<label>Precio</label>
            <input
              onChange={handleChangeCurso("price")}
              value={curso.price}
              type="number"
              className={
                validatePrice ? "form-control is-invalid" : "form-control"
              }
              placeholder="Precio"
            />
            {validatePrice && (
              <p className="text-danger my-1">
                El Precio es obligatorio
              </p>
            )}




            <label>Valor matricula</label>
            <input
              onChange={handleChangeCurso("valorMatricular")}
              value={curso.valorMatricular}
              type="number"
              className={
                validateValorMatricula
                  ? "form-control is-invalid"
                  : "form-control"
              }
              placeholder="Valor de matricula"
            />
            {validateValorMatricula && (
              <p className="text-danger my-1">
                El valor de la matricula obligatorio
              </p>
            )}



           
                       <label>Valor derecho de examen</label>
            <input
              onChange={handleChangeCurso("valorDerechoExamen")}
              value={curso.valorDerechoExamen}
              type="number"
              className={
                validateDerechoExamen
                  ? "form-control is-invalid"
                  : "form-control"
              }
              placeholder="Valor derecho a examen"
            />
            {validateDerechoExamen && (
              <p className="text-danger my-1">
                El valor del derecho al examen es obligatorio
              </p>
            )}



            <label>Cantidad de cupos</label>
            <input
              onChange={handleChangeCurso("cupos")}
              value={curso.cupos}
              type="number"
              className={
                validateCupos ? "form-control is-invalid" : "form-control"
              }
              placeholder="Cupos"
            />
            {validateCupos && (
              <p className="text-danger my-1">
                La cantidad de cupos obligatorio
              </p>
            )}
          </form>
        </div>

        <div className="formulario__footer">
          <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
          <ButtonCrear onClick={clickSubmitCurso}>Crear</ButtonCrear>
        </div>
      </AgregarFormularioAlumno>

    </>
  );
}

export default NuevoCurso;