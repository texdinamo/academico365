export const val = [
    
    {
      "id": 3,
      "idSeccion": 22,
      "idEspecialidad": 1,
      "idProfesores": 22,
      "idMaterias": 1,
      "idAlumnos": 22,
      "name":  "curso 3",
      "shortName": "curso 1",
      "year": 2021,
      "valorMatricula": "2000 $",
      "valorPrimeraCuota": "1000 $",
      "valorRestantesCuotas": "1000 $",
      "valorDerechoExamen": "500 $",
      "cupo": 50
    },
    {
      "id": 4,
      "idSeccion": 22,
      "idEspecialidad": 1,
      "idProfesores": 22,
      "idMaterias": 1,
      "idAlumnos": 22,
      "name":  "curso 1",
      "shortName": "curso 1",
      "year": 2021,
      "valorMatricula": "2000 $",
      "valorPrimeraCuota": "1000 $",
      "valorRestantesCuotas": "1000 $",
      "valorDerechoExamen": "500 $",
      "cupo": 50
    },
    {
      "id": 5,
      "idSeccion": 22,
      "idEspecialidad": 1,
      "idProfesores": 22,
      "idMaterias": 1,
      "idAlumnos": 22,
      "name":  "curso 2",
      "shortName": "curso 2",
      "year": 2021,
      "valorMatricula": 2000,
      "valorPrimeraCuota": 1000 ,
      "valorRestantesCuotas": 1000 ,
      "valorDerechoExamen": 2222,
      "cupo": 50
    }

  ]