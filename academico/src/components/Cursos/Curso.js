import React, { useState, useEffect } from 'react';
import Navbar from '../estructura/Navbar'
import { Link } from 'react-router-dom'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Spinner } from 'react-bootstrap'
import 'react-tabs/style/react-tabs.css';
import Moment from 'react-moment'

import SweetAlert from 'react-bootstrap-sweetalert';
import {
  AgregarFormularioAlumno, ButtonCrear, ButtonCancelar,
  TextFormulario
} from '../styled-component/Navbar'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import styled from 'styled-components'
import { Container3 } from '../styled-component/slidebar'
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { ToastContainer, toast } from 'react-toastify'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import 'react-toastify/dist/ReactToastify.min.css'
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import ReactTooltip from 'react-tooltip';
import { getCursoId,addStudent,updateCurso, addMaterias, addDocentes } from './apiCursos'
import {dia,obtenerFecha} from '../../helpers/cursos'
import {getAlumnos,getAlumnoId} from '../Alumnos/apiAlumnos'
import {getMaterias, getMateriaId} from '../Materias/apiMaterias'
import ModificarPrecio from './Curso/ModificarPrecio' 
import {  getTutores } from '../Tutores/apiTutores'
import {getProfesores,getProfesorId} from '../Profesores/apiProfesores'

const ButtonAgregar = styled.button`
width: 143px;
height: 42px;
border-radius: 6px;
background-color: #2873B7;
font-size: 11px;
line-height: 15px;
color: #ffffff;
font-weight: 900;
font-family: "Lato";
cursor: pointer;
align-self: center;
`

const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`;

const Curso = (props) => {
  const { SearchBar } = Search;
  const [alumnoEspecial,setAlumnoEspecial] = useState()
  const localizer = momentLocalizer(moment)
  const [confirmar, setConfirmar] = useState(false)
  const [eliminar, setEliminar] = useState(false)
  const [alumno, setAlumno] = useState()
  const [alumnoId,setAlumnoId] = useState()
  const [listaAlumnos, setListaAlumnos] = useState()
  const [toggle, setToggle] = useState(false)
  const [allAlumnos, setAllAlumnos] = useState()
  const [toggleAlumno, setToggleAlumno] = useState(false)
  const [detalleAlumno, setDetalleAlumno] = useState()
  const [toggleModificar, setModificar] = useState(false)
  const [toggleModificarHorario, setModificarHorario] = useState(false)
  const [agregarPlan, setAgregarPlan] = useState(false)
  
  
  const [allMaterias,setAllMaterias] = useState()
  const [materiaDetalle,setMateriaDetalle] = useState()
  const [toggleMaterias,setToggleMaterias] = useState(false)
  const [toggleMateriaId,setToggleMateriaId] = useState(false)
  const [materiaId,setMateriaId] = useState()
  const changeToggleMaterias = () => setToggleMaterias(prevLogin => !prevLogin)
  const changeToggleMateriaId = () => setToggleMateriaId(prevLogin => !prevLogin)

  const [docenteId,setDocenteId] = useState(false)
  const [docenteDetalle,setDocenteDetalle] = useState()
  const [toggleDocentes,setToggleDocentes] = useState(false)
  const [toggleDocenteId,setToggleDocenteId] = useState(false)
  const [allDocentes,setAllDocentes] = useState()




  const changeToggleDocentes = () => setToggleDocentes(prevLogin => !prevLogin)
  const changeToggleDocenteId = () => setToggleDocenteId(prevLogin => !prevLogin)

  const changeToggle = () => setToggle(prevLogin => !prevLogin)
  const changeTogglePlan = () => setAgregarPlan(prevLogin => !prevLogin)
  const changeToggleModificar = () => setModificar(prevLogin => !prevLogin)
  const changeToggleModificarHorario = () => setModificarHorario(prevLogin => !prevLogin)
  const changeToggleAlumno = () => setToggleAlumno(prevLogin => !prevLogin)
  const changeEliminar = () => setEliminar(prevLogin => !prevLogin)
  
  const changeConfirmar = () => setConfirmar(prevLogin => !prevLogin)
  const numberFormat = value =>
    new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "ARS"
    }).format(value);

let idEspecial 

  useEffect(() => {

  

    getAlumnos().then(data => {
      setAllAlumnos(data)
      
    })

    getTutores().then(data => {
      console.log('todos los tutores',data)
    })

    getMaterias().then(data => {
      
      setAllMaterias(data)
      
    })

    getProfesores().then(data => {
      console.log(data)
      let result = []
      for(let val of data){
        result.push(val.user)
      } 
      setAllDocentes(result)
    })

    
    


    if(alumnoId){

      console.log(alumnoId)
      getAlumnoId(alumnoId.idAlumno).then(data => {

        setDetalleAlumno(data)
      
  })

    }

    if(materiaId){ 

      getMateriaId(materiaId).then(data => {
        console.log('data de materia',data)
        setMateriaDetalle(data)
      
  })
    }


    if(docenteId){
      getProfesorId(docenteId.idTeacher).then(data => {
        console.log('data de materia',data.user)
      setDocenteDetalle(data.user)
      
  })
    }


  
    

    if(props.location.state.fromDashboard){
      getCursoId(props.location.state.fromDashboard).then(data => {
        console.log(data)
        setAlumno(data)
      })
  
    }



  }, [alumnoId,alumnoEspecial,materiaId,docenteId])

  const parseTime = (time) => {
    let timeInt = parseInt(time);
    let minutes = time.substring(3, 5);
    // you could then add or subtract time here as needed
    if (time > '12:00') {
      return `${timeInt - 12}:${minutes} PM`;
    } else {
      return `${timeInt}:${minutes} AM`;
    }
  }


///////DOCENTES

const columnsDocentes = [

  {dataField: "idTeacher", text: 'Id',sort: true,  searchable: true,

  sortCaret: (order, column) => {
    if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
    else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
    else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
    return null;
  }
},
  {dataField: "name", text: 'Nombre',sort: true,  searchable: true,
  sortCaret: (order, column) => {
    if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
    else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
    else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
    return null;
  }

},
{dataField: "surname", text: 'Apellido',sort: true,  searchable: true,
  sortCaret: (order, column) => {
    if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
    else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
    else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
    return null;
  }

},

  
{
  dataField: "link",
  text: "Accion",
  formatter: (rowContent, row) => {
    return (
      <>

        
          <i  onClick={changeToggleDocenteId} className="far fa-eye"></i>
      

        <i
          className="far fa-trash-alt ml-3 text-danger"
          onClick={changeEliminar}

        ></i>
      </>
    );
  },
}
]


const rowEventsDocentes = {
  onClick: (e, row, rowIndex) => {
   setDocenteId(row)
    console.log(row)
  },
  onMouseEnter: (e, row, rowIndex) => {
    
    setDocenteId(row)

  }

};

  /////MATERIAS
  const columnsMaterias = [

    {dataField: "id", text: 'Id',sort: true,  searchable: true,
  
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  },
    {dataField: "name", text: 'Materia',sort: true,  searchable: true,
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  
  },

    
  {
    dataField: "link",
    text: "Accion",
    formatter: (rowContent, row) => {
      return (
        <>

          
            <i  onClick={changeToggleMateriaId} className="far fa-eye"></i>
        

          <i
            className="far fa-trash-alt ml-3 text-danger"
            onClick={changeEliminar}

          ></i>
        </>
      );
    },
  }
  ]

  const rowEventsMaterias = {
    onClick: (e, row, rowIndex) => {
     setMateriaId(row.id)
      console.log(row)
    },
    onMouseEnter: (e, row, rowIndex) => {
     
      setMateriaId(row.id)

    }
  
  };


  const optionsMaterias = {
    paginationSize: 3,
    pageStartIndex: 0,
    withFirstAndLast: false,
    firstPageText: 'First',
    prePageText: 'Anterior',
    nextPageText: 'Siguiente',
  
    showTotal: false,
    disablePageTitle: true
  }
   


  //////ALUMNOS
  const columnsAlumnos = [
    {
      dataField: "idAlumno", text: 'id', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
    {
      dataField: "name", text: 'Nombre', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }

    },

    {
      dataField: "surname", text: 'Apellido', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
    
    {
      dataField: 'link',
      text: 'Accion',
      formatter: (rowContent, row) => {
        return (
          <>
            <i onClick={changeToggleAlumno} className="far fa-eye"></i>
            <i onClick={changeEliminar} className="far fa-trash-alt ml-3 text-danger" ></i>
          </>
        )
      }
    }
  ]

  const columns = [
    { dataField: "id", text: 'id' },
    { dataField: "name", text: 'Nombre' },
    { dataField: "surname", text: 'Apellido' },
    {
      dataField: 'link',
      text: 'ACTION',
      formatter: (rowContent, row) => {
        console.log(row)
        return (

          <>
            <i onClick={e => addAlumno(row.id)} className="text-success fas fa-plus-circle"></i>
          </>
        )
      }
    }
  ]


  //adddocentes

  const columnsAddDocentes = [
    { dataField: "id", text: 'id' },
    { dataField: "name", text: 'Nombre' },
    { dataField: "surname", text: 'Apellido' },
   
    {
      dataField: 'link',
      text: 'ACTION',
      formatter: (rowContent, row) => {
        console.log('..............',row)
        return (

          <>
            <i onClick={e => agregarDocente(row.id)} className="text-success fas fa-plus-circle"></i>
          </>
        )
      }
    }
  ]


  //addmaterias

  const columnsAddMaterias = [
    { dataField: "id", text: 'id' },
    { dataField: "name", text: 'Nombre' },
    {
      dataField: 'link',
      text: 'ACTION',
      formatter: (rowContent, row) => {
        console.log(row)
        return (

          <>
            <i onClick={e => agregarMateria(row.id)} className="text-success fas fa-plus-circle"></i>
          </>
        )
      }
    }
  ]

  

  const options = {
    paginationSize: 3,
    pageStartIndex: 0,
    withFirstAndLast: false,
    firstPageText: "First",
    prePageText: "Anterior",
    nextPageText: "Siguiente",
    showTotal: false,
    disablePageTitle: true,
  };  

  const cambio = () => {
    setEliminar(false)
    setConfirmar(true)
  }

  const clickSubmit = event => {
    event.preventDefault()
    toast.success("Precio modificado correctamente!")
    changeToggleModificar()
  }


  const clickSubmitHorario = event => {
    event.preventDefault()

    toast.success("Horario correctamente!")
    changeToggleModificarHorario()
  }


  

  const addAlumno = async (idEstudent) => {
    console.log(alumnoEspecial)

      let result = await addStudent(props.location.state.fromDashboard,idEstudent)

      console.log(result)
    if(result){

        if(result.ok === true){

          toast.success("Alumno añadido correctamente!")
          changeToggle()
        }

        if(result.ok === false){

          toast.error("Error al añadir")
          changeToggle()
        }
     
    }
    

  }



  const agregarMateria =  async (idMateria) => {

  
    let result = await addMaterias(props.location.state.fromDashboard,idMateria)

    console.log(result)
  if(result){

      if(result.ok === true){

        toast.success("Materia añadida correctamente!")
        changeToggleMaterias()
      }

      if(result.ok === false){

        toast.error("Error al añadir")
        changeToggle()
      }
   
  }

  }




  const agregarDocente =  async (idMateria) => {


    let result = await addDocentes(props.location.state.fromDashboard,idMateria)

    console.log(result)
  if(result){

      if(result.ok === true){

        toast.success("Docente asignado correctamente!")
        changeToggleDocentes()
      }

      if(result.ok === false){

        toast.error("Error al añadir")
        changeToggleDocentes()
      }
   
  }

  }




        let myEventsList = [{
          title: "fecha de clase",

          start: Date(),
          end: Date()
        }]

   if(alumno){

    let result = []
    for( let fecha of obtenerFecha(alumno.schedules[0].dateSince,alumno.schedules[0].dateUntil,alumno.schedules[0].dayOfWeek) ){

        let objeto = {
      title: alumno.name,
       allDay: true,  
      start: moment(fecha).toDate(),
      end: moment(fecha).toDate()
        }

        result.push(objeto)

    }

  console.log(result)
   myEventsList = result
  }


  const rowEvents = {
    onClick: (e, row, rowIndex) => {
      console.log(row,e)
      setAlumnoId(row);
    },
    onMouseEnter: (e, row, rowIndex) => {
      setAlumnoId(row);
      console.log(row)
    },
  };

  const rowEventsEspecial = {
    onClick: (e, row, rowIndex) => {
     console.log(row)
      setAlumnoEspecial(row.id);
    },
    onMouseEnter: (e, row, rowIndex) => {
      console.log(row)
      setAlumnoEspecial(row.id);
    },
  };


  return (
    <>
      <Navbar />
      <div className="dashboard">
        <ToastContainer />
        {toggle && <AgregarShadow onClick={changeToggle} />}
        {toggleModificar && <AgregarShadow onClick={changeToggleModificar} />}
        {toggleModificarHorario && <AgregarShadow onClick={changeToggleModificarHorario} />}
        {toggleAlumno && <AgregarShadow onClick={changeToggleAlumno} />}
        {toggleMaterias && <AgregarShadow onClick={changeToggleMaterias} />}
      {toggleMateriaId && <AgregarShadow onClick={changeToggleMateriaId} />}
      {toggleDocentes && <AgregarShadow onClick={changeToggleDocentes} /> }
      {toggleDocenteId && <AgregarShadow onClick={changeToggleDocenteId} /> }

        {toggleModificarHorario &&
          <>
            <AgregarFormularioAlumno >
              <div className="formulario__head">
                <div>
                  <TextFormulario>Modificar Horario</TextFormulario>
                </div>
                <div className="delete" onClick={changeToggleModificarHorario}><i class="fas fa-times"></i></div>
              </div>
              <div className="formulario__body ">
                <div>
                  <form className="input-form">
                    <label>Horario</label>
                    <input type="date" className="input" placeholder="valor de matricula" />
                  </form>
                </div>
                <div className="formulario__footer">
                  <ButtonCancelar onClick={changeToggleModificarHorario}>
                    Cancelar
                  </ButtonCancelar>
                  <ButtonCrear onClick={clickSubmitHorario} >
                    Modificar
                  </ButtonCrear>
                </div>
              </div>
            </AgregarFormularioAlumno>
          </>
        }


        {
          toggleModificar &&
          
      <ModificarPrecio changeToggle={changeToggleModificar} alumnoId={props.location.state.fromDashboard}  alumno={alumno} />
       
       }



{toggleMateriaId &&


<Container3 className="container">

<div className=" d-flex justify-content-between header-descripcion  mb-3">
<p className="info-descripcion" >Detalles de Materia</p>
<div className="delete" onClick={changeToggleMateriaId}><i class="fas fa-times mr-2"></i></div>    
</div>
<div className="datos-usuario descripcion d-flex">

<div>

<div className="d-flex" >
<h5 className="info-descripcion">Nombre:</h5>
<p>{materiaDetalle.name}</p>

</div>

<div className="d-flex" >
<h5 className="info-descripcion">Notas:</h5>
<p>{materiaDetalle.metadata}</p>
</div>


<div className="d-flex" >
<h5 className="info-descripcion">Fecha de baja:</h5>
<p>{materiaDetalle.fecha_baja ?  materiaDetalle.fecha_baja.split('T')[0] : null}</p>
</div>
</div>
</div>
</Container3>

}

{toggleDocenteId &&


<Container3 className="container Profesor">

<div className=" d-flex justify-content-between header-descripcion  mb-3">
<p className="info-descripcion" >Detalles de Docente</p>
<div className="delete" onClick={changeToggleDocenteId}><i class="fas fa-times mr-2"></i></div>    
</div>

<div className="datos-usuario d-flex">

<div>
<div className="d-flex" >
<h5 className="info-descripcion mt-1">id:</h5>  <p>{docenteDetalle.id}</p>
</div>

<div className="d-flex" >
<h5 className="info-descripcion mt-1">Nombre:</h5>
<p>{docenteDetalle.name}</p>

</div>

<div className="d-flex" >
<h5 className="info-descripcion mt-1">Apellido:</h5>
<p>{docenteDetalle.surname}</p>

</div>


<div className="d-flex" >

<h5 className="info-descripcion mt-1">Dni:</h5>
<p>{docenteDetalle.documentation}</p>

</div>


<div className="d-flex" >

<h5 className="info-descripcion mt-1">Document:</h5>
<p>{docenteDetalle.documentation}</p>

</div>

<div className="d-flex" >
<h5 className="info-descripcion  mt-1">Fecha de nacimiento:</h5>

<Moment format="YYYY/MM/DD">
<p>{docenteDetalle.birthDate}</p>
     </Moment>
</div>

<div className="d-flex" >
<h5 className="info-descripcion mt-1">Sexo:</h5>
<p>{docenteDetalle.gender}</p>

</div>
</div>

<div className="mr-4 ml-4">

<div className="d-flex" >
<h5 className="info-descripcion mt-1">Nacionalidad:</h5>
<p>{docenteDetalle.nacionality}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion mt-1">Domicilio:</h5>
<p>{docenteDetalle.address}</p>

</div>



<div className="d-flex" >
<h5 className="info-descripcion mt-1">Barrio:</h5>
<p>{docenteDetalle.neighborhood}</p>

</div>

<div className="d-flex" >
<h5 className="info-descripcion mt-1">Ciudad:</h5>
<p>{docenteDetalle.city}</p>

</div>



<div className="d-flex" >
<h5 className="info-descripcion mt-1">Pais:</h5>
<p>{docenteDetalle.country}</p>

</div>




<div className="d-flex" >
<h5 className="info-descripcion mt-1">Provincia:</h5>
<p>{docenteDetalle.province}</p>

</div>
</div>


<div>

<div className="d-flex" >
<h5 className="info-descripcion mt-1">Cod.postal:</h5>
<p>{docenteDetalle.zipCode}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion mt-1">email:</h5>
<p>{docenteDetalle.email}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion mt-1">Telefono:</h5>
<p>{docenteDetalle.phone}</p>

</div>




</div>
<div>
<div className="d-flex" >
<h5 className="info-descripcion mt-1">fecha de alta:</h5>
<p>{docenteDetalle.fecha_alta}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion mt-1">fecha de baja:</h5>
<p>{docenteDetalle.fecha_baja}</p>

</div>


</div>

</div>


</Container3>

}



{toggleAlumno && 
<Container3 className="container">

<div className=" d-flex justify-content-between header-descripcion  mb-3">
<p className="info-descripcion" >Detalles de Alumno</p>
<div className="delete" onClick={changeToggleAlumno}><i class="fas fa-times mr-2"></i></div>    
</div>
<div className="datos-usuario descripcion d-flex">

<div>
<div className="d-flex" >
<h5 className="info-descripcion">Legajo:</h5>  <p>{detalleAlumno.docket}</p>
</div>

<div className="d-flex" >
<h5 className="info-descripcion">Nombre:</h5>
<p>{detalleAlumno.name}</p>

</div>

<div className="d-flex" >
<h5 className="info-descripcion">Apellido:</h5>
<p>{detalleAlumno.surname}</p>

</div>


<div className="d-flex" >

<h5 className="info-descripcion">Dni:</h5>
<p>{detalleAlumno.document}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion">Fecha de nacimiento:</h5>

<Moment format="YYYY/MM/DD">
<p>{detalleAlumno.birthDate}</p>
     </Moment>
</div>

<div className="d-flex" >
<h5 className="info-descripcion">Sexo:</h5>
<p>{detalleAlumno.gender}</p>

</div>
</div>

<div className="mx-4 ">

<div className="d-flex " >
<h5 className="info-descripcion">Nacionalidad:</h5>
<p>{detalleAlumno.nationality}</p>

</div>

<div className="d-flex" >
<h5 className="info-descripcion">Tipo de sangre:</h5>
<p>{detalleAlumno.bloodtype}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion">Domiciolio:</h5>
<p>{detalleAlumno.address}</p>

</div>



<div className="d-flex" >
<h5 className="info-descripcion">Barrio:</h5>
<p>{detalleAlumno.neighborhood}</p>

</div>

<div className="d-flex" >
<h5 className="info-descripcion">Ciudad:</h5>
<p>{detalleAlumno.city}</p>

</div>



<div className="d-flex" >
<h5 className="info-descripcion">Pais:</h5>
<p>{detalleAlumno.country}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion">Barrio:</h5>
<p>{detalleAlumno.neighborhood}</p>
</div>


<div className="d-flex" >
<h5 className="info-descripcion">Provincia:</h5>
<p>{detalleAlumno.province}</p>

</div>
</div>


<div>

<div className="d-flex" >
<h5 className="info-descripcion">Codigo postal:</h5>
<p>{alumno.zipCode}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion">email:</h5>
<p>{detalleAlumno.email}</p>

</div>


<div className="d-flex" >
<h5 className="info-descripcion">Telefono:</h5>
<p>{detalleAlumno.phoneNumber}</p>

</div>



<div className="d-flex" >
<h5 className="info-descripcion">Nivel de estudio:</h5>
<p>{detalleAlumno.studyLevel}</p>

</div>

</div>


</div>


</Container3>}




        {eliminar &&
          <SweetAlert
            primary
            showCancel
            cancelBtnBsStyle="secondary"
            style={{ fontSize: '12px', fontWeight: '700', width: '500px', height: '240px' }}
            onConfirm={() => cambio()}
            onCancel={() => setEliminar(false)}
            confirmBtnText="Aceptar"
            title="Estas seguro de realizar esta accion?"
            focusCancelBtn
          >
            <p style={{ fontSize: '14px', margin: '5px' }} >Esta apunto de realizar una accion irreversible</p>
            <p style={{ fontSize: '14px', marginBottom: '20px' }} >Confirma tu accion</p>
          </SweetAlert>
        }

        {confirmar &&
          <SweetAlert 
          danger
          title="Error"
          onConfirm={() => changeConfirmar()} >
          </SweetAlert>
        
        }

        {alumno ?
          <Tabs>
            <TabList>
              <Tab>Informacion</Tab>
              <Tab>Alumnos</Tab>
              <Tab>Precio</Tab>
              <Tab>Horarios</Tab>
              <Tab>Docentes</Tab>
              <Tab>Materias</Tab>
            </TabList>
            <TabPanel className="alumno">
              <div className="header-descripcion mb-4">Informacion</div>
              <div className="datos-usuario d-flex">
                <div className="datos">
                  <div className="d-flex" >
                    <h5 className="info-descripcion">Nombre:</h5>
                    <p>{alumno.name}</p>
                  </div>
                  <div className="d-flex" >
                    <h5 className="info-descripcion">Nombre corto:</h5>
                    <p>{alumno.shortName}</p>
                  </div>
                  <div className="d-flex" >
                    <h5 className="info-descripcion">Año:</h5>
                    <p>{alumno.year}</p>
                  </div>
                </div>
                <div>
                </div>
              </div>
            </TabPanel >
            <TabPanel className="alumno">
              <div className="header-descripcion d-flex justify-content-between">
                <div>
                  <p>Cursos Alumnos   <span className="ml-3 cupos">Cupos {alumno.cupo}</span></p>
                </div>
                <div>
                  <ButtonAgregar onClick={changeToggle} >Añadir Alumno</ButtonAgregar>
                </div>
              </div>


<BootstrapTable
        keyField="idAlumno"
        data={alumno.alumnos}
        columns={columnsAlumnos}
        pagination={paginationFactory(options)}
        rowEvents={rowEvents}

        />


              {toggle && 
<Container3>

<div className="mx-2 my-2 d-flex justify-content-between ">
<p className="info-descripcion" >Agregar Alumno</p>
<div className="delete " onClick={changeToggle}><i class="mr-2 fas fa-times"></i></div>    
</div>
<ToolkitProvider
  keyField="id"
  data={ allAlumnos}
  columns={columns}
  search
>
  {
    props => (
        <>
      <div className="m-3 search-alumno">

        <SearchBar
        
        placeholder="Buscar Alumno"
        { ...props.searchProps } />
    
    </div>
        <BootstrapTable
        
          { ...props.baseProps }
          rowEvents={rowEventsEspecial}
  pagination={paginationFactory(options)}
        />
        </>
      
    )
  }
</ToolkitProvider>

</Container3>}
            </TabPanel>

            <TabPanel className="alumno">
              <div className="header-descripcion d-flex justify-content-between">
                <p>Cursos Precios</p>
                <ButtonAgregar onClick={changeToggleModificar} >Modificar Precios</ButtonAgregar>
              </div>
              <div className="datos-usuario d-flex">
                <div className="datos">
                <div className="d-flex" >
                    <a data-tip data-for='sadFace'>
                      <i class="fas fa-question-circle"></i>
                    </a>
                    <ReactTooltip id='sadFace' type='dark' place="right" effect='solid'>
                    
                      <span >Consignar en este campo el precio de lista de la primera cuota para planes de Montos Periódico (si el valor del mismo es distinto del precio de lista de las restantes) o bien valor del curso completo para planes de Monto Único Financiado.</span>
                    </ReactTooltip>
                    <h5 className="info-descripcion">Precio del curso:</h5>
                    <p> $ {numberFormat(alumno.price)}</p>
                  </div>
                  

                  <div className="d-flex" >
                    <a data-tip data-for='sadFace2'>
                      <i class="fas fa-question-circle"></i>
                    </a>
                    <ReactTooltip id='sadFace2' type='dark' place="right" effect='solid'>
                      <span>Consignar en este campo el precio de lista de la primera cuota para planes de Montos Periódico (si el valor del mismo es distinto del precio de lista de las restantes) o bien valor del curso completo para planes de Monto Único Financiado.</span>
                    </ReactTooltip>
                    <h5 className="info-descripcion">Matricula:</h5>
                    <p> $ {numberFormat(alumno.valorMatricula)}</p>
                  </div>
                  <div className="d-flex" >
                    <a data-tip data-for='sadFace3'>
                      <i class="fas fa-question-circle"></i>
                    </a>
                    <ReactTooltip id='sadFace3' type='dark' place="right" effect='solid'>
                      <span >Consignar en este campo el precio de lista de la primera cuota para planes de Montos Periódico (si el valor del mismo es distinto del precio de lista de las restantes) o bien valor del curso completo para planes de Monto Único Financiado.</span>
                    </ReactTooltip>
                    <h5 className="info-descripcion">Derecho de examen:</h5>
                    <p> $ {numberFormat(alumno.valorDerechoExamen)}</p>
                  </div>
                </div>
              </div>
            </TabPanel>
            <TabPanel className="alumno">
              <div className="header-descripcion d-flex justify-content-between">
                <p>Cursos Horarios</p>
                <ButtonAgregar onClick={changeToggleModificarHorario} >Modificar Horario</ButtonAgregar>
              </div>
              <div className="datos-usuario horario col-12 d-flex">
                <div className="col-9 p-4">
                  <Calendar
                    localizer={localizer}
                    events={myEventsList}
                    startAccessor="start"
                    endAccessor="end"
                    style={{ height: 500 }}
                  />
                </div>
                <div className="col-3 datos-horarios p-4">

                   <p>{`Inicio de curso ${alumno.schedules[0].dateSince}`}</p>
                   <p>{`Fin del curso ${alumno.schedules[0].dateUntil}`}</p>

              <p>  {
                
             `Todos los ${dia(alumno.schedules[0].dayOfWeek)}`
                
                }</p>
                      <p>{parseTime(alumno.schedules[0].hourFrom)} - {parseTime(alumno.schedules[0].hourTo)}</p>
                </div>
              </div>
            </TabPanel>
                        <TabPanel className="alumno">
              <div className="header-descripcion d-flex justify-content-between">
                <p>Docentes</p>
                <ButtonAgregar onClick={changeToggleDocentes}>Agregar Docente</ButtonAgregar>
              </div>


                      {toggleDocentes && 
        <Container3>

        <div className="mx-2 my-2 d-flex justify-content-between ">
        <p className="info-descripcion" >Agregar Docentes</p>
        <div className="delete " onClick={changeToggleDocentes}><i class="mr-2 fas fa-times"></i></div>    
        </div>

        <ToolkitProvider
  keyField="id"
  data={ allDocentes}
  columns={columnsAddDocentes}
  search
>
  {
    props => (
        <>
      <div className="m-3 search-alumno">

        <SearchBar
        
        placeholder="Buscar Docente"
        { ...props.searchProps } />
    
    </div>
        <BootstrapTable
        
          { ...props.baseProps }
          rowEvents={rowEventsEspecial}
  pagination={paginationFactory(options)}
        />
        </>
      
    )
  }
</ToolkitProvider>


        
        </Container3>}




          <BootstrapTable
        keyField="id"
        data={alumno.docentes}
        columns={columnsDocentes}
        pagination={paginationFactory(optionsMaterias)}
        rowEvents={rowEventsDocentes}

        />

              
            </TabPanel>
            <TabPanel className="alumno">
              <div className="header-descripcion d-flex justify-content-between">
                <p>Materias</p>
                <ButtonAgregar onClick={changeToggleMaterias} >Agregar Materia</ButtonAgregar>
              </div>
              
              <BootstrapTable
        keyField="id"
        data={alumno.materias}
        columns={columnsMaterias}
        pagination={paginationFactory(optionsMaterias)}
        rowEvents={rowEventsMaterias}

        />


{toggleMaterias && 
<Container3>

<div className="mx-2 my-2 d-flex justify-content-between ">
<p className="info-descripcion" >Agregar Materia</p>
<div className="delete " onClick={changeToggleMaterias}><i class="mr-2 fas fa-times"></i></div>    
</div>
<ToolkitProvider
  keyField="id"
  data={ allMaterias}
  columns={columnsAddMaterias}
  search
>
  {
    props => (
        <>
      <div className="m-3 search-alumno">

        <SearchBar
        
        placeholder="Buscar Materia"
        { ...props.searchProps } />
    
    </div>
        <BootstrapTable
        
          { ...props.baseProps }
          rowEvents={rowEventsEspecial}
  pagination={paginationFactory(options)}
        />
        </>
      
    )
  }
</ToolkitProvider>

</Container3>}





              
            </TabPanel>
          </Tabs>
          :

          <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
            <Spinner animation="grow" variant="primary" size="xl" />
          </div>
        }
      </div>
    </>
  );
}

export default Curso;