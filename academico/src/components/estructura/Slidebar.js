import React, { useState, useEffect } from "react";
import { withRouter, Link ,useHistory } from "react-router-dom";
import {
  Container,
  ContainerMenu,
  DialogShadow,
  ImgLogoPrincipal,
  MenuAlumnos,
  ImagenFinal,
  TitleIcon,
} from "../styled-component/slidebar";
import logo from "../../img/logo.svg";
import logout from "../../img/logout.svg";
import UserInfo from "./UserInfo";

const SliderBar = (props) => {

  let history = useHistory()

    console.log('hola slidebar props',props)


  const [toggleEscritorio, setToggleEscritorio] = useState(false);
  const [toggleAlumnos, setToggleAlumnos] = useState(false);
  const [toggleProfesores, setToggleProfesores] = useState(false);
  const [toggleTutores, setToggleTutores] = useState(false);
  const [toggleCursos, setToggleCursos] = useState(false);
  const [toggleMaterias, setToggleMaterias] = useState(false);
  const [togglePerfil, setTogglePerfil] = useState(false);
  const [toggleAdministracion, setToggleAdministracion] = useState(false);
  const [valMenu, setValMenu] = useState('');

  const [ruta, setRuta] = useState('/dashboard');

  useEffect(() => {
    

    console.log(    
      toggleEscritorio ,
      toggleAlumnos ,
      toggleProfesores ,
      toggleTutores ,
      toggleCursos ,
      toggleMaterias,
            togglePerfil,
      toggleAdministracion)

    if (
      !toggleEscritorio &&
      !toggleAlumnos &&
      !toggleProfesores &&
      !toggleTutores &&
      !toggleCursos &&
      !toggleMaterias &&
      !togglePerfil &&
      !toggleAdministracion
    ) {


    console.log(    
      toggleEscritorio ,
      toggleAlumnos ,
      toggleProfesores ,
      toggleTutores ,
      toggleCursos ,
      toggleMaterias,
            togglePerfil,
      toggleAdministracion)
      setRuta(history.location.pathname);
    }
  }, [
    ruta,
    toggleEscritorio,
    toggleAlumnos,
    valMenu,
    toggleProfesores,
    toggleTutores,
    togglePerfil,
    toggleCursos,
    toggleMaterias,
    toggleAdministracion,
  ]);

  const changeToggleEscritorio = () => {
    setToggleEscritorio((prevLogin) => !prevLogin);
    setValMenu("escritorio");
    if (!toggleEscritorio) {
      setRuta("");
      setToggleAlumnos(false);
      setToggleTutores(false);
      setToggleCursos(false);
      setToggleProfesores(false);
      setToggleMaterias(false);
      setTogglePerfil(false);
      setToggleAdministracion(false);
    }
  };
  const changeHome = () => {
    setValMenu("escritorio");
  };

  const changeToggleAlumnos = () => {
    setToggleAlumnos((prevLogin) => !prevLogin);
    history.push({
      state: {
        dashboard: false
      }
    })

    setValMenu("alumnos");
    if (!toggleAlumnos) {
      setRuta("");
      setToggleEscritorio(false);
      setToggleProfesores(false);
      setToggleTutores(false);
      setToggleCursos(false);
      setToggleMaterias(false);
      setTogglePerfil(false);

      setToggleAdministracion(false);

    
    }


    
  };

  const changeToggleCursos = () => {
    setToggleCursos((prevLogin) => !prevLogin);
    history.push({
      state: {
        dashboard: false
      }
    })
    setValMenu("cursos");
    if (!toggleCursos) {
      setRuta("");
      setToggleEscritorio(false);
      setToggleProfesores(false);
      setToggleTutores(false);
      setToggleAlumnos(false);
      setToggleMaterias(false);
      setToggleAdministracion(false);
      setTogglePerfil(false);
    }
  };

  const changeToggleTutores = () => {
    setToggleTutores((prevLogin) => !prevLogin);
    history.push({
      state: {
        dashboard: false
      }
    })
    setValMenu("tutores");
    if (!toggleTutores) {
      setRuta("");
      setToggleEscritorio(false);
      setToggleProfesores(false);
      setToggleAlumnos(false);
      setToggleCursos(false);
      setToggleMaterias(false);
      setTogglePerfil(false);
      setToggleAdministracion(false);
    }
  };

  const changeToggleProfesores = () => {
    setToggleProfesores((prevLogin) => !prevLogin);
    history.push({
      state: {
        dashboard: false
      }
    })
    setValMenu("docentes");
    if (!toggleProfesores) {
      setRuta("");
      setToggleEscritorio(false);
      setToggleAlumnos(false);
      setToggleTutores(false);
      setToggleCursos(false);
      setToggleMaterias(false);
      setTogglePerfil(false);

      setToggleAdministracion(false);
    }
  };

  const changeTogglePerfil = () => {
    setTogglePerfil((prevLogin) => !prevLogin);
    history.push({
      state: {
        dashboard: false
      }
    })
    setValMenu("perfil");
    if (!togglePerfil) {
      setRuta("");
      setToggleEscritorio(false);
      setToggleAlumnos(false);
      setToggleTutores(false);
      setToggleCursos(false);
      setToggleProfesores(false);
      setToggleMaterias(false);

      setToggleAdministracion(false);
    }
  };

  const changeToggleMaterias = () => {
    setToggleMaterias((prevLogin) => !prevLogin);
    history.push({
      state: {
        dashboard: false
      }
    })
    setValMenu("materias");
    if (!toggleMaterias) {
      setRuta("");
      setToggleEscritorio(false);
      setToggleAlumnos(false);
      setToggleTutores(false);
      setToggleCursos(false);
      setToggleProfesores(false);
      setTogglePerfil(false);

      setToggleAdministracion(false);
    }
  };

  const changeToggleAdministracion = () => {
    setToggleAdministracion((prevLogin) => !prevLogin);
    history.push({
      state: {
        dashboard: false
      }
    })
    setValMenu("administracion");
    if (!toggleAdministracion) {
      setRuta("");
      setToggleEscritorio(false);
      setToggleAlumnos(false);
      setToggleTutores(false);
      setToggleCursos(false);
      setToggleProfesores(false);
      setTogglePerfil(false);
      setToggleMaterias(false);
    }
  };

  const handleClick = () => {
    setToggleEscritorio(false);
    setToggleAlumnos(false);
    setToggleProfesores(false);
    setToggleTutores(false);
    setToggleCursos(false);
    setToggleMaterias(false);
    setTogglePerfil(false);
    setToggleAdministracion(false);
  };

  return (
    <>
      {toggleAlumnos && (
        <UserInfo handleClick={handleClick} valorMenu={valMenu} />
      )}
      {toggleProfesores && (
        <UserInfo handleClick={handleClick} valorMenu={valMenu} />
      )}
      {toggleTutores && (
        <UserInfo handleClick={handleClick} valorMenu={valMenu} />
      )}
      {toggleCursos && (
        <UserInfo handleClick={handleClick} valorMenu={valMenu} />
      )}
      {toggleMaterias && (
        <UserInfo handleClick={handleClick} valorMenu={valMenu} />
      )}
      {togglePerfil && (
        <UserInfo handleClick={handleClick} valorMenu={valMenu} />
      )}
      {toggleAdministracion && (
        <UserInfo handleClick={handleClick} valorMenu={valMenu} />
      )}

      {  props.location.pathname === '/' || props.location.pathname === '/crear-cuenta' || props.location.pathname === '/reset-password' ?

        (null) :
        (
          <Container>
            <ContainerMenu>
              <ImgLogoPrincipal src={logo} />

              <div className="menues">
                <Link className="escritorio" to="/dashboard" onClick={changeHome}>
                  <MenuAlumnos
                    className="escritorio"
                    borderSelection={
                      toggleEscritorio || ruta === "/dashboard" || props.location.state && props.location.state.dashboard === true
                        ? "3px solid  #009DDC"
                        : "3px solid #FFFF"
                    }
                    onClick={changeToggleEscritorio}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="25.474"
                      height="25.474"
                      viewBox="0 0 25.474 25.474"
                    >
                      <g id="dashboard" transform="translate(0 0)">
                        <path
                          id="Trazado_86"
                          data-name="Trazado 86"
                          d="M9.818,8.492H1.857A1.86,1.86,0,0,1,0,6.634V1.858A1.859,1.859,0,0,1,1.857,0H9.818a1.86,1.86,0,0,1,1.858,1.857V6.634A1.86,1.86,0,0,1,9.818,8.492Zm-7.961-6.9a.266.266,0,0,0-.265.265V6.634a.266.266,0,0,0,.265.265H9.818a.266.266,0,0,0,.265-.265V1.858a.266.266,0,0,0-.265-.265Zm0,0"
                          fill={
                            toggleEscritorio || ruta === "/dashboard" || props.location.state && props.location.state.dashboard === true
                              ? "#002683"
                              : "#bbc5d5"
                          }
                        />
                        <path
                          id="Trazado_87"
                          data-name="Trazado 87"
                          d="M9.818,228.192H1.857A1.859,1.859,0,0,1,0,226.335V215.19a1.86,1.86,0,0,1,1.857-1.858H9.818a1.86,1.86,0,0,1,1.858,1.858v11.145A1.859,1.859,0,0,1,9.818,228.192ZM1.857,214.924a.266.266,0,0,0-.265.266v11.145a.266.266,0,0,0,.265.265H9.818a.266.266,0,0,0,.265-.265V215.19a.266.266,0,0,0-.265-.266Zm0,0"
                          transform="translate(0 -202.718)"
                          fill={
                            toggleEscritorio || ruta === "/dashboard" || props.location.state && props.location.state.dashboard === true
                              ? "#002683"
                              : "#bbc5d5"
                          }
                        />
                        <path
                          id="Trazado_88"
                          data-name="Trazado 88"
                          d="M287.15,349.824H279.19a1.859,1.859,0,0,1-1.858-1.857V343.19a1.86,1.86,0,0,1,1.858-1.858h7.961a1.86,1.86,0,0,1,1.857,1.858v4.776A1.859,1.859,0,0,1,287.15,349.824Zm-7.961-6.9a.266.266,0,0,0-.266.266v4.776a.265.265,0,0,0,.266.265h7.961a.265.265,0,0,0,.265-.265V343.19a.266.266,0,0,0-.265-.266Zm0,0"
                          transform="translate(-263.534 -324.349)"
                          fill={
                            toggleEscritorio || ruta === "/dashboard" || props.location.state && props.location.state.dashboard === true
                              ? "#002683"
                              : "#bbc5d5"
                          }
                        />
                        <path
                          id="Trazado_89"
                          data-name="Trazado 89"
                          d="M287.15,14.86H279.19A1.86,1.86,0,0,1,277.332,13V1.858A1.859,1.859,0,0,1,279.19,0h7.961a1.859,1.859,0,0,1,1.857,1.857V13A1.859,1.859,0,0,1,287.15,14.86ZM279.19,1.592a.266.266,0,0,0-.266.265V13a.266.266,0,0,0,.266.265h7.961a.266.266,0,0,0,.265-.265V1.858a.266.266,0,0,0-.265-.265Zm0,0"
                          transform="translate(-263.534)"
                          fill={
                            toggleEscritorio || ruta === "/dashboard" || props.location.state && props.location.state.dashboard === true
                              ? "#002683"
                              : "#bbc5d5"
                          }
                        />
                      </g>
                    </svg>
                    <TitleIcon
                      style={{
                        color:
                          toggleEscritorio || ruta === "/dashboard" || props.location.state && props.location.state.dashboard === true
                            ? "#002683"
                            : "#bbc5d5",
                      }}
                    >
                      Escritorio
                </TitleIcon>
                  </MenuAlumnos>
                </Link>
                <MenuAlumnos
                  borderSelection={
                    toggleAlumnos ||
                      ruta === "/alumnos" ||
                      ruta === "/alumnos/alumno" 
                      ? "3px solid  #009DDC"
                      : "3px solid #FFFF"
                  }
                  onClick={changeToggleAlumnos}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="25.474"
                    height="32.186"
                    viewBox="0 0 25.474 32.186"
                  >
                    <g id="graduated" transform="translate(-53.387 0)">
                      <path
                        id="Trazado_71"
                        data-name="Trazado 71"
                        d="M75.415,21.028l-1.889-.555-4.11-2.684v-.615a5.47,5.47,0,0,0,2.159-3.842,1.256,1.256,0,0,0,1.054-1.238V10.4c0-.007,0-.013,0-.019V8.24a.93.93,0,0,0,.389-.759V5.521l2.836-.483a1.005,1.005,0,0,0,.136-1.949L66.428.047a1,1,0,0,0-.61,0L56.257,3.089a1.009,1.009,0,0,0-.313,1.755V7.408H55.92a.978.978,0,0,0-.976.976V9.657l-.63,3.7a1.05,1.05,0,0,0,1.035,1.226h2.134a1.05,1.05,0,0,0,1.035-1.226l-.63-3.7V8.384a.978.978,0,0,0-.976-.976h-.023V5.122l2.344.4v1.96a.93.93,0,0,0,.387.757v3.855a1.256,1.256,0,0,0,1.054,1.238,5.47,5.47,0,0,0,2.157,3.84v.618l-4.109,2.684-1.887.554a4.794,4.794,0,0,0-3.446,4.6v1.405a.471.471,0,1,0,.943,0V25.631a3.875,3.875,0,0,1,2.769-3.7l1.759-.516,7.026,4.175a.477.477,0,0,0,.482,0l7.026-4.175,1.758.516a3.875,3.875,0,0,1,2.768,3.7v4.554a1.06,1.06,0,0,1-1.059,1.059H74.781V29.211a.471.471,0,0,0-.943,0v2.032H58.408V29.211a.471.471,0,1,0-.943,0v2.032H55.389a1.06,1.06,0,0,1-1.059-1.059V29.24a.471.471,0,1,0-.943,0v.944a2,2,0,0,0,2,2h21.47a2,2,0,0,0,2-2V25.63a4.794,4.794,0,0,0-3.446-4.6ZM57.482,13.639H55.348a.106.106,0,0,1-.105-.124l.57-3.347h1.2l.57,3.347A.106.106,0,0,1,57.482,13.639Zm-.539-5.255v.841H55.887V8.384a.034.034,0,0,1,.034-.034h.99A.034.034,0,0,1,56.943,8.384Zm7.495,15.074.188.288-4.813-2.86,3.055-2A9.9,9.9,0,0,0,64.438,23.459Zm0-2.035a12.124,12.124,0,0,0,3.379,0,9,9,0,0,1-.794,1.521l-.9,1.377-.9-1.377a9.006,9.006,0,0,1-.794-1.521Zm3.376,2.035a9.9,9.9,0,0,0,1.57-4.567l3.055,2-4.813,2.86ZM70.568,8.416h1.117V9.931h-.555a.563.563,0,0,1-.563-.563V8.416ZM56.543,3.987,66.1.946a.062.062,0,0,1,.038,0L75.7,3.987a.053.053,0,0,1,.043.064c0,.049-.031.054-.052.057l-3.161.538L71.347,4.5a.471.471,0,0,0-.116.936l.843.1V7.473h-11.9V5.54l4.879-.6a8.733,8.733,0,0,1,2.141,0l1.847.228a.471.471,0,0,0,.116-.936L67.31,4a9.672,9.672,0,0,0-2.373,0l-5.224.646-3.162-.539c-.02,0-.048-.008-.052-.057a.053.053,0,0,1,.044-.064Zm4.019,5.944V8.416H61.68v.952a.563.563,0,0,1-.563.563h-.556ZM61.6,13.089a.731.731,0,0,0-.726-.685.311.311,0,0,1-.311-.311v-1.22h.556a1.507,1.507,0,0,0,1.506-1.506V8.416h7v.952a1.507,1.507,0,0,0,1.506,1.506h.556v1.22a.311.311,0,0,1-.311.311.731.731,0,0,0-.726.685,4.534,4.534,0,0,1-9.05,0Zm4.525,5.188a5.442,5.442,0,0,0,2.35-.533v.3a8.985,8.985,0,0,1-.318,2.368,11.133,11.133,0,0,1-4.063,0,8.985,8.985,0,0,1-.318-2.368c0-.01,0-.02,0-.03s0,0,0-.006v-.264A5.443,5.443,0,0,0,66.124,18.278Z"
                        transform="translate(0 0)"
                        fill={
                          toggleAlumnos ||
                            ruta === "/alumnos" ||
                            ruta === "/alumnos/alumno"
                            ? "#002683"
                            : "#bbc5d5"
                        }
                      />
                    </g>
                  </svg>
                  <TitleIcon
                    style={{
                      color:
                        toggleAlumnos ||
                          ruta === "/alumnos" ||
                          ruta === "/alumnos/alumno" 
                          ? "#002683"
                          : "#bbc5d5",
                    }}
                  >
                    Alumnos
              </TitleIcon>
                </MenuAlumnos>

                <MenuAlumnos
                  borderSelection={
                    toggleProfesores || ruta === "/docentes" || ruta === "/docentes/docente"
                      ? "3px solid  #009DDC"
                      : "3px solid #FFFF"
                  }
                  onClick={changeToggleProfesores}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="25.241"
                    height="35.022"
                    viewBox="0 0 25.241 35.022"
                  >
                    <g id="teacher" transform="translate(-71.501 0)">
                      <g
                        id="Grupo_6"
                        data-name="Grupo 6"
                        transform="translate(71.501 0)"
                      >
                        <path
                          id="Trazado_70"
                          data-name="Trazado 70"
                          d="M96.708,32.6c-1.56-7.835-1.493-7.5-1.5-7.534a3.369,3.369,0,0,0-1.712-2.328,3.787,3.787,0,0,0-.908-.351l-3.833-1.331-1.278-.876v-.994a6.76,6.76,0,0,0,3.275-5.09,1.948,1.948,0,0,0,1.995-2.726V7.188c0-3.024-1.645-4.077-2.147-4.327A8.811,8.811,0,0,0,80.067.959l-.007,0A8.628,8.628,0,0,0,78.4,2.067a.513.513,0,0,0,.671.776A7.816,7.816,0,0,1,89.879,3.6a.493.493,0,0,0,.213.155c.066.027,1.627.684,1.627,3.432v3.238a2.208,2.208,0,0,0-.929-.173V8.575a.82.82,0,0,0-.746-.819,2.026,2.026,0,0,1-1.6-1.1,1.03,1.03,0,0,0-1.571-.284,8.3,8.3,0,0,1-6.728,1.794,2.524,2.524,0,0,0-2.855,2.088,2.219,2.219,0,0,0-.962.172c0-1.766-.2-3.688,1.243-5.89a.513.513,0,0,0-.859-.561c-1.966,3.012-1.263,5.711-1.41,7.4A1.948,1.948,0,0,0,77.3,14.1a6.748,6.748,0,0,0,1.791,3.9h0a6.969,6.969,0,0,0,1.677,1.3v.882l-1.277.876c-5,1.737-4.492,1.526-4.9,1.774a3.364,3.364,0,0,0-1.522,2.087c-.044.176.029-.157-1.534,7.683a2.816,2.816,0,0,0-.02.729,1.867,1.867,0,0,0,2.095,1.694h14.1a.513.513,0,1,0,0-1.026H77.218V29.3a.513.513,0,0,0-1.026,0V34H73.61c-.8,0-1.182-.345-1.066-1.211,1.4-6.86,1.407-7.413,1.636-7.975A2.34,2.34,0,0,1,75.527,23.5a4.1,4.1,0,0,1,.428-.133c.024-.007-.094.033,1.749-.607l1.229,2.269a.513.513,0,0,0,.9-.489l-1.149-2.121.786-.273,4.1,7.562a.63.63,0,0,0,1.062.074c.048-.066-.225.424,4.136-7.638l.786.273L85.4,30.1a1.451,1.451,0,0,1-2.551,0l-1.872-3.455a.513.513,0,1,0-.9.489l1.872,3.455a2.478,2.478,0,0,0,4.356,0l4.232-7.829c1.838.638,1.731.6,1.754.609.362.11.321.089.428.133a2.77,2.77,0,0,1,.62.355,2.285,2.285,0,0,1,.828,1.251c.024.088,1.547,7.639,1.553,7.893.015.644-.263,1-1.083,1H92.051V29.3a.513.513,0,0,0-1.026,0V34H90.1a.513.513,0,1,0,0,1.026h4.532a1.866,1.866,0,0,0,2.095-1.693A2.849,2.849,0,0,0,96.708,32.6ZM90.791,11.279a1,1,0,0,1,1.121.9,1,1,0,0,1-1.121.9ZM77.259,13.071a1,1,0,0,1-1.121-.9,1,1,0,0,1,1.121-.9ZM80,9.181a9.3,9.3,0,0,0,7.541-2.03l.008,0a3.044,3.044,0,0,0,2.216,1.6v1.817l-.585.346a1.333,1.333,0,0,0-1.1-.576H85.736a1.336,1.336,0,0,0-1.231.821h-.96a1.336,1.336,0,0,0-1.231-.821H79.967a1.332,1.332,0,0,0-1.1.576l-.583-.344A1.5,1.5,0,0,1,80,9.181Zm8.391,2.494V12.7a.808.808,0,0,1-.807.807H86.235a.808.808,0,0,1-.807-.807V11.675h0a.312.312,0,0,1,.308-.308h2.348A.312.312,0,0,1,88.391,11.675Zm-5.769,0V12.7a.808.808,0,0,1-.807.807H80.466a.808.808,0,0,1-.807-.807V11.675a.312.312,0,0,1,.308-.308h2.348A.312.312,0,0,1,82.622,11.675ZM79.669,17.1c-1.66-1.935-1.34-3.814-1.384-5.341l.348.205V12.7a1.835,1.835,0,0,0,1.833,1.833h1.349A1.835,1.835,0,0,0,83.648,12.7v-.509H84.4V12.7a1.835,1.835,0,0,0,1.833,1.833h1.349A1.835,1.835,0,0,0,89.417,12.7v-.729l.348-.205v1.6a5.738,5.738,0,0,1-5.74,5.739h-.069a5.508,5.508,0,0,1-1.5-.217A5.753,5.753,0,0,1,79.669,17.1Zm4.453,5.739-2.329-2.591v-.5a6.773,6.773,0,0,0,4,.145,6.7,6.7,0,0,0,.661-.216v.569Zm.578,1.828-.227.467h-.7l-.227-.467.578-.424Zm-1.93,1.412-.327-.6.26-.191.194.4Zm-.816-1.516-1.559-2.883.806-.556,2.161,2.4Zm2.168,4.012-.678-1.251.378-1.163h.6l.378,1.161Zm1.351-2.5-.127-.39.194-.4.257.189Zm.818-1.512-1.408-1.034,2.161-2.4.806.556Z"
                          transform="translate(-71.501 0)"
                          fill={
                            toggleProfesores || ruta === "/docentes" || ruta === "/docentes/docente"
                              ? "#002683"
                              : "#bbc5d5"
                          }
                        />
                      </g>
                    </g>
                  </svg>
                  <TitleIcon
                    style={{
                      color:
                        toggleProfesores || ruta === "/docentes" || ruta === "/docentes/docente"
                          ? "#002683"
                          : "#bbc5d5",
                    }}
                  >
                    Docentes
              </TitleIcon>
                </MenuAlumnos>

                <MenuAlumnos
                  borderSelection={
                    toggleTutores || ruta === "/tutores" || ruta === "/tutores/tutor"
                      ? "3px solid  #009DDC"
                      : "3px solid #FFFF"
                  }
                  onClick={changeToggleTutores}
                >
                  <svg
                    id="Grupo_7"
                    data-name="Grupo 7"
                    xmlns="http://www.w3.org/2000/svg"
                    width="25.237"
                    height="35.006"
                    viewBox="0 0 25.237 35.006"
                  >
                    <path
                      id="Trazado_84"
                      data-name="Trazado 84"
                      d="M96.644,32.584c-1.6-8.03-1.509-7.739-1.707-8.205a3.377,3.377,0,0,0-1.655-1.731c-.123-.06-4.583-1.6-4.59-1.6l-1.093-.75h1.128A3.879,3.879,0,0,0,92.6,16.419V13.16a1.8,1.8,0,0,0,0-1.99c-.124-1.127.635-5.378-2.242-8.466a8.628,8.628,0,0,0-12.406-.145.513.513,0,0,0,.743.707,7.6,7.6,0,0,1,10.91.135c2.436,2.612,1.894,6.342,1.969,6.972a2.212,2.212,0,0,0-.82-.13V9.966a1.479,1.479,0,0,0-1.012-1.4,2.928,2.928,0,0,1-1.486-1.638,1.111,1.111,0,0,0-.879-.666c-2.945-.373-3.511.518-5.815,1.952a2.4,2.4,0,0,1-2.05.34,1.748,1.748,0,0,0-2.286,1.659v.03a2.213,2.213,0,0,0-.627.07A11.784,11.784,0,0,1,77.424,5.1a.513.513,0,0,0-.927-.439c-1.222,2.577-.854,5.2-.92,6.261a1.808,1.808,0,0,0,0,2.484v3.012a3.879,3.879,0,0,0,3.875,3.875h1.072l-1.093.75c-4.921,1.709-4.515,1.545-4.768,1.7a3.378,3.378,0,0,0-1.69,2.338c-1.6,8.04-1.539,7.579-1.528,8.017a1.872,1.872,0,0,0,2.108,1.909H87.645a.513.513,0,1,0,0-1.026H77.159v-4.7a.513.513,0,1,0-1.026,0v4.7H73.553c-.8,0-1.183-.338-1.065-1.211l1.49-7.484a2.35,2.35,0,0,1,1.28-1.7,3.321,3.321,0,0,1,.666-.239l1.722-.6.913,1.685a.513.513,0,0,0,.9-.489l-.833-1.538.785-.273c4.395,8.113,4.1,7.571,4.133,7.62a.629.629,0,0,0,1.024.014c.048-.066-.225.424,4.135-7.636l.785.273-4.152,7.68a1.451,1.451,0,0,1-2.551,0L80.6,26.051a.513.513,0,0,0-.9.489l2.187,4.037a2.477,2.477,0,0,0,4.355,0l4.231-7.827c1.807.627,2.111.719,2.143.725l.063.027c.067.028.1.044.1.043a2.431,2.431,0,0,1,1.045.9c.327.521.2.225,1.817,8.328.116.866-.262,1.211-1.065,1.211H91.989v-4.7a.513.513,0,1,0-1.026,0v4.7h-.924a.513.513,0,1,0,0,1.026H94.57A1.871,1.871,0,0,0,96.678,33.1,2.928,2.928,0,0,0,96.644,32.584ZM90.756,11.269c.92-.049,1.483.885.855,1.489a1.153,1.153,0,0,1-.855.3Zm-.039,2.816a2.279,2.279,0,0,0,.859-.127v2.46h0a2.852,2.852,0,0,1-2.849,2.849H87.415v-.077A6.857,6.857,0,0,0,88.908,18l.023-.025A6.761,6.761,0,0,0,90.717,14.086ZM78.255,10.213a.723.723,0,0,1,.952-.68c1.628.512,2.768-.339,4.031-1.186A4.59,4.59,0,0,1,87.251,7.28a.082.082,0,0,1,.064.05,3.847,3.847,0,0,0,2.1,2.209.454.454,0,0,1,.314.427v.579l-.613.362a1.332,1.332,0,0,0-1.1-.575H85.675a1.335,1.335,0,0,0-1.231.821h-.959a1.335,1.335,0,0,0-1.231-.821H79.907a1.332,1.332,0,0,0-1.1.575l-.557-.329v-.365Zm10.075,1.452v1.021a.808.808,0,0,1-.807.807H86.174a.808.808,0,0,1-.807-.807V11.665h0a.312.312,0,0,1,.308-.308h2.347A.312.312,0,0,1,88.329,11.665Zm-5.767,0v1.021a.808.808,0,0,1-.807.807H80.406a.808.808,0,0,1-.807-.807V11.665a.312.312,0,0,1,.308-.308h2.347A.312.312,0,0,1,82.562,11.665Zm-3.988.293v.729a1.835,1.835,0,0,0,1.833,1.833h1.349a1.835,1.835,0,0,0,1.832-1.833v-.509h.754v.509a1.835,1.835,0,0,0,1.833,1.833h1.349a1.835,1.835,0,0,0,1.833-1.833v-.729l.376-.222a15.453,15.453,0,0,1-.1,2.693l-.021.106c-.034.179-.1.408-.1.4A5.839,5.839,0,0,1,88.163,17.3a5.725,5.725,0,0,1-4.1,1.8h-.109a5.7,5.7,0,0,1-3.64-1.333,4.574,4.574,0,0,1-.517-.489h0l0-.005.005.005a5.918,5.918,0,0,1-1.525-3.458c-.031-.391-.013-.52-.019-2.045ZM84.061,22.83l-2.328-2.59v-.507a6.584,6.584,0,0,0,2.34.387,6.431,6.431,0,0,0,1.684-.232c.147-.048.26-.063.632-.2v.558Zm.578,1.828-.227.467h-.7l-.227-.467.578-.424ZM76.424,11.526a1.162,1.162,0,0,1,.805-.257V13.06C76.3,13.109,75.71,12.142,76.424,11.526Zm3.027,7.742A2.852,2.852,0,0,1,76.6,16.419v-2.4a2.294,2.294,0,0,0,.666.069A6.767,6.767,0,0,0,80.7,19.268Zm3.258,6.8-.327-.6.26-.191.194.4Zm-.816-1.516-1.558-2.883.806-.556,2.16,2.4Zm2.168,4.011-.677-1.25.378-1.163h.6l.377,1.161Zm1.351-2.5-.127-.39.194-.4.257.189Zm.817-1.512-1.408-1.034,2.16-2.4.806.556Z"
                      transform="translate(-71.441 0)"
                      fill={
                        toggleTutores || ruta === "/tutores" || ruta === "/tutores/tutor" ? "#002683" : "#bbc5d5"
                      }
                    />
                  </svg>
                  <TitleIcon
                    style={{
                      color:
                        toggleTutores || ruta === "/tutores" || ruta === "/tutores/tutor"
                          ? "#002683"
                          : "#bbc5d5",
                    }}
                  >
                    Tutores
              </TitleIcon>
                </MenuAlumnos>

                <MenuAlumnos
                  borderSelection={
                    toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso"
                      ? "3px solid  #009DDC"
                      : "3px solid #FFFF"
                  }
                  onClick={changeToggleCursos}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="31"
                    height="23.767"
                    viewBox="0 0 31 23.767"
                  >
                    <g id="homework" transform="translate(-2 -9)">
                      <path
                        id="Trazado_72"
                        data-name="Trazado 72"
                        d="M32.483,11.067h-1.55V9.517A.517.517,0,0,0,30.417,9H19.567A2.58,2.58,0,0,0,17.5,10.049,2.58,2.58,0,0,0,15.433,9H4.583a.517.517,0,0,0-.517.517v1.55H2.517A.517.517,0,0,0,2,11.583V32.25a.517.517,0,0,0,.517.517H32.483A.517.517,0,0,0,33,32.25V11.583A.517.517,0,0,0,32.483,11.067ZM3.033,31.733V12.1H4.067V30.183a.517.517,0,0,0,.517.517h10.85a1.555,1.555,0,0,1,1.457,1.033ZM16.983,30.2a2.544,2.544,0,0,0-1.55-.532H5.1V10.033H15.433a1.555,1.555,0,0,1,1.55,1.55Zm.517.517-.016-.016h.031Zm.517-19.132a1.555,1.555,0,0,1,1.55-1.55H29.9V29.667H19.567a2.544,2.544,0,0,0-1.55.532Zm13.95,20.15H18.11A1.555,1.555,0,0,1,19.567,30.7h10.85a.517.517,0,0,0,.517-.517V12.1h1.033Z"
                        fill={
                          toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso" ? "#002683" : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_73"
                        data-name="Trazado 73"
                        d="M45.067,13A2.067,2.067,0,0,0,43,15.067v12.4a.517.517,0,0,0,.072.264l1.55,2.583a.517.517,0,0,0,.889,0l1.55-2.583a.517.517,0,0,0,.072-.264v-12.4A2.067,2.067,0,0,0,45.067,13Zm0,16.048-.853-1.426.853-.568.853.568ZM46.1,26.5l-.517-.346V19.2H44.55v6.954l-.517.346V18.167H46.1Zm0-9.367H44.033V16.1H46.1Zm-2.067-2.067a1.033,1.033,0,1,1,2.067,0Z"
                        transform="translate(-19.817 -1.933)"
                        fill={
                          toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso" ? "#002683" : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_74"
                        data-name="Trazado 74"
                        d="M10,14h9.817v1.033H10Z"
                        transform="translate(-3.867 -2.417)"
                        fill={
                          toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso" ? "#002683" : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_75"
                        data-name="Trazado 75"
                        d="M10,18h5.683v1.033H10Z"
                        transform="translate(-3.867 -4.35)"
                        fill={
                          toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso" ? "#002683" : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_76"
                        data-name="Trazado 76"
                        d="M23,18h3.1v1.033H23Z"
                        transform="translate(-10.15 -4.35)"
                        fill={
                          toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso" ? "#002683" : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_77"
                        data-name="Trazado 77"
                        d="M10,22h2.067v1.033H10Z"
                        transform="translate(-3.867 -6.283)"
                        fill={
                          toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso" ? "#002683" : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_78"
                        data-name="Trazado 78"
                        d="M16,22h6.717v1.033H16Z"
                        transform="translate(-6.767 -6.283)"
                        fill={
                          toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso" ? "#002683" : "#bbc5d5"
                        }
                      />
                    </g>
                  </svg>
                  <TitleIcon
                    style={{
                      color:
                        toggleCursos || ruta === "/cursos" || ruta === "/cursos/curso" ? "#002683" : "#bbc5d5",
                    }}
                  >
                    Cursos
              </TitleIcon>
                </MenuAlumnos>

                <MenuAlumnos
                  borderSelection={
                    toggleMaterias || ruta === "/materias"
                      ? "3px solid  #009DDC"
                      : "3px solid #FFFF"
                  }
                  onClick={changeToggleMaterias}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="25"
                    height="32.576"
                    viewBox="0 0 25 32.576"
                  >
                    <g
                      id="Grupo_27"
                      data-name="Grupo 27"
                      transform="translate(-25 -499.305)"
                    >
                      <path
                        id="Trazado_79"
                        data-name="Trazado 79"
                        d="M155.41,368.6H141.767a.477.477,0,0,0,0,.954H155.41a.477.477,0,0,0,0-.954Z"
                        transform="translate(-111.088 154.155)"
                        fill="#bbc5d5"
                      />
                      <path
                        id="Trazado_80"
                        data-name="Trazado 80"
                        d="M155.887,401.566a.477.477,0,0,0-.477-.477H141.767a.477.477,0,1,0,0,.954H155.41A.477.477,0,0,0,155.887,401.566Z"
                        transform="translate(-111.088 123.735)"
                        fill={
                          toggleMaterias || ruta === "/materias"
                            ? "#002683"
                            : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_81"
                        data-name="Trazado 81"
                        d="M173.133,433.577a.477.477,0,1,0,0,.954h9.651a.477.477,0,0,0,0-.954Z"
                        transform="translate(-140.459 93.314)"
                        fill={
                          toggleMaterias || ruta === "/materias"
                            ? "#002683"
                            : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_82"
                        data-name="Trazado 82"
                        d="M106.882,93.353l2.867-1.046a.65.65,0,0,0,0-1.222l-9.455-3.451a2.185,2.185,0,0,0-1.5,0l-9.455,3.451a.65.65,0,0,0,0,1.222l2.867,1.046v5.64a1.342,1.342,0,0,0,.416.962,7.024,7.024,0,0,0,.824.663h0a1.691,1.691,0,0,0-.505,1.2v.321a.681.681,0,0,0,.122.388L92.6,103.6a.477.477,0,0,0,.876.38l.437-1.007q.094.03.189.051v1.074a.477.477,0,0,0,.954,0V103.02q.1-.021.189-.051l.437,1.007a.477.477,0,1,0,.876-.38l-.464-1.069a.682.682,0,0,0,.122-.388v-.246a12.256,12.256,0,0,0,3.328.453,10.336,10.336,0,0,0,6.921-2.39,1.346,1.346,0,0,0,.415-.962C106.882,98.356,106.882,94,106.882,93.353Zm-7.758-4.822a1.23,1.23,0,0,1,.842,0l8.672,3.165-8.672,3.165a1.23,1.23,0,0,1-.842,0l-3.333-1.216,2.758-1.362.248.091a2.185,2.185,0,0,0,1.5,0l.44-.161a.685.685,0,0,0,.449-.641V91.22a1.645,1.645,0,0,0-1.606-1.637,1.611,1.611,0,0,0-1.163.468,1.691,1.691,0,0,0-.505,1.2v.284c-3.481,1.719-3.259,1.605-3.344,1.662l-4.112-1.5Zm-.262,2.85v-.131a.731.731,0,0,1,.219-.518.661.661,0,0,1,.477-.2.686.686,0,0,1,.669.682v.161l-.262.1a1.23,1.23,0,0,1-.842,0ZM94.1,97.643a6.145,6.145,0,0,1-.824-.648.37.37,0,0,1-.116-.268V93.7l.94.343C94.1,94.106,94.1,93.632,94.1,97.643Zm-.205,4.306a.8.8,0,0,1,.219-.649.663.663,0,0,1,.477-.2c.49.009.669.356.669.844A1.43,1.43,0,0,1,93.9,101.949ZM94.1,99.91a6.214,6.214,0,0,1-.75-.581.427.427,0,0,1-.19-.335v-.848a7.92,7.92,0,0,0,.94.611Zm1.767.871a1.629,1.629,0,0,0-.813-.56v-1a11.379,11.379,0,0,0,3.331.808.477.477,0,0,0,.086-.95,10.234,10.234,0,0,1-3.418-.9V94.393l3.74,1.365a2.185,2.185,0,0,0,1.5,0h0l5.634-2.056v3.025a.37.37,0,0,1-.116.268,8.832,8.832,0,0,1-5.2,2.081.477.477,0,0,0,.086.95,10.254,10.254,0,0,0,5.225-1.881v.848a.373.373,0,0,1-.116.268,9.375,9.375,0,0,1-6.266,2.13A11.047,11.047,0,0,1,95.87,100.781Z"
                        transform="translate(-62.046 417.369)"
                        fill={
                          toggleMaterias || ruta === "/materias"
                            ? "#002683"
                            : "#bbc5d5"
                        }
                      />
                      <path
                        id="Trazado_83"
                        data-name="Trazado 83"
                        d="M61.441,32.576H82.633a1.906,1.906,0,0,0,1.9-1.9V17.656a.477.477,0,0,0-.954,0V30.672a.951.951,0,0,1-.949.949H61.441a.951.951,0,0,1-.95-.949V3.9H83.583V15.429a.477.477,0,0,0,.954,0V1.9a1.906,1.906,0,0,0-1.9-1.9H61.441a1.906,1.906,0,0,0-1.9,1.9V30.672a1.906,1.906,0,0,0,1.9,1.9ZM83.583,1.9V2.947H64.3V.954h18.33A.951.951,0,0,1,83.583,1.9Zm-23.091,0a.949.949,0,0,1,.95-.949h1.908V2.947H60.491Z"
                        transform="translate(-34.537 499.305)"
                        fill={
                          toggleMaterias || ruta === "/materias"
                            ? "#002683"
                            : "#bbc5d5"
                        }
                      />
                    </g>
                  </svg>
                  <TitleIcon
                    style={{
                      color:
                        toggleMaterias || ruta === "/materias"
                          ? "#002683"
                          : "#bbc5d5",
                    }}
                  >
                    Materias
              </TitleIcon>
                </MenuAlumnos>

                <MenuAlumnos
                  borderSelection={
                    toggleAdministracion || ruta === "/administracion/planes" || ruta === "/administracion/planes/plan" || ruta === "/administracion/cuotas" || ruta === "/administracion/generar-cuotas"
                      ? "3px solid  #009DDC"
                      : "3px solid #FFFF"
                  }
                  onClick={changeToggleAdministracion}
                >
                  <svg
                    id="Layer_5"
                    enable-background="new 0 0 64 64"
                    fill={
                      toggleAdministracion || ruta === "/administracion/planes" || ruta === "/administracion/planes/plan" || ruta === "/administracion/cuotas" || ruta === "/administracion/generar-cuotas"
                        ? "#002683"
                        : "#bbc5d5"
                    }
                    height="25"
                    viewBox="0 0 64 64"
                    width="25"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g>
                      <path d="m56.526 30.376-.172-.316 5.318-5.318c.856-.856 1.328-1.995 1.328-3.206 0-2.501-2.035-4.536-4.536-4.536-.508 0-.997.101-1.464.26v-13.26c0-1.654-1.346-3-3-3h-31.171c-.801 0-1.555.312-2.122.879l-8.828 8.828c-.567.566-.879 1.32-.879 2.122v2.222c-5.598.507-10 5.221-10 10.949v32c0 2.757 2.243 5 5 5h12c2.757 0 5-2.243 5-5v-1h24 1 14.181l-2.184-13.104c-.018-4.711-1.215-9.384-3.471-13.52zm1.45 12.867-9.749 2.659-2.351-1.881c-1.192-.955-1.876-2.378-1.876-3.905v-8.116-1c1.103 0 2 .897 2 2v7h2v-1.586l6.874-6.874c1.923 3.6 2.984 7.624 3.102 11.703zm-13.976-14.243c-1.103 0-2 .897-2 2v1c0 .551-.449 1-1 1s-1-.449-1-1v-1c0-2.206 1.794-4 4-4h2.586l-2.054 2.054c-.176-.024-.35-.054-.532-.054zm-10.951 17.072 1.316-3.292 2.855 2.855-3.293 1.317c-.423.172-.927-.169-.927-.629 0-.087.016-.171.049-.251zm5.951-1.486-3.586-3.586 6.047-6.047c.187-.029.366-.076.539-.138v5.3c0 .454.052.898.136 1.334zm-.816-11.586h-11.184v-4h11.35c-.222.627-.35 1.298-.35 2v1c0 .352.072.686.184 1zm-11.184 2h11.586l-4 4h-7.586zm0 6h5.923l-1.6 4h-4.323zm0 6h4.098c.302 1.147 1.339 2 2.58 2 .343 0 .678-.064.994-.191l2.328-.932v3.123h-10zm12 .077.562-.225 3.33-3.33c.036.065.069.131.108.195v7.283h-4zm6-1.197 2 1.6v3.52h-2zm16-24.344c0 .677-.264 1.314-.743 1.793l-12.257 12.257v-2.586c0-1.252-.59-2.358-1.494-3.092l10.165-10.165c.479-.479 1.116-.743 1.793-.743 1.398 0 2.536 1.138 2.536 2.536zm-40-17.122v5.586c0 .551-.449 1-1 1h-5.586zm-9 12.586c4.962 0 9 4.038 9 9s-4.038 9-9 9-9-4.038-9-9 4.038-9 9-9zm-5 18.786c1.502.77 3.199 1.214 5 1.214s3.498-.444 5-1.214v5.214h-10zm11 25.214h-12c-1.654 0-3-1.346-3-3v-25.695c.569.81 1.239 1.544 2 2.174v6.521c0 1.103.897 2 2 2h10c1.103 0 2-.897 2-2v-6.521c.761-.63 1.431-1.364 2-2.174v25.695c0 1.654-1.346 3-3 3zm5-6v-29c0-5.728-4.402-10.442-10-10.949v-2.051h7c1.654 0 3-1.346 3-3v-7h31c.551 0 1 .449 1 1v14.586l-6.414 6.414h-4.586c-1.771 0-3.36.776-4.46 2h-14.54v26h22v2zm26 0v-7.236l9.195-2.508 1.624 9.744z" />
                      <path d="m11 30.855v2.145h2v-2.145c1.155-.366 2-1.435 2-2.709 0-1.087-.604-2.066-1.578-2.553l-1.95-.974c-.291-.146-.472-.439-.472-.765 0-.471.383-.854.854-.854h.292c.471 0 .854.383.854.854v1.146h2v-1.146c0-1.274-.845-2.343-2-2.709v-2.145h-2v2.145c-1.155.366-2 1.435-2 2.709 0 1.087.604 2.066 1.578 2.553l1.95.974c.291.146.472.439.472.765 0 .471-.383.854-.854.854h-.292c-.471 0-.854-.383-.854-.854v-1.146h-2v1.146c0 1.274.845 2.343 2 2.709z" />
                      <path d="m17 45h-10c-1.103 0-2 .897-2 2v10c0 1.103.897 2 2 2h10c1.103 0 2-.897 2-2v-10c0-1.103-.897-2-2-2zm0 4h-2v-2h2zm-4 8h-2v-2h2zm0-4h-2v-2h2zm-4 0h-2v-2h2zm2-4v-2h2v2zm-2-2v2h-2v-2zm-2 8h2v2h-2zm8 2v-6h2.001l.001 6z" />
                      <path d="m53 5h-28v6h28zm-2 4h-24v-2h24z" />
                      <path d="m25 13h28v2h-28z" />
                      <path d="m25 17h28v2h-28z" />
                      <path d="m25 21h16v2h-16z" />
                    </g>
                  </svg>

                  <TitleIcon
                    style={{
                      color:
                        toggleAdministracion || ruta === "/administracion/planes" || ruta === "/administracion/planes/plan" || ruta === "/administracion/cuotas"  || ruta === "/administracion/generar-cuotas"
                          ? "#002683"
                          : "#bbc5d5",
                    }}
                  >
                    Administracion
              </TitleIcon>
                </MenuAlumnos>

                <MenuAlumnos
                  borderSelection={
                    togglePerfil || ruta === "/perfil"
                      ? "3px solid  #009DDC"
                      : "3px solid #FFFF"
                  }
                  onClick={changeTogglePerfil}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="25"
                    height="25"
                    viewBox="0 0 25 25"
                  >
                    <g id="user" transform="translate(0)">
                      <g id="Grupo_8" data-name="Grupo 8" transform="translate(0)">
                        <path
                          id="Trazado_85"
                          data-name="Trazado 85"
                          d="M21.339,16.161a12.452,12.452,0,0,0-4.75-2.979,7.227,7.227,0,1,0-8.178,0A12.52,12.52,0,0,0,0,25H1.953a10.547,10.547,0,1,1,21.094,0H25A12.419,12.419,0,0,0,21.339,16.161ZM12.5,12.5a5.273,5.273,0,1,1,5.273-5.273A5.279,5.279,0,0,1,12.5,12.5Z"
                          transform="translate(0)"
                          fill={
                            togglePerfil || ruta === "/perfil"
                              ? "#002683"
                              : "#bbc5d5"
                          }
                        />
                      </g>
                    </g>
                  </svg>
                  <TitleIcon
                    style={{
                      color:
                        togglePerfil || ruta === "/perfil" ? "#002683" : "#bbc5d5",
                    }}
                  >
                    Perfil
              </TitleIcon>
                </MenuAlumnos>
              </div>

              <MenuAlumnos>
                <ImagenFinal src={logout} />
              </MenuAlumnos>
            </ContainerMenu>
          </Container>
        )
      }
      {toggleAlumnos && <DialogShadow onClick={changeToggleAlumnos} />}
      {toggleProfesores && <DialogShadow onClick={changeToggleProfesores} />}
      {toggleTutores && <DialogShadow onClick={changeToggleTutores} />}
      {toggleCursos && <DialogShadow onClick={changeToggleCursos} />}
      {togglePerfil && <DialogShadow onClick={changeTogglePerfil} />}
      {toggleMaterias && <DialogShadow onClick={changeToggleMaterias} />}
      {toggleAdministracion && (
        <DialogShadow onClick={changeToggleAdministracion} />
      )}

    </>
  );
};

export default withRouter(SliderBar);
