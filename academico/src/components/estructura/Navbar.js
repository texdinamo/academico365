import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import {
  AgregarShadow,
  ButtonAgregar,
  TextInfo,
  ContainerMenu,
  SubTextInfo
} from "../styled-component/Navbar";
import NuevaMateria from '../Materias/NuevaMateria'
import NuevoCurso from '../Cursos/NuevoCurso'
import NuevoAlumno from '../Alumnos/NuevoAlumno'
import NuevoProfesor from '../Profesores/NuevoProfesor'
import NuevoTutor from '../Tutores/NuevoTutor'
import NuevoPlan from '../administracion/NuevoPlan'
import NuevoMedioPago from "../MediosDePagos/NuevoMedioPago";
import NuevoPuntoVenta from "../PuntosDeVentas/NuevoPuntoVenta"
import NuevoCuentaContable from "../CuentasContables/NuevaCuentaContable";
import NuevoCentroCosto from '../CentroDeCostos/NuevoCentroCosto';
import NuevoProveedor from "../Proveedores/NuevoProveedor";

const Navbar = (props) => {
  const [toggle, setToggle] = useState(false);
  let ruta = props.location.pathname.split("/");
  let valor = ruta[1] === "dashboard" ? "Escritorio" : ruta[1];
   
  useEffect(() => {
  }, [toggle]);

  const changeToggle = () =>
    setToggle((prevLogin) => {
    return !prevLogin;
  });

  return (
    <>
      {toggle && <AgregarShadow onClick={changeToggle} />}
      {toggle && valor === "cursos" && (
        <NuevoCurso changeToggle={changeToggle} />
      )}
      {toggle && valor === "administracion" && (       
        <NuevoPlan changeToggle={changeToggle} /> 
      )}
      {toggle && valor === "materias" && (
        <NuevaMateria changeToggle={changeToggle} />
      )}
      {toggle && valor === "docentes" && (
        <NuevoProfesor changeToggle={changeToggle} />
      )}
      {toggle && valor === "tutores" && (
        <NuevoTutor changeToggle={changeToggle} />
      )}
      {toggle && valor === "alumnos" && (
        <NuevoAlumno changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/medios-pagos"  &&
      (<NuevoMedioPago changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/medios-pagos/medio-pago"  &&
      (<NuevoMedioPago changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/puntos-ventas"  &&
      (<NuevoPuntoVenta changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/puntos-ventas/punto-venta"  &&
      (<NuevoPuntoVenta changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/cuentas-contables"  &&
      (<NuevoCuentaContable changeToggle={changeToggle} />
      )}
       {toggle && props.location.pathname === "/administracion/cuentas-contables/cuenta-contable"  &&
      (<NuevoCuentaContable changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/centros-costos"  &&
      (<NuevoCentroCosto changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/centros-costos/centro-costo"  &&
      (<NuevoCentroCosto changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/proveedores"  &&
      (<NuevoProveedor changeToggle={changeToggle} />
      )}
      {toggle && props.location.pathname === "/administracion/proveedores/proveedor"  &&
      (<NuevoProveedor changeToggle={changeToggle} />
      )}             
                  
      <ContainerMenu>
        <TextInfo>
          {props.datos ? props.datos : valor.replace(/\b\w/g, (l) => l.toUpperCase())}
          <br />
          <SubTextInfo>
            Inicio > {valor.replace(/\b\w/g, (l) => l.toUpperCase())}
          </SubTextInfo>
        </TextInfo>
        {valor === "Escritorio" || props.location.pathname === '/perfil' ||  props.location.pathname === '/administracion/planes/plan' ||  props.location.pathname === '/administracion/cuotas' || props.location.pathname === '/administracion/generar-cuotas'  ? null : (
          <ButtonAgregar onClick={changeToggle}>
            {valor === "alumnos" && "Añadir Alumno"}
            {valor === "docentes" && "Añadir Docente"}
            {valor === "tutores" && "Añadir Tutor"}
            {valor === "cursos" && "Añadir Curso"}
            {valor === "materias" && "Añadir Materias"}
            {props.location.pathname === "/administracion/planes" && "Añadir Plan" }
            {props.location.pathname === "/administracion/medios-pagos" && "Añadir Medios de Pagos"}
            {props.location.pathname === "/administracion/medios-pagos/medio-pago" && "Añadir Medios de Pagos"}
            {props.location.pathname === "/administracion/puntos-ventas" && "Añadir Puntos de Ventas"}
            {props.location.pathname === "/administracion/puntos-ventas/punto-venta" && "Añadir Puntos de Ventas"}
            {props.location.pathname === "/administracion/cuentas-contables" && "Añadir Cuenta Contables"}
            {props.location.pathname === "/administracion/cuentas-contables/cuenta-contable" && "Añadir Cuenta Contables"}
            {props.location.pathname === "/administracion/centros-costos" && "Añadir Centro Costo"}
            {props.location.pathname === "/administracion/centros-costos/centro-costo" && "Añadir Centro Costo"}
            {props.location.pathname === "/administracion/proveedores" && "Añadir Proveedor"}
            {props.location.pathname === "/administracion/proveedores/proveedor" && "Añadir Proveedor"}                 
          </ButtonAgregar>
        )}
      </ContainerMenu>
    </>
  );
};

export default withRouter(Navbar);
