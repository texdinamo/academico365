import React from 'react';
import styled from 'styled-components'

import {AgregarShadow,AgregarFormulario,ButtonAgregar,ButtonCrear,ButtonCancelar,
TextFormulario,TextInfo,ContainerMenu,SubTextInfo,AgregarFormularioAlumno} from '../styled-component/Navbar'


    const Select = styled.select`

width: 100%;
color: #BBC5D5;
border: 1px solid #BBC5D5;
padding: 10px;
border-radius: 4px;
`

const Linea = styled.hr`

width: 100%;
border: 3px solid #06adff;
background-color: #06adff;


`
    
const handleChange = () => console.log('44')



const AgregarAlumnoForm = ({changeToggle,handleChange,clickSubmit,name,surname,billingAddress,billingCity,billingProvince,billingZip,birthDate,province,city,country,document,gender,nationality,bloodtype,address,neighborhood,zipCode,email,phoneNumber,docket,studyLevel,
validateNombre,validateDocket,validateDocument,validateEmail,validateGender,validateNationality,
validateNeighborhood,validatePhonenumber,validateStudylevel,validateSurname,validateZipcode,validateAddress,
validateBillingcity,validateBillingprovince,validateBloodtype,validateBillingaddress,validateCountry,
validateProvince,validateCity,validateBillingzip,validateBirthDate
}) => {
    return ( 


        <>
           <AgregarFormularioAlumno >
          
  
        <div className="formulario__head">
            <div>
                <TextFormulario>Crear nuevo Alumno</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}><i class="fas fa-times"></i></div>
        </div>

        <div className="formulario__body ">
        <div>

            <form className="input-form">

                <h5>Datos personales</h5>
                <Linea/>
                <label>Nombre</label>
                <input onChange={this.handleChange('name')}  value={name} type="text" className={validateNombre ? 'form-control is-invalid'  : 'form-control'}  placeholder="Nombre"/>
               
               
               {validateNombre &&  <p className="text-danger my-1">El nombre es obligatorio</p> }



                <label>Apellido</label>
                <input  onChange={handleChange('surname')}  value={surname} type="text" className={validateSurname ? 'form-control is-invalid'  : 'form-control'} placeholder="Apellido"/>

                {validateSurname &&  <p className="text-danger my-1">El apellido es obligatorio</p> }


                <label>Dni</label>
                <input  onChange={handleChange('document')}  value={document} type="number" className={validateDocument ? 'form-control is-invalid'  : 'form-control'}  placeholder="Dni"/>

                {validateDocument &&  <p className="text-danger my-1">El apellido es obligatorio</p> }
                
                
                <label>Fecha de nacimiento</label>
                <input  onChange={handleChange('birthDate')}  value={birthDate} type="date" className={validateBirthDate ? 'form-control is-invalid'  : 'form-control'} placeholder="Cuil"/>

    { validateBirthDate &&  <p className="text-danger my-1">La fecha de nacimineto es obligatorio</p> }
            

                <label>Sexo</label>
                <div className="radio d-flex justify-content-center" >
                <label>Masculino</label>
                <input 
                    type="radio"
                    name="gender"
                    value="masculino"
                    checked={gender === "masculino"}
                    onChange={handleChange('gender')}
                /> 

                <label>Femenino</label>

                <input 
                    type="radio"
                    name="gender"
                    value="femenino"
                    checked={gender === "femenino"}
                    onChange={handleChange('gender')}
                /> 
                </div>
        
                { validateGender &&  <p className="text-danger my-1">El sexo  es obligatorio</p> }

            <label>Nacionalidad</label>
                <input  onChange={handleChange('nationality')}  value={nationality} type="text" className={validateNationality ? 'form-control is-invalid'  : 'form-control'} placeholder="Nacionalidad"/>


    { validateNationality &&  <p className="text-danger my-1">La nacionalidad es obligatoria</p> }
            

                <label className="mb-3">Tipo de sangre</label>
            <Select
                    name="bloodtype"
                    value={bloodtype}
                    onChange={handleChange('bloodtype')}
                    style={{ borderColor : validateBloodtype ?  'red' : '' }}
              
                >
                    <option value="">-- Seleccione --</option>
                    <option value="A +">A POSTIVO</option>
                    <option value="A -">A NEGATIVO</option>

                    <option value="B +">B POSTIVO</option>
                    <option value="B -">B NEGATIVO</option>
                </Select>

                { validateBloodtype &&  <p className="text-danger my-1">El tipo de sangre obligatorio</p> }
     

   <h5 className="mt-4">Contactos</h5>
                <Linea/>
             

            <label>Domicilio</label>
                <input  onChange={handleChange('address')}  value={address} type="text" className={validateAddress ? 'form-control is-invalid'  : 'form-control'}   placeholder="Domicilio"/>
     
                { validateAddress &&  <p className="text-danger my-1">La direccion es obligatorio</p> }
               

                <label>Barrio</label>
                <input onChange={handleChange('neighborhood')}  value={neighborhood} type="text" className={validateNeighborhood ? 'form-control is-invalid'  : 'form-control'} placeholder="Barrio"/>
               
                { validateNeighborhood &&  <p className="text-danger my-1">El barrio es obligatorio</p> }
               

            <label>Ciudad</label>
                <input onChange={handleChange('city')}  value={city} type="text" className={validateCity ? 'form-control is-invalid'  : 'form-control'} placeholder="Ciudad"/>


                { validateCity &&  <p className="text-danger my-1">La ciudad es obligatoria</p> }
               

                <label className="mb-3" >Pais</label>
                <Select
                    name="country"
                    value={country}
                    onChange={handleChange('country')}
                    style={{ borderColor : validateCountry ?  'red' : '' }}
              
>
                    <option value="">-- Seleccione pais--</option>
                    <option value="argentina">Argentina</option>
                    <option value="brasil">Brasil</option>
                
                    <option value="uruguay">Uruguay</option>
                </Select>

                { validateCountry &&  <p className="text-danger my-1">El pais es obligatorio</p> }
               


                <label>Provincia</label>
                <input  onChange={handleChange('province')}  value={province} type="text" className={validateProvince ? 'form-control is-invalid'  : 'form-control'} placeholder="Provincia"/>
                { validateProvince &&  <p className="text-danger my-1">La provincia obligatorio</p> }
                
            
            

            <label>Codigo postal</label>
                <input onChange={handleChange('zipCode')}  value={zipCode}  type="text" className={validateZipcode ? 'form-control is-invalid'  : 'form-control'} placeholder="Cod postal"/>
                { validateZipcode &&  <p className="text-danger my-1">El codigo postal es obligatorio</p> }
            

                <label>Telefono</label>
                <input onChange={handleChange('phoneNumber')}  value={phoneNumber} type="number" className={validatePhonenumber ? 'form-control is-invalid'  : 'form-control'} placeholder="Telefono"/>
                { validatePhonenumber &&  <p className="text-danger my-1">El telefono es obligatorio</p> }
            
                <label>Email</label>
                <input  onChange={handleChange('email')}  value={email}  type="email" className={validateEmail ? 'form-control is-invalid'  : 'form-control'} placeholder="Email"/>
                { validateEmail &&  <p className="text-danger my-1">El telefono es obligatorio</p> }
            

   <h5 className="mt-4">Escolares</h5>
                <Linea/>
             

                <label>Legajo</label>
                <input onChange={handleChange('docket')}  value={docket}  type="text" className={validateDocket ? 'form-control is-invalid'  : 'form-control'} placeholder="Legajo"/>

                { validateDocket &&  <p className="text-danger my-1">El legajo es obligatorio</p> }
            

                <label className="mb-3" >Nivel de estudio</label>
            <Select
                    name="studyLevel"
                    value={studyLevel}
                    onChange={handleChange('studyLevel')}
                    style={{ borderColor : validateStudylevel ?  'red' : '' }}
                >
                    <option value="">-- Seleccione nivel --</option>
                    <option value="primario">Primario</option>
                    <option value="secundario">Secundario</option>

                    <option value="universitario">Universitario</option>
                </Select>

                { validateStudylevel &&  <p className="text-danger my-1">El Nivel de estudio es obligatorio</p> }
            

                <h5 className="mt-4">Facturacion</h5>
                <Linea/>
             
                <label>Domicilio de facturacion</label>
                <input onChange={handleChange('billingAddress')}  value={billingAddress} type="text" className={validateBillingaddress ? 'form-control is-invalid'  : 'form-control'}  placeholder="Domicilio"/>
               
                { validateBillingaddress &&  <p className="text-danger my-1">El Domicilio de la factuaracion es obligatorio</p> }
            
            
            <label  >Ciudad de facturacion</label>
                <input onChange={handleChange('billingCity')}  value={billingCity}  type="text" className={validateBillingcity ? 'form-control is-invalid'  : 'form-control'} placeholder="Ciudad"/>

                { validateBillingcity &&  <p className="text-danger my-1">La ciudad de la facturacion es obligatorio</p> }


                <label>Provincia de facturacion</label>
                <input  onChange={handleChange('billingProvince')}  value={billingProvince} type="text" className={validateBillingprovince ? 'form-control is-invalid'  : 'form-control'}  placeholder="Provincia"/>
            
                { validateBillingprovince &&  <p className="text-danger my-1">La provincia de la facturacion es obligatorio</p> }



                <label>Codigo postal</label>
                <input onChange={handleChange('billingZip')}  value={billingZip} type="text" className={validateBillingzip ? 'form-control is-invalid'  : 'form-control'} placeholder="Cod postal"/>

                { validateBillingzip &&  <p className="text-danger my-1">El codigo postal de la facturacion es obligatorio</p> }
            

            </form>
       
        </div>

        <div className="formulario__footer">
        <ButtonCancelar>
            Cancelar
            </ButtonCancelar>
            <ButtonCrear onClick={clickSubmit} >
            Crear
            </ButtonCrear>
            
        </div>

        </div>
        </AgregarFormularioAlumno> 
    
        </>


     );
}
 
export default AgregarAlumnoForm;