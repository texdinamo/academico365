import React, { useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import logo from "../../img/user.svg";
// import campana from "../../img/Notifications-1.svg";
// import buscador from "../../img/Search.svg";
import arrow from "../../img/arrow - right.svg";
import icono from "../../img/exclamation.svg";
import logout from "../../img/logout.svg";
import { Container2 } from "../styled-component/slidebar";

const Test = styled.div`
  width: 200px;
  height: 200px;
  background-color: white;
  margin-left: 75px;
  width: 100%;
  height: 100vh;
  padding: 19px 26px;
`;

const UserInfo = ({ valorMenu, handleClick}) => {
  const [close, setClose] = useState(true);
  return (
    <Container2>
      <Test>
        <div className="userInfo__head d-flex justify-content-between"></div>
        {valorMenu === "perfil" && (
          <div>
            <div className=" d-flex justify-content-between">
              <div className="userInfo__datos">
                <p className="userInfo__datos--text-1">
                  Hola,
                  <br />
                  <span className="userInfo__datos--text-2">Hernan!</span>
                </p>
                <Link onClick={handleClick} className="userInfo__datos--link" to="/perfil">
                  Administra tu perfil
                </Link>
              </div>

              <div className="userInfo__img">
                <img src={logo} />
              </div>
            </div>

            <div className="userInfo__notificaciones d-flex justify-content-between">
              <div className="userInfo__notificaciones--title">
                <p>Notificaciones</p>
              </div>

              <div className="userInfo__notificaciones--link">
                <a href="#">
                  Ver todas <img src={arrow} />
                </a>
              </div>
            </div>

            <div className="d-block userInfo__texto">
              <ul>
                <li className="userInfo__texto__list ">
                  <img src={icono} />{" "}
                  <p className="userInfo__texto__list--text1">
                    Ajustes de cuenta
                    <br />
                    <span className="userInfo__texto__list--text2">
                      Lorem ipsum dolor sit amet, consetetur
                    </span>
                  </p>
                </li>

                <li className="userInfo__texto__list ">
                  <img src={icono} />{" "}
                  <p className="userInfo__texto__list--text1">
                    Mensaje nuevo
                    <br />
                    <span className="userInfo__texto__list--text2">
                      Lorem ipsum dolor sit amet, consetetur
                    </span>
                  </p>
                </li>

                <li className="userInfo__texto__list ">
                  <img src={icono} />{" "}
                  <p className="userInfo__texto__list--text1">
                    Actualizacion de datos
                    <br />
                    <span className="userInfo__texto__list--text2">
                      Lorem ipsum dolor sit amet, consetetur
                    </span>
                  </p>
                </li>

                <li className="userInfo__texto__list ">
                  <img src={icono} />{" "}
                  <p className="userInfo__texto__list--text1">
                    Actualizacion de datos
                    <br />
                    <span className="userInfo__texto__list--text2">
                      Lorem ipsum dolor sit amet, consetetur
                    </span>
                  </p>
                </li>
              </ul>
            </div>

            <div className="userInfo__footer">
              <p className="userInfo__footer--text">Cerrar sesion</p>
              <img className="userInfo__footer--img" src={logout} />
            </div>
          </div>
        )}

        {valorMenu === "docentes" && (
          <div className="alumnos">
            <h2 className="userInfo__datos alumnos__title">Docentes</h2>
            <ul>
              <li>
                <Link onClick={handleClick} to="/docentes">Ver Docentes</Link>
              </li>
            </ul>
          </div>
        )}

        {valorMenu === "administracion" &&(
          <div className="alumnos">
            <h2 className="userInfo__datos alumnos__title">Administracion</h2>
            <ul>
              <h3 className="userInfo__datos alumnos__title">Cuotas</h3>
              <li>
                <Link onClick={handleClick} to="/administracion/generar-cuotas">Generar cuotas</Link>
              </li>
              <li>
                <Link onClick={handleClick} to="/administracion/cuotas">Listado de cuotas</Link>
              </li>
              <li>
                <Link onClick={handleClick} to="/administracion/planes">Planes de cuotas</Link>
              </li>
            </ul>
            <ul>
              <h3 className="userInfo__datos alumnos__title">Facturación y cobranza</h3>
              <li>
                <Link onClick={handleClick} to="/dashboard">Generar recibos y facturas (SP5)</Link>
              </li>
              <li>
                <Link onClick={handleClick} to="/dashboard">Listados de recibos y facturas (SP5) (2)</Link>
              </li>
            </ul>
            <ul>
              <h3 className="userInfo__datos alumnos__title">Listados</h3>
              <ul>
                <h4 className="userInfo__datos alumnos__title">Administrativos</h4>
                <li>
                  <Link onClick={handleClick} to="/dashboard">Caja Diaria</Link>
                </li>
                <li>
                  <Link onClick={handleClick} to="/dashboard">Listado de Pagos</Link>
                </li>
              </ul>
              <ul>
                <h4 className="userInfo__datos alumnos__title">Fiscales</h4>
                <li>
                  <Link onClick={handleClick} to="/dashboard">Libro IVA Ventas (exportable a Excel)</Link>
                </li>
                <li>
                  <Link onClick={handleClick} to="/dashboard">Libro IVA Compras (exportable a Excel)</Link>
                </li>
              </ul>
            </ul>
            <ul>
              <h3 className="userInfo__datos alumnos__title">Gastos y pagos</h3>
              <li>
                <Link onClick={handleClick} to="/dashboard">Cargar de gastos</Link>
              </li>
              <li>
                <Link onClick={handleClick} to="/administracion/proveedores">Proveedores</Link>
              </li>             
            </ul>
            <ul>
              <h3 className="userInfo__datos alumnos__title">Configuración</h3>
              <li>
                <Link onClick={handleClick} to="/administracion/cuentas-contables">Cuentas contables</Link>
              </li>
              <li>
                <Link onClick={handleClick} to="/administracion/centros-costos">Centros de costos</Link>
              </li>
              <li>
                <Link onClick={handleClick} to="/administracion/puntos-ventas">Puntos de venta</Link>
              </li>
              <li>
                <Link onClick={handleClick} to="/administracion/medios-pagos" >Medios de pago</Link>
              </li>
            </ul>
            

          </div>
        )}

        {valorMenu === "tutores" && (
          <div className="alumnos">
            <h2 className="userInfo__datos alumnos__title">Tutores</h2>

            <ul>
              <li>
                <Link onClick={handleClick} to="/tutores">Ver Tutores</Link>
              </li>
            </ul>
          </div>
        )}

        {valorMenu === "alumnos" && (
          <div className="alumnos">
            <h2 className="userInfo__datos alumnos__title">Alumnos</h2>

            <ul>
              <li>
                <Link onClick={handleClick} to="/alumnos">Ver listado</Link>
              </li>

            </ul>
          </div>
        )}

        {valorMenu === "cursos" && (
          <div className="alumnos">
            <h2 className="userInfo__datos alumnos__title">Cursos</h2>

            <ul>
              <li>
                <Link onClick={handleClick} to="/cursos">Ver Cursos</Link>
              </li>
            </ul>
          </div>
        )}

        {valorMenu === "materias" && (
          <div className="alumnos">
            <h2 className="userInfo__datos alumnos__title">Materias</h2>

            <ul>
              <li>
                <Link onClick={handleClick} to="/materias">Ver Materias</Link>
              </li>
            </ul>
          </div>
        )}
      </Test>
    </Container2>
  );
};

export default UserInfo;
