import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

import {
    AgregarShadow,
    AgregarFormulario,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    Select,
    Linea,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    AgregarFormularioAlumno,
  } from "../styled-component/Navbar";
  import { create } from './apiProfesores'


const NuevoProfesor = ({changeToggle}) => {
    const [validarEmail,setValidarEmail] = useState(false)
    const [validateNombre, setValidateNombre] = useState(false);
    const [validateSurname, setValidateSurname] = useState(false);
    const [validateDocument, setValidateDocument] = useState(false);
    const [validateBirthDate, setValidateBirthDate] = useState(false);
    const [validateGender, setValidateGender] = useState(false);
    const [validateNationality, setValidateNationality] = useState(false);
    const [validateBloodtype, setValidateBloodtype] = useState(false);
    const [validateStudylevel, setValidateStudylevel] = useState(false);
    const [validateAddress, setValidateAddress] = useState(false);
    const [validateNeighborhood, setValidateNeighborhood] = useState(false);
    const [validateCity, setValidateCity] = useState(false);
    const [validateProvince, setValidateProvince] = useState(false);
    const [validateCountry, setValidateCountry] = useState(false);
    const [validateZipcode, setValidateZipcode] = useState(false);
    const [validatePhonenumber, setValidatePhonenumber] = useState(false);
    const [validateEmail, setValidateEmail] = useState(false);
    const [validateBusinessname, setValidateBusinessname] = useState(false);
    const [validateDocket, setValidateDocket] = useState(false);
    const [validateCuit, setValidateCuit] = useState(false);
    const [validateBillingaddress, setValidateBillingaddress] = useState(false);
    const [validateBillingcity, setValidateBillingcity] = useState(false);
    const [validateBillingprovince, setValidateBillingprovince] = useState(false);
    const [validateBillingzip, setValidateBillingzip] = useState(false);
  
    const [validateIdSeccion, setValidateIdSeccion] = useState(false);
    const [validateYear, setValidateYear] = useState(false);
    const [validateDetalle, setValidateDetalle] = useState(false);
    const [values, setValues] = useState({
        docket: "",
        name: "",
        surname: "",
        document: "",
        birthDate: "",
        gender: "",
        nationality: "",
        address: "",
        neighborhood: "",
        city: "",
        province: "",
        country: "",
        zipCode: "",
        phoneNumber: "",
        email: ""
       
      });


      const handleChange = (name) => (event) => {
        setValues({ ...values, [name]: event.target.value });
    
        if (name === "name") {
          setValidateNombre(false);
        }
    
        if (name === "surname") {
          setValidateSurname(false);
        }
    
        if (name === "document") {
          setValidateDocument(false);
        }
    
        //console.log(name);
        if (name === "birthDate") {
          setValidateBirthDate(false);
        }
    
        if (name === "gender") {
          setValidateGender(false);
        }
    
        if (name === "nationality") {
          setValidateNationality(false);
        }
    
        if (name === "bloodtype") {
          setValidateBloodtype(false);
        }
        if (name === "address") {
          setValidateAddress(false);
        }
        if (name === "neighborhood") {
          setValidateNeighborhood(false);
        }
        if (name === "city") {
          setValidateCity(false);
        }
        if (name === "province") {
          setValidateProvince(false);
        }
    
        if (name === "country") {
          setValidateCountry(false);
        }
    
        if (name === "zipCode") {
          setValidateZipcode(false);
        }
        if (name === "phoneNumber") {
          setValidatePhonenumber(false);
        }
    
        if (name === "email") {


          if(event.target.value.indexOf('@') !== -1 &&  event.target.value.indexOf('.') !== -1 ){
             setValidarEmail(false)
          }else{
           setValidarEmail(true)
         }
     
          setValidateEmail(false);
       
       
       }
    
        if (name === "docket") {
          setValidateDocket(false);
        }
    
        if (name === "studyLevel") {
          setValidateStudylevel(false);
        }
    
        if (name === "billingAddress") {
          setValidateBillingaddress(false);
        }
    
        if (name === "billingCity") {
          setValidateBillingcity(false);
        }
        if (name === "billingProvince") {
          setValidateBillingprovince(false);
        }
    
        if (name === "billingZip") {
          setValidateBillingzip(false);
        }
      };


      const clickSubmit = (event) => {
        event.preventDefault();
    
        if (!values.name) {
          setValidateNombre(true);
        }
        if (!values.surname) {
          setValidateSurname(true);
        }
        if (!values.document) {
          setValidateDocument(true);
        }
        if (!values.birthDate) {
          setValidateBirthDate(true);
        }
        if (!values.nationality) {
          setValidateNationality(true);
        }
    
        if (!values.bloodtype) {
          setValidateBloodtype(true);
        }
    
        if (!values.address) {
          setValidateAddress(true);
        }
    
        if (!values.neighborhood) {
          setValidateNeighborhood(true);
        }
    
        if (!values.city) {
          setValidateCity(true);
        }
    
        if (!values.province) {
          setValidateProvince(true);
        }
    
        if (!values.country) {
          setValidateCountry(true);
        }
        if (!values.zipCode) {
          setValidateZipcode(true);
        }
    
        if (!values.phoneNumber) {
          setValidatePhonenumber(true);
        }
    
        if (!values.email) {
          setValidateEmail(true);
        }
    
        if (!values.docket) {
          setValidateDocket(true);
        }
    
        if (!values.studyLevel) {
          setValidateStudylevel(true);
        }
    
        if (!values.billingAddress) {
          setValidateBillingaddress(true);
        }
        if (!values.billingCity) {
          setValidateBillingcity(true);
        }
    
        if (!values.billingProvince) {
          setValidateBillingprovince(true);
        }
    
        if (!values.billingZip) {
          setValidateBillingzip(true);
        }
    
        if (!values.gender) {
          setValidateGender(true);
        }
    
        
    if (
        values.address && 
        values.gender &&
        values.docket &&
        values.email &&
        values.phoneNumber &&
        values.zipCode &&
        values.country &&
        values.province &&
        values.city &&
        values.neighborhood &&
        values.nationality &&
        values.birthDate &&
        values.name &&
        values.surname &&
        values.document &&
        validarEmail === false
      ) {
        //console.log(values)
        
       const val = {
          "userType": "TEACHER",
          "user": {
            "name": values.name,
            "surname": values.surname,
            "documentation": values.document,
            "cuil": null,
            "birthDate":  values.birthDate,
            "fecha_alta": null,
            "fecha_baja": null,
            "gender":  values.gender,
            "nacionality": values.nationality,
            "address": values.address,
            "neighborhood": values.neighborhood,
            "city": values.city,
            "province": values.province,
            "country": values.country,
            "zipCode": values.zipCode,
            "phone": values.phoneNumber,
            "email": values.email,
            "metadata": null
          }
        }
        console.log(val)

        create(val)
        .then(data => {

          if(data){
            console.log(data)
            if(data.ok === true){
  
              toast.success("Profesor agregado");
              changeToggle();
            }
            if(data.ok === false){
  
              toast.error("Error al agregar");
              changeToggle();
            }
  
          }else{
            toast.error("Error en la api");
            changeToggle();
          
          }
         
        })

      
      }
    
      };


  
    return ( 
        <>
        <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>
                Crear un nuevo Docente
              </TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>

          <div className="formulario__body ">
            <div>
              <form className="input-form">
                <p className="subtitulo">Datos personales</p>
                <Linea />
                <label>Nombre</label>
                <input
                  onChange={handleChange("name")}
                  value={values.name}
                  type="text"
                  className={
                    validateNombre ? "form-control is-invalid" : "form-control"
                  }
                  placeholder="Nombre"
                />

                {validateNombre && (
                  <p className="text-danger my-1">El nombre es obligatorio</p>
                )}

                <label>Apellido</label>
                <input
                  onChange={handleChange("surname")}
                  value={values.surname}
                  type="text"
                  className={
                    validateSurname ? "form-control is-invalid" : "form-control"
                  }
                  placeholder="Apellido"
                />

                {validateSurname && (
                  <p className="text-danger my-1">El apellido es obligatorio</p>
                )}

                <label>Dni</label>
                <input
                  onChange={handleChange("document")}
                  value={values.document}
                  type="number"
                  className={
                    validateDocument
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  placeholder="Dni"
                />

                {validateDocument && (
                  <p className="text-danger my-1">El apellido es obligatorio</p>
                )}

                <label>Fecha de nacimiento</label>
                <input
                  onChange={handleChange("birthDate")}
                  value={values.birthDate}
                  type="date"
                  className={
                    validateBirthDate
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  placeholder="nacimiento"
                />

                {validateBirthDate && (
                  <p className="text-danger my-1">
                    La fecha de nacimineto es obligatorio
                  </p>
                )}

                <label>Sexo</label>
                <div className="radio d-flex justify-content-center">
                  <label>Masculino</label>
                  <input
                    type="radio"
                    name="gender"
                    value="masculino"
                    checked={values.gender === "masculino"}
                    onChange={handleChange("gender")}
                  />

                  <label>Femenino</label>

                  <input
                    type="radio"
                    name="gender"
                    value="femenino"
                    checked={values.gender === "femenino"}
                    onChange={handleChange("gender")}
                  />
                </div>

                {validateGender && (
                  <p className="text-danger my-1">El sexo es obligatorio</p>
                )}

                <label>Nacionalidad</label>
                <input
                  onChange={handleChange("nationality")}
                  value={values.nationality}
                  type="text"
                  className={
                    validateNationality
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  placeholder="Nacionalidad"
                />

                {validateNationality && (
                  <p className="text-danger my-1">
                    La nacionalidad es obligatoria
                  </p>
                )}

                <p className="mt-4 subtitulo">Contactos</p>
                <Linea />

                <label>Domicilio</label>
                <input
                  onChange={handleChange("address")}
                  value={values.address}
                  type="text"
                  className={
                    validateAddress ? "form-control is-invalid" : "form-control"
                  }
                  placeholder="Domicilio"
                />

                {validateAddress && (
                  <p className="text-danger my-1">
                    La direccion es obligatorio
                  </p>
                )}

                <label>Barrio</label>
                <input
                  onChange={handleChange("neighborhood")}
                  value={values.neighborhood}
                  type="text"
                  className={
                    validateNeighborhood
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  placeholder="Barrio"
                />

                {validateNeighborhood && (
                  <p className="text-danger my-1">El barrio es obligatorio</p>
                )}

                <label>Ciudad</label>
                <input
                  onChange={handleChange("city")}
                  value={values.city}
                  type="text"
                  className={
                    validateCity ? "form-control is-invalid" : "form-control"
                  }
                  placeholder="Ciudad"
                />

                {validateCity && (
                  <p className="text-danger my-1">La ciudad es obligatoria</p>
                )}

                <label className="mb-3">Pais</label>
                <Select
                  name="country"
                  value={values.country}
                  onChange={handleChange("country")}
                  style={{ borderColor: validateCountry ? "red" : "" }}
                >
                  <option value="">-- Seleccione pais--</option>
                  <option value="argentina">Argentina</option>
                  <option value="bolivia">Bolivia</option>
                  <option value="chile">Chile</option>
                  <option value="peru">Perú</option>
                  <option value="venezuela">Venezuela</option>
                </Select>

                {validateCountry && (
                  <p className="text-danger my-1">El pais es obligatorio</p>
                )}

                <label>Provincia</label>
                <input
                  onChange={handleChange("province")}
                  value={values.province}
                  type="text"
                  className={
                    validateProvince
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  placeholder="Provincia"
                />
                {validateProvince && (
                  <p className="text-danger my-1">La provincia obligatorio</p>
                )}

                <label>Codigo postal</label>
                <input
                  onChange={handleChange("zipCode")}
                  value={values.zipCode}
                  type="text"
                  className={
                    validateZipcode ? "form-control is-invalid" : "form-control"
                  }
                  placeholder="Cod postal"
                />
                {validateZipcode && (
                  <p className="text-danger my-1">
                    El codigo postal es obligatorio
                  </p>
                )}

                <label>Telefono</label>
                <input
                  onChange={handleChange("phoneNumber")}
                  value={values.phoneNumber}
                  type="number"
                  className={
                    validatePhonenumber
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  placeholder="Telefono"
                />
                {validatePhonenumber && (
                  <p className="text-danger my-1">El telefono es obligatorio</p>
                )}

                <label>Email</label>
                <input
                  onChange={handleChange("email")}
                  value={values.email}
                  type="email"
                  className={
                    validateEmail ? "form-control is-invalid" : "form-control"
                  }
                  placeholder="Email"
                />
                {validateEmail && (
                  <p className="text-danger my-1">El email es obligatorio</p>
                )}

{validarEmail && (
                <p className="text-danger my-1">El Email debe contener @ y .</p>
              )}


                <p className="mt-4 subtitulo">Laborales</p>
                <Linea />

                <label>Legajo</label>
                <input
                  onChange={handleChange("docket")}
                  value={values.docket}
                  type="text"
                  className={
                    validateDocket ? "form-control is-invalid" : "form-control"
                  }
                  placeholder="Legajo"
                />
                                {validateDocket && (
                  <p className="text-danger my-1">El legajo es obligatorio</p>
                )}

              </form>
            </div>

            <div className="formulario__footer">
              <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
              <ButtonCrear onClick={clickSubmit}>Crear</ButtonCrear>
            </div>
          </div>
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default NuevoProfesor;