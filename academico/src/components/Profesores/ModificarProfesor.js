import {useState,useEffect} from 'react'
import { updateProfesor } from './apiProfesores'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'



const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarProfesor = ({changeToggle,alumno,alumnoId}) => {
  const [email,setValidarEmail] = useState(false)
  const [values, setValues] = useState({
    docket: "",
    name: "",
    surname: "",
    document: "",
    birthDate: "",
    gender: "",
    nationality: "",
    address: "",
    neighborhood: "",
    city: "",
    province: "",
    country: "",
    zipCode: "",
    phoneNumber: "",
    email: ""
   
  });


  useEffect(()=>{

    if(alumnoId){
      

      console.log('soy values',values)
      console.log('soy alumno',alumnoId)

      setValues({
        id: alumnoId.id,
        docket: alumnoId.docket,
        name: alumnoId.name,
        surname: alumnoId.surname,
        document: alumnoId.document,
        birthDate: alumnoId.birthDate,
        gender: alumnoId.gender,
        nationality: alumnoId.nationality,
        address: alumnoId.address,
        neighborhood: alumnoId.neighborhood,
        city: alumnoId.city,
        province: alumnoId.province,
        country: alumnoId.country,
        zipCode: alumnoId.zipCode,
        phoneNumber: alumnoId.phoneNumber,
        email: alumnoId.email
      });


    
    }

  },[alumnoId])
console.log('ss',values)

    const clickSubmit = () => {

      console.log(values)

      if(email === false){

        updateProfesor(values)
        .then(data => {
          console.log(data)
  
          if(data){
          if(data === true){
  
            toast.success("Alumno modificado");
            changeToggle();
          }
          if(data === false){
  
            toast.error("Error al modificar");
            changeToggle();
          }
        }
        else{
          toast.error("Problema con la api");
          changeToggle();
        }
  
        })
      }


    }

    const handleChange = name => e => {
      setValues({...values, [name]: e.target.value})



      if(name === "email"){
  
        if(e.target.value.indexOf('@') !== -1 &&  e.target.value.indexOf('.') !== -1 ){
          setValidarEmail(false)
       }else{
        setValidarEmail(true)
      }
  
        
  
      }
  
  
  }


    return ( 
        <>
            <AgregarFormularioAlumno >

<div className="formulario__head">
            <div>
                <TextFormulario>Modificar Docentes</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}><i class="fas fa-times"></i></div>
        </div>

        <div className="formulario__body ">
        <div>

            <form className="input-form">

                <p className="mt-2 subtitulo">Datos personales</p>
                <Linea/>
                <label>Nombre</label>
                <input onChange={handleChange} defaultValue={alumnoId ? alumnoId.name : false} type="text" className="input" placeholder="Nombre"/>
                <label>Apellido</label>
                <input  onChange={handleChange('surname')} defaultValue={alumnoId ?alumnoId.surname : false} type="text" className="input" placeholder="Apellido"/>
                <label>Dni</label>
                <input  onChange={handleChange('document')}  defaultValue={alumnoId ? alumnoId.documentation : false} type="number" className="input" placeholder="Dni"/>
              
                <label>Dni</label>
                <input  onChange={handleChange('document')}  defaultValue={ alumnoId ?  alumnoId.cuil : false} type="number" className="input" placeholder="cuil"/>
              
                <label>Fecha de nacimiento</label>
                <input  onChange={handleChange('birthDate')}  defaultValue={ alumnoId ? alumnoId.birthDate : false} type="date" className="input" placeholder="Cuil"/>



                <label>Sexo</label>
                <div className="radio d-flex justify-content-center" >
                <label>Masculino</label>
                <input 
                    type="radio"

                    checked={ alumnoId ?  alumnoId.gender === "masculino" : false}
                    name="gender"
                    value="masculino"
                    onChange={handleChange('gender')}
                /> 

                <label>Femenino</label>

                <input 
                    type="radio"

                    checked={ alumnoId ? alumnoId.gender === "femenino" : false}
                    name="gender"
                    value="femenino"
                    onChange={handleChange('gender')}
                /> 
                </div>
        

            <label>Nacionalidad</label>
                <input  onChange={handleChange('nationality')}  defaultValue={ alumnoId ? alumnoId.nacionality : false} type="text" className="input" placeholder="Apellido"/>




   <p className="mt-4 subtitulo">Contactos</p>
                <Linea/>
             

            <label>Domicilio</label>
                <input  onChange={handleChange('address')}  defaultValue={ alumnoId ?  alumnoId.address : false} type="text" className="input"   placeholder="Domicilio"/>
               

                <label>Barrio</label>
                <input onChange={handleChange('neighborhood')}  defaultValue={ alumnoId ?  alumnoId.neighborhood : false} type="text" className="input" placeholder="Barrio"/>
               

            <label>Ciudad</label>
                <input onChange={handleChange('city')}  defaultValue={alumnoId ?  alumnoId.city : false} type="text" className="input" placeholder="Ciudad"/>


                <label className="mb-3" >Pais</label>
                <Select
                    name="country"
                    defaultValue={alumnoId ? alumnoId.country : false}

                    onChange={handleChange('country')}
                >
                    <option value="">-- Seleccione pais--</option>
                    <option value="argentina">Argentina</option>
                    <option value="brasil">Brasil</option>
                
                    <option value="uruguay">Uruguay</option>
                </Select>


                <label>Provincia</label>
                <input  onChange={handleChange('province')}  defaultValue={ alumnoId ?  alumnoId.province : false} type="text" className="input" placeholder="Provincia"/>
            
            
            

            <label>Codigo postal</label>
                <input onChange={handleChange('zipCode')}  defaultValue={ alumnoId ? alumnoId.zipCode  : false}  type="text" className="input" placeholder="Cod postal"/>
            
                <label>Telefono</label>
                <input onChange={handleChange('phoneNumber')}  defaultValue={ alumnoId ? alumnoId.phone : false} type="number" className="input" placeholder="Telefono"/>
            
                
                <label>Email</label>
              <input
                onChange={handleChange("email")}
                defaultValue={alumnoId ? alumnoId.email : false}
                type="email"
                className="input"
                placeholder="Email"
              />

{email && (
                <p className="text-danger my-1">El Email debe contener @ y .</p>
              )}

                <p className="mt-4 subtitulo">Altas y bajas</p>
                <Linea/>
             
            <label>Fecha de alta</label>
                <input onChange={handleChange('zipCode')}  defaultValue={ alumnoId ?  alumnoId.fecha_alta : false}  type="text" className="input" placeholder="fecha alta"/>
            
                <label>Fecha de baja</label>
                <input onChange={handleChange('phoneNumber')}  defaultValue={ alumnoId ?  alumnoId.fecha_baja : false} type="number" className="input" placeholder="fecha baja"/>
            


            </form>
       
        </div>

        <div className="formulario__footer">
        <ButtonCancelar  onClick={changeToggle}>
            Cancelar
            </ButtonCancelar>

            <ButtonCrear onClick={clickSubmit}  >
            Modificar
            </ButtonCrear>
            
        </div>

        </div>
      
            
        </AgregarFormularioAlumno> 
    
        </>
     );
}
 
export default ModificarProfesor;