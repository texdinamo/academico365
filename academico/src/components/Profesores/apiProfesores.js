export const create = (post) => {
    return fetch(`${process.env.REACT_APP_TEACHER_URL}`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}

export const updateProfesor = (post) => {
    return fetch(`${process.env.REACT_APP_TEACHER_URL}`, {
        method: "PUT",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response.json()
        })
        .catch(err => console.log(err))
}

export const deleteProfesores = (post) => {
    return fetch(`${process.env.REACT_APP_TEACHER_URL}/remove`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}


export const getProfesores = () => {
    return fetch(`${process.env.REACT_APP_TEACHER_URL}`, {
        method: "GET"
    })
        .then(response => {
            return response.json()
        })
        .catch(err => console.log(err))
}

export const getProfesorId = (id) => {
    return fetch(`${process.env.REACT_APP_TEACHER_URL}/${id}`, {
        method: "GET"
    })
        .then(response => {
            return response.json()
        })
        .catch(err => console.log(err))
}