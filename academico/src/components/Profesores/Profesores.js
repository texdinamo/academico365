import React,{useState,useEffect} from 'react';
import {Link} from 'react-router-dom'
import Navbar from '../estructura/Navbar'
import SweetAlert from 'react-bootstrap-sweetalert';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import {URL} from '../../valores'
import {AgregarFormularioAlumno,ButtonAgregar,ButtonCrear,ButtonCancelar,
  TextFormulario,TextInfo,ContainerMenu,SubTextInfo,Linea} from '../styled-component/Navbar'
import * as ReactBootstrap from 'react-bootstrap'
import down from '../../img/down.png'
import styled from 'styled-components'

import { deleteProfesores , getProfesores, getProfesorId ,create } from './apiProfesores'
import {ToastContainer, toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.min.css'

import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import ModificarProfesor from './ModificarProfesor'


const Select = styled.select`

width: 100%;
color: #BBC5D5;
border: 1px solid #BBC5D5;
padding: 10px;
border-radius: 4px;
`

export const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`;



const Profesores = () => {

  const { SearchBar } = Search;

  const [eliminar, setEliminar] = useState(false)
  const [values,setValues] = useState()
  const [toggle , setToggle] = useState(false)
  const [alumnos, setAlumnos] = useState([])
  const [alumno , setAlumno] = useState()
  const [alumnoId, setAlumnoId] = useState();
  const [confirmar, setConfirmar] = useState(false);
  const [confirmarError, setConfirmarError] = useState(false);


  const changeToggle = () => setToggle(prevLogin => !prevLogin)
  

  const changeEliminar = () => setEliminar(prevLogin => !prevLogin)

    const changeConfirmar = () => setConfirmar(prevLogin => !prevLogin)


  
const cambio = async () => {
  setEliminar(false);
 

  console.log('AAAAA',alumnoId)

  const val = {
    "user": {
  "id": alumnoId.id
  },
    "userType": "TEACHER"
  }

  deleteProfesores(val).then(data => {
      

      if(data){

      
      if(data.ok === true){

        setConfirmar(true);
      
      }

      if(data.ok === false){
          setConfirmarError(true)
      }
    }else{
      setConfirmarError(true)
    }
    })
    
}

useEffect(() => {

  
  getProfesores().then(data => {
    console.log(data)
    let result = []
    for(let val of data){
      result.push(val.user)
    }
  setAlumnos(result)
  })



  if(confirmar || alumnoId || !toggle){
    getProfesores()
    
  }

  console.log(alumnoId)
}, [confirmar , alumnoId,toggle]);

console.log(alumnoId)

  const columns = [

    {dataField: "id", text: 'Legajo',sort: true,  searchable: true,
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  },
    {dataField: "name", text: 'Nombre',sort: true,  searchable: true,
  
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  },
    {dataField: "surname", text: 'Apellido',sort: true,  searchable: true,
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  },
    {dataField: "documentation", text: 'Dni',sort: true,  searchable: true,
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  },
  {
    dataField: "link",
    text: "Accion",
    formatter: (rowContent, row) => {
      return (
        <>
          <i
            className="fas fa-pencil-alt mr-3 text-success"
            onClick={changeToggle}
          ></i>

          <Link
            to={{
              pathname: "/docentes/docente",
              state: { fromDashboard: row.id },
            }}
            className="link-i"
          >
            <i className="far fa-eye"></i>
          </Link>

          <i
            className="far fa-trash-alt ml-3 text-danger"
            onClick={changeEliminar}

          ></i>
        </>
      );
    },
  }
  ]


  const rowEvents = {
    onClick: (e, row, rowIndex) => {
      console.log(row,e)
      setAlumnoId(row);
    },
    onMouseEnter: (e, row, rowIndex) => {
      setAlumnoId(row);
      console.log(row,e)
    },
  };
  



  const options = {
    paginationSize: 3,
    pageStartIndex: 0,
    withFirstAndLast: false,
    firstPageText: "First",
    prePageText: "Anterior",
    nextPageText: "Siguiente",

    showTotal: false,
    disablePageTitle: true,
  };

    
  


    return ( 

      <>
        <Navbar/>
        <div className="dashboard">
      <ToastContainer/>
      <ToolkitProvider keyField="id" data={alumnos} columns={columns} search>
        {(props) => (
          <div>
            <div className="barra">
              <i className="fas fa-search ml-3 mt-2"></i>
              <SearchBar placeholder="Buscar docente" {...props.searchProps} />
            </div>
            <BootstrapTable
              rowEvents={rowEvents}
              {...props.baseProps}
              pagination={paginationFactory(options)}
            />
          </div>
        )}
      </ToolkitProvider>


      {toggle && <AgregarShadow onClick={changeToggle} /> } 


 {eliminar && (
        <SweetAlert
          primary
          showCancel
          cancelBtnBsStyle="secondary"
          style={{
            fontSize: "12px",
            fontWeight: "700",
            width: "500px",
            height: "240px",
          }}
          onConfirm={() => cambio()}
          onCancel={() => setEliminar(false)}
          confirmBtnText="Aceptar"
          title="Estas seguro de realizar esta accion?"
          focusCancelBtn
        >
          <p style={{ fontSize: "14px", margin: "5px" }}>
            Esta apunto de realizar una accion irreversible
          </p>

          <p style={{ fontSize: "14px", marginBottom: "20px" }}>
            Confirma tu accion
          </p>
        </SweetAlert>
      )}

      {confirmar && (
        <SweetAlert
          success
          title="Eliminado"
          onConfirm={() => changeConfirmar()}
        ></SweetAlert>
      )}

{confirmarError && (
        <SweetAlert
          danger
          title="Error"
          onConfirm={() => setConfirmarError(false)}
        ></SweetAlert>
      )}

      {toggle && 
      
      (

      <ModificarProfesor changeToggle={changeToggle} alumnoId={alumnoId}  alumno={alumno} />
      )
      
      }


        

        
        </div>
        </>
     );
}

export default Profesores;

