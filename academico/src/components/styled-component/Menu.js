import styled from 'styled-components'


const Container = styled.div`
    margin-top: 0px;
    width: 60px;
    z-index:10000!important;

`

const ImgLogo = styled.img`
cursor: pointer;
`

const ListItem = styled.ul`
    color: #F3F8FE;
    background-color: #161B2E;
    margin-bottom: 0px!important;
    margin-left: 0px;
    width: 220px;
    height: 300px;
  
    z-index:-1000000!important;
`
