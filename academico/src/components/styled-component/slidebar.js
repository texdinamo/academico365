import { fadeInLeft, zoomIn } from 'react-animations'
import styled , {keyframes} from 'styled-components'

 const fadeInLeftAnimation = keyframes`${fadeInLeft}`;
const zoomInAnimation = keyframes`${zoomIn}`

export const ContainerMenu = styled.div`
    margin-top: 0px;
    width: 60px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    z-index:10000!important;
    padding: 0px 0px 25px 0px;
    height: 600px!important;

`

export const ImgLogoPrincipal = styled.img`
cursor: default;
padding: 10px 0px;
margin-bottom: 10px;
`
export const ImgLogo = styled.img`
cursor: pointer;
`
export const ImgLogoFinal = styled.img`
cursor: pointer;
`
export const Container = styled.div`
 background-color: #ffff; 
    position: fixed;
    left: 0;
    bottom: 0;
    top: 0;
    width: 80px;
    z-index: 1000;
    display: flex;
    border-right: 2px solid #E2E7EE;
`

export const Container2 = styled.div`
 background-color:  white;//#f7f8fa; 
    position: fixed;
    left: 0;
    bottom: 0;
    top: 0;
    width: 500px;
    overflow-x: auto;
    heigth: 100vh;
    z-index: 950;
    display: flex;
    animation: .6s ${fadeInLeftAnimation};
` 
export const Container3 = styled.div`
padding: 10px 0px;
background-color: white;
border-radius: 10px;
height: auto;
width: 60%;
animation: .6s ${zoomInAnimation};
position: fixed;
    left: 23%;
    top: 10%;
z-index:10000;

`


export const ContainerCuotas = styled.div`
padding: 10px 0px;
background-color: white;
border-radius: 10px;
height: auto;
width: 60%;
animation: .6s ${zoomInAnimation};
position: fixed;
    left: 23%;
    top: 10%;
z-index:10000;
min-height: auto;
overflow-y: scroll;

`


export const DialogShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 940;
  left: 60px;
`;


export const Imagen = styled.img`
width: 27px;



`
export const ImagenFinal = styled(Imagen)`

margin-top: 10px;


`

export const MenuAlumnos = styled.div`
    background-color: white;
    min-width: 78px;
    height: 63px;
    border-right: ${props => props.borderSelection };
    text-align: center;
    padding-top: 5px 0px;
    padding: 6px 0px;
    cursor: pointer;
    margin: 0px 0px;
    
    
`

export const TitleIcon = styled.p`
color: #bbc5d5;
padding: 5px 0px;
text-align: center;
font-size: 9px;
font-weight: bold;
margin-bottom: 5px;
`
