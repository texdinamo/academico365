import { fadeInLeft } from 'react-animations'
import styled , {keyframes} from 'styled-components'



export const Title = styled.p`
font: var(--unnamed-font-style-normal) normal bold var(--unnamed-font-size-14)/16px Raleway;
letter-spacing: var(--unnamed-character-spacing-0);
text-align: left;
font: normal normal bold 14px/16px Raleway;
letter-spacing: 0px;
color: "red";
text-transform: capitalize;
opacity: 1;

`

export const Modelo = styled.div`
width: 100%;
height: 116px;
background: #FFFFFF 0% 0% no-repeat padding-box;
border-radius: 12px;
opacity: 1; 
font: var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-normal) var(--unnamed-font-size-14)/17px var(--unnamed-font-family-lato);
letter-spacing: var(--unnamed-character-spacing-0);
text-align: left;
font: normal normal normal 11px Lato;
letter-spacing: 0px;
color: #354052;
opacity: 1;
padding-top: 29px;
padding-left: 21px;

`

export const Box = styled(Modelo)`
margin: 0px 30px;

`
export const BoxBegin = styled(Modelo)`
margin-right: 30px;
`

export const BoxEnd = styled(Modelo)`
margin-left: 30px;
`
export const Text = styled.p`
margin: 0px;

`
export const Number = styled.p`
font: var(--unnamed-font-style-normal) normal 900 33px/40px var(--unnamed-font-family-lato);
letter-spacing: var(--unnamed-character-spacing-0);
text-align: left;
font: normal normal 900 33px/40px Lato;
letter-spacing: 0px;
color: #11141A;
opacity: 1;
padding:0px;
margin:0px;
padding-top: 6px;

`
export const NumberDanger = styled(Number)`
color: #F35162;
`
export const Imagen = styled.img`
height: 10px;
width: 19px;
margin-bottom: 7px;
margin-left: 8px;

`

export const Panel = styled.div`
background: #FFFFFF 0% 0% no-repeat padding-box;
border-radius: 12px;
opacity: 1;
margin-top: 17px;
height: 70vh;
width: 100%;
margin-bottom: 20px;
overflow: hidden;


`

export const PanelHead = styled.div`

    
    
    width: 100%;
    border-bottom: 1px solid #d4d6dd;
    font-size: 13px;
    color: #354052;
    font-weight: 700;
    font-family: "Raleway";
    padding: 20px;


`