import { fadeInRight } from 'react-animations'
import styled, { keyframes } from 'styled-components'
const zoomInAnimation = keyframes`${fadeInRight}`;

export const ContainerMenu = styled.div`
  background-color: #ffff;
  position: fixed;
  top: 0;
  z-index:100;
  //padding: 10px 20px;
  left: 0;
  margin: 0px;
  width: 100%;
  height: 72px;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
  z-index:100;
  padding-left: 100px;
  padding-right: 27px;
  box-shadow:  0 0 20px 0 rgba(0,0,0,0.1); 
`
export const InputNav = styled.input`
  width: 800px;
  height: 44px;
  border-radius: 22px;
  background-color: #f7f8fa;
  outline: none;
  font-size: 12px;
  line-height: 17px;
  color: #bbc5d5;
  font-weight: 400;
  font-family: "Lato";
  border-color: transparent;
`
export const ButtonAgregar = styled.button`
  width: auto;
  height: 42px;
  padding-left: 30px;
  padding-right: 30px;
  border-radius: 6px;
  background-color: #009DDC;
  font-size: 11px;
  line-height: 15px;
  color: #ffffff;
  font-weight: 900;
  font-family: "Lato";
  cursor: pointer;
`
export const Select = styled.select`
  width: 100%;
  color: #BBC5D5;
  border: 1px solid #BBC5D5;
  padding: 10px;
  border-radius: 4px;
  font-size: 13px;
`
export const Linea = styled.hr`
  width: 100%;
  border: 1px solid #BBC5D5;
  background-color: #BBC5D5;
`
export const ButtonCrear = styled(ButtonAgregar)`
  
`
export const ButtonCancelar = styled(ButtonAgregar)`
  background-color: #bbc5d5;
  width: 110px
`
export const TextInfo = styled.p`
  font-size: 28px;
  line-height: 17px;
  color: #354052;
  font-weight: 700;
  font-family: "Raleway";
  margin-top: 40px;
`
export const SubTextInfo = styled.p`
  margin-top: 5px;
  font-size: 8px;
  color: #bbc5d5;
  text-align: left;
`
export const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`
export const AgregarFormulario = styled.div`
  background-color:  white;//#f7f8fa; 
  position: fixed;
  right: 0;
  bottom: 0;
  top: 0;
  width: 360px;
  height: 100%;
  overflow-y: scroll;
  z-index: 10000;
  animation: .6s ${zoomInAnimation};
`
export const AgregarFormularioAlumno = styled.div`
  background-color:  white;//#f7f8fa; 
  position: fixed;
  right: 0;
  bottom: 0;
  top: 0;
  width: 500px;
  height: 100%;
  overflow-y: scroll;
  z-index: 10000;
  animation: .6s ${zoomInAnimation};
`
export const TextFormulario = styled.p`
  font-size: 18px;
  color: #322A7D;
  font-weight: 700;
  margin-top: 2px;
  font-family: "Raleway";
`