import React, { useState, useEffect } from 'react';
import Navbar from '../estructura/Navbar'
import { Email, Item, Span, A, renderEmail ,Box,Image } from 'react-html-email'
import { Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import Moment from 'react-moment'
import moment from 'moment'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Spinner } from 'react-bootstrap'
import 'react-tabs/style/react-tabs.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import { URL } from '../../valores'
import {
  AgregarFormularioAlumno, ButtonCrear, ButtonCancelar,
  TextFormulario, TextInfo, ContainerMenu, SubTextInfo, Linea
} from '../styled-component/Navbar'
import  BootstrapTable  from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import { useDatos } from '../hooks/useDatos'
import styled from 'styled-components'
import { Container3 } from '../styled-component/slidebar'
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { ToastContainer, toast } from 'react-toastify'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import 'react-toastify/dist/ReactToastify.min.css'
import filterFactory, { dateFilter, Comparator, selectFilter } from 'react-bootstrap-table2-filter'

import { getAlumnos, getAlumnoId } from '../Alumnos/apiAlumnos'
import { getCursos ,getCursoId } from '../Cursos/apiCursos'
import {getPlanId ,getCuotas , cancelarCuotas,envioEmail } from './apiAdministracion'
import {getTutores} from '../Tutores/apiTutores'
import { getProfesorId } from '../Profesores/apiProfesores'
const Select = styled.select`
  width: 20%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  height: 35px;
  border-radius: 4px;
`;


const ButtonAgregar = styled.button`

width: 143px;
height: 42px;
border-radius: 6px;
background-color:  #009DDC;
font-size: 11px;
line-height: 15px;
color: #ffffff;
font-weight: 900;
font-family: "Lato";
cursor: pointer;
align-self: center;

`
const ButtonModal = styled.button`

width: 143px;
height: 42px;
border-radius: 6px;
background-color:  #009DDC;
font-size: 11px;
line-height: 15px;
color: #ffffff;
font-weight: 900;
font-family: "Lato";
cursor: pointer;
align-self: center;

`

const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`;

const Cuotas = () => {
  const { SearchBar } = Search;

  const [validar,setValidar] = useState(false)
  const [cuotaEstudiante,setCuotaEstudiante] = useState([])
  const [confirmar, setConfirmar] = useState(0)
  const [eliminar, setEliminar] = useState(false)
  const [alumno, setAlumno] = useState([])
  const [listaAlumnos, setListaAlumnos] = useState()
  const [toggle, setToggle] = useState(false)
  const [allAlumnos, setAllAlumnos] = useState()
  const [toggleAlumno, setToggleAlumno] = useState(false)
  const [detalleAlumno, setDetalleAlumno] = useState()
  const [toggleModificar, setModificar] = useState(false)
  const [toggleModificarHorario, setModificarHorario] = useState(false)
  const [agregarPlan, setAgregarPlan] = useState(false)
  const [desde,setDesde] = useState('')
  const [hasta,setHasta] = useState('')
  const [cuotasValue, setCuotasValue] = useState('')
  const [validateDesde,setValidateDesde] = useState(false)
  const [validateHasta, setValidateHasta] = useState(false)
  const [validateCuotasValue,setValidateCuotasValue] = useState(false)
  const [studentFilter, setStudentFilter] = useState([])
  const [cursos,setCursos] = useState([])
  const [cuotasStudent,setCuotasStudent] = useState('')
  const [err ,setErr] = useState(false)
  const [seleccionados,setSeleccionados] = useState([])
  const [toggleEmail,setToggleEmail] = useState(false)
  const [toggleAnular,setToggleAnular] = useState(false)
  const [validarAnular,setValidarAnular] = useState(false)
  const [toggleRespuesta,setToggleRespuesta] = useState(false)
  const [respuestaOk,setRespuestaOk] = useState(false)
  const [esperandoRespuesta,setEsperandoRespuesta] = useState(false)
  const changeToggle = () => setToggle(prevLogin => !prevLogin)

  const changeTogglePlan = () => setAgregarPlan(prevLogin => !prevLogin)

  const changeToggleModificar = () => setModificar(prevLogin => !prevLogin)

  const changeToggleModificarHorario = () => setModificarHorario(prevLogin => !prevLogin)

  const changeToggleEmail = () => setToggleEmail(prevLogin => !prevLogin)


  const changeToggleEsperandoRespuesta = () => setEsperandoRespuesta(prevLogin => !prevLogin)

  const changeToggleAlumno = () => setToggleAlumno(prevLogin => !prevLogin)


  const changeToggleAnular = () => setToggleAnular(prevLogin => !prevLogin)
  
  const changeToggleRespuesta = () => setToggleRespuesta(prevLogin => !prevLogin)





  useEffect(  () => {

    const cargandoDatos = async () => {
      const cursosAll = await getCursos()
      setCursos(cursosAll)
      
      const studiantesAll = await getAlumnos()
      setListaAlumnos(studiantesAll)
    }

    cargandoDatos()

    const getAll = async () => {
      setValidar(true)
      const data = await getCuotas()
      

      let result = []

    for  (let val of data){


      if(val.idStudent && val.idDivition && val.idPlan){


        const studentGet =  await getAlumnoId(val.idStudent)

        console.log('studentget',studentGet)

          if(studentGet){

            const cursoGet = await  getCursoId(val.idDivition)
         

            const planGet = await  getPlanId(val.idPlan)
  
            let nuevaCuota = {
            active: val.active,
            description: val.description,
            endDate: val.endDate,
            finalPrice: val.finalPrice,
            id: val.id,
            idDivition: cursoGet.name,
            idInstitution: val.idInstitution,
            idPlan: planGet.name,
            idStudent: val.idStudent,
            nameStudent: `${studentGet.name ? studentGet.name : null } ${studentGet.surname ? studentGet.surname : null}`,
            price: val.price,
            rate: val.rate,
            rateType: val.rateType,
            divitionId: val.idDivition
          }
          result.push(nuevaCuota)
        }

      }
    


    }


    let filterResult = result.filter(value =>  value.active === true && value.finalPrice > 0 || value.active === true && value.finalPrice === 0 )


    
    setValidar(false)



    setAlumno(filterResult)
    
    }


    getAll()
                           

   
    
    

  }, [])



  const anular = () => {
    setErr(true)
    changeToggleAnular()
    if(seleccionados.length === 0){
      setValidarAnular(false)
      setErr(true)
    }
    if(seleccionados.length > 0){
      setValidarAnular(true)
      setErr(false)
    }
  

  }

  const anularPost =  async() => {
    changeToggleAnular()

    let result = []

    for(let val of seleccionados){
      result.push(val.id)
    }
    

    const resp = await cancelarCuotas(result)

    changeToggleRespuesta()

    if(resp){
      if(resp.ok === true){

        setRespuestaOk(true)

      }

      if(resp.ok === false){
        setRespuestaOk(false)
      }

    }else{

      setRespuestaOk(false)
    }

  }



  const enviarEmail = async() => {
  
    const result  = await getTutores()

    let tutoresEmail = []
    for(let val of result){

      tutoresEmail.push(val.user.email)
    }

    let estudiantesEmail = []
   

  
  
    if(seleccionados.length === 0){
      changeToggleEmail()
      setErr(true)
    }
    if(seleccionados.length > 0){

      changeToggleEsperandoRespuesta()
   //   setErr(false)
    console.log(seleccionados)
     
    let valores = new Set()
      for(let idStudent of seleccionados){
          valores.add(idStudent.idStudent)
      }

        for(let val of valores){
          
          var resultadoFilter = seleccionados.filter((value) => value.idStudent  === val );
          console.log('resultado filter',resultadoFilter)

          let student = await getAlumnoId(resultadoFilter[0].idStudent)

          const edad =  moment().diff(student.birthDate, 'years');


          if(student){

          let msg = ''
          for(let val of resultadoFilter){
           msg+= "\n" + "\n" + `Alumno/a : ${val.nameStudent}` + "\n" + `Descripción: ${val.description}` + "\n" + `Plan: ${val.idPlan}` + "\n" + `Curso: ${val.idDivition}` + "\n"  + `Vencimiento: ${val.endDate}` + "\n" + `Precio: ${val.price}` + "\n" + `Precio final: ${val.finalPrice}` + "\n" + `Tipo de plan: ${val.rateType}` + "\n" + `Interes: ${val.rate}` + "\n" 
          }

          
          if(edad > 18 ){

            let email = {
              "content": msg,
              "subject": `Estado de cuotas del alumno ${resultadoFilter[0].nameStudent}`,
                "to": student.email,
                "validKey": "044c03d8a383e32c9ad3e0633e37069b56843b3d87b6255665d1bdacd39c8720d3ea653437b485590524ba7fb77d46ddab59695c9c2ddc238bccda3af3ae180f17"
              
            }
  
           
            let sendEmail = await envioEmail(email)
           console.log('resp envio',sendEmail)
            }



          let tutores = await getTutores()

           for await( let tutor of tutores ){
              
              console.log(tutor.user.email)

              let email = {

                "content": msg ,
                "subject": `Estado de cuotas del alumno ${resultadoFilter[0].nameStudent}`,
                "to": tutor.user.email,
                "validKey": "044c03d8a383e32c9ad3e0633e37069b56843b3d87b6255665d1bdacd39c8720d3ea653437b485590524ba7fb77d46ddab59695c9c2ddc238bccda3af3ae180f17"
              }
              
              let sendEmail = await envioEmail(email)
              console.log('sendemail',sendEmail)
  
            }


          }


        }





      
      changeToggleEsperandoRespuesta()
      changeToggleEmail()
      setErr(false)
      

   
    }
  
    

  }

  const clickSearch = async () => {
    setValidar(true)
//    changeToggle()
    if(!desde){
      setValidateDesde(true)
    }
    if(!hasta){
      setValidateHasta(true)
    }
    if(!cuotasValue){
      setValidateCuotasValue(true)


    }


    const resultadoGet = await axios.get(`http://vps-1924366-x.dattaweb.com:8003/api/institutions/1/fees?dateFrom=${desde}&dateTo=${hasta}&idDivition=${cuotasValue}&idStudent=${cuotasStudent}`)
   
    let result = []

    for  await (let val of resultadoGet.data){


      if(val.idStudent && val.idDivition && val.idPlan){


        const studentGet =  await getAlumnoId(val.idStudent)
        console.log(studentGet)

         const cursoGet = await  getCursoId(val.idDivition)

          const planGet = await  getPlanId(val.idPlan)


          let nuevaCuota = {
            active: val.active,
            description: val.description,
            endDate: val.endDate,
            finalPrice: val.finalPrice,
            id: val.id,
            idDivition: cursoGet.name,
            idInstitution: val.idInstitution,
            idPlan: planGet.name,
            idStudent: val.idStudent,
            nameStudent: `${studentGet.name} ${studentGet.surname}`,
            price: val.price,
            rate: val.rate,
            rateType: val.rateType
          }
          result.push(nuevaCuota)

      }
    


    }

    setValidar(false)
    setConfirmar(1)
    setAlumno(result)
      
  }






  const selectOptions = {
    0: 'todas',
    1: 'no canceladas',
  };
 


  const selectRow = {
    mode: 'checkbox',
    onSelect: (row, isSelect, rowIndex, e) => {
      // ...
 

      if(isSelect){

       
        seleccionados.push(row)
        

      }

      if(!isSelect){
          let resultado = seleccionados.filter((value) => value.id !== row.id)
          setSeleccionados(resultado)

      }
      


      
    },
     onSelectAll: (isSelect, rows, e) => {
       setSeleccionados([])
      if(isSelect){
        setSeleccionados(rows)
      
       }
       if(!isSelect){
          setSeleccionados([])
       }
  }

  };


  const options = {
    paginationSize: 3,
    pageStartIndex: 0,
    withFirstAndLast: false,
    firstPageText: "First",
    prePageText: "Anterior",
    nextPageText: "Siguiente",

    showTotal: false,
    disablePageTitle: true,
  };

  
  const columnsAdministracion = [

    {
      dataField: "nameStudent", text: 'Alumno/s', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
    {
      dataField: "idDivition", text: 'Curso', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    }
    , {
      dataField: "description", text: 'Cuota', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
   
  
    {
      dataField: "rate", text: 'Porcentaje', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    }, {
      dataField: "rateType", text: 'Tipo de Plan'
      , sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
      {
        dataField: "finalPrice", text: 'Imp .a pagar'
        , sort: true, searchable: true,
        sortCaret: (order, column) => {
          if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
          else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
          else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
          return null;
        }
    },
    {
      dataField: "idPlan", text: 'Plan'
      , sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
      {
        dataField: "endDate", text: 'Vencimiento'
        , sort: true, searchable: true,
        sortCaret: (order, column) => {
          if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
          else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
          else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
          return null;
        }
      },
    {
      dataField: 'link',
      text: 'Acciones',
      formatter: (rowContent, row) => {
        return (
          <>
            <i class="fas fa-pencil-alt mr-3 text-success"></i>

          </>
        )
      }
    },
  ]







  const cambio = () => {
    setEliminar(false)
    setConfirmar(true)
  }

 

  return (
    <>
      <Navbar />
      <div className="dashboard">

        <ToastContainer />


        {toggle && <AgregarShadow onClick={changeToggle} />}

        {toggleModificar && <AgregarShadow onClick={changeToggleModificar} />}

        {toggleModificarHorario && <AgregarShadow onClick={changeToggleModificarHorario} />}


        {toggleAlumno && <AgregarShadow onClick={changeToggleAlumno} />}




        {eliminar &&

          <SweetAlert
            primary
            showCancel
            cancelBtnBsStyle="secondary"
            style={{ fontSize: '12px', fontWeight: '700', width: '500px', height: '240px' }}
            onConfirm={() => cambio()}
            onCancel={() => setEliminar(false)}
            confirmBtnText="Aceptar"

            title="Estas seguro de realizar esta accion?"
            focusCancelBtn
          >
            <p style={{ fontSize: '14px', margin: '5px' }} >Esta apunto de realizar una accion irreversible</p>

            <p style={{ fontSize: '14px', marginBottom: '20px' }} >Confirma tu accion</p>

          </SweetAlert>



        }


{toggle  &&
<Container3>

<div className="mx-2 my-3 d-flex justify-content-between">
<p className="info-descripcion" >Cuotas</p>
<div className="delete " onClick={changeToggle}><i class="mr-2 fas fa-times"></i></div>    
</div>

<BootstrapTable keyField='id' data={ cuotaEstudiante }

pagination={paginationFactory(options)}
 columns={columnsAdministracion} />

</Container3>}


{toggleEmail && err  &&
  <SweetAlert 
          danger
          title="debe seleccionar 1 persona"
          onConfirm={() => changeToggleEmail()} >
          </SweetAlert>
}



{toggleEmail && !err  &&
  <SweetAlert 
          success
          title="operacion correcta"
          onConfirm={() => changeToggleEmail()} >
          </SweetAlert>
}


{esperandoRespuesta && 

<SweetAlert className="boton-sacar"
   showCancel={false}
   showConfirm={false}
  focusCancelBtn={true}
                  
disableButton={true}
>
  <div className="d-flex justify-content-center flex-column p-5">
      <div>
<Spinner animation="grow" variant="primary" size="xl" />
      </div>
      <div className="mt-5">
        <h3 className="text-center text-dark">Enviando email...</h3>
      </div>
  </div>
</SweetAlert>


}


{toggleRespuesta && 
<div>

{respuestaOk ? 
  <SweetAlert 
          success
          title="Operacion exitosa!"
          onConfirm={() => changeToggleRespuesta()} >
          </SweetAlert>
:
<SweetAlert 
          danger
          title="Error en la operacion"
          onConfirm={() => changeToggleRespuesta()} >
          </SweetAlert>
}

</div>
}


{toggleAnular &&
<div>

  {
    validarAnular ? 
    <SweetAlert
          primary
          showCancel
          cancelBtnBsStyle="secondary"
          style={{
            fontSize: "12px",
            fontWeight: "700",
            width: "500px",
            height: "240px",
          }}
          onConfirm={() => anularPost()}
          onCancel={() => changeToggleAnular()}
          confirmBtnText="Aceptar"
          title="Estas seguro de realizar esta accion?"
          focusCancelBtn
        >
          <p style={{ fontSize: "14px", margin: "5px" }}>
            Esta apunto de realizar una accion irreversible
          </p>

          <p style={{ fontSize: "14px", marginBottom: "20px" }}>
            Confirma tu accion
          </p>
        </SweetAlert>
    :
    <SweetAlert 
          danger
          title="Debes seleccionar al menos"
          onConfirm={() => changeToggleAnular()} >
          </SweetAlert>
  }


</div>

}



        {alumno ?

          <Tabs>
            <TabList>

              <Tab>Cuotas</Tab>

            </TabList>

            <TabPanel className="alumno">




              <ToolkitProvider keyField="id" data={alumno} columns={columnsAdministracion} search>
                {(props) => (
                  <div>

                    <div className="header-descripcion d-flex justify-content-between ">


                      <div className="fechas">
                        <input
                        onChange={event => setDesde(event.target.value)}

                          type="date"
                          className={
              validateDesde
                ? "input desde  is-invalid"
                : " input desde "
            }
                        />
                        <input
                          type="date"
                          onChange={event => setHasta(event.target.value)}
                          className={
              validateHasta
                ? "input hasta  is-invalid"
                : " input hasta "
            }
                        />
                      </div>


                      <Select
                        name="bloodtype"

                        onChange={event => setCuotasStudent(event.target.value)}
                      >
                        <option value="">-- Alumno --</option>
                        <option value="">Todos</option>
                        
                         { listaAlumnos ?  listaAlumnos.map((item,i)=>(

                        <option key={i} value={item.id}>{item.name} {item.surname}</option>
                        
                        ))
                        :
                        null
                      }

                      </Select>

                      <Select
                        name="bloodtype"

                        onChange={event => setCuotasValue(event.target.value)}
                      >
                        <option value="">-- Cursos --</option>
                        {cursos.length ?  <option value="">Todos</option> : null }
                       
                         { cursos.length ?  cursos.map((item,i)=>(

                        <option key={i} value={item.id_divition}>{item.name}</option>
                        
                        ))
                        :
                        <option  value="">No hay cursos</option>
                        
                      }

                      </Select>



                      <Select
                        name="bloodtype"

                      >
                        <option value="">-- Cuotas --</option>
                        <option value="todas">Todas</option>
                        <option value="no cancelada">No canceladas</option>

                      </Select>

                      <ButtonModal onClick={clickSearch} >
                        Filtrar
                      </ButtonModal>
                      

                    </div>
                    <div className="header-descripcion d-flex justify-content-center mb-4 ">

                    <SearchBar placeholder="Buscar Alumno" {...props.searchProps} />

                    </div>

                    {validar ? 
                    
                    
                    
                    
                    
                    <div className="d-flex justify-content-center align-items-center mt-5 spinner ">


<Spinner animation="grow" variant="primary" size="xl" />

</div>

:


<BootstrapTable
                      {...props.baseProps}
                      selectRow={ selectRow }
                      filter={filterFactory()}
                      pagination={paginationFactory(options)}
                      width="500"
/>


                    

      
                    }

{validar ? null : 



<div className="d-flex justify-content-between  valor-padding"  >

<div>
<ButtonAgregar  onClick={enviarEmail} >Enviar e-mail</ButtonAgregar>
</div>
<div>

<div></div>

<ButtonAgregar  onClick={anular} className="mr-1">Anular</ButtonAgregar>

</div>

</div>


}

                  </div>
                )}
              </ToolkitProvider>



            </TabPanel >

          </Tabs>


          :

          <div className="d-flex justify-content-center align-items-center mt-5 spinner ">


            <Spinner animation="grow" variant="primary" size="xl" />

          </div>

        }

      </div>
    </>
  );
}

export default Cuotas;