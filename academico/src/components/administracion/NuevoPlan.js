import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import {create} from './apiAdministracion'
import {
    AgregarShadow,
    AgregarFormulario,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    Select,
    Linea,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    AgregarFormularioAlumno,
  } from "../styled-component/Navbar";


const NuevoPlan = ({changeToggle}) => {


  ///plan
  const [checkedMatricula, setCheckedMatricula] = useState(false);

  const [checkedCuota, setCheckedCuota] = useState(false);


  const [checkedExamen, setCheckedExamen] = useState(false);



  const [validatePlanName,setValidatePlanName] = useState(false)

  const [validateTipoPlan,setValidateTipoPlan] = useState(false)

  const [validateModalidadPlan,setValidateModalidadPlan] = useState(false)

  const [validateCantCuotas,setValidateCantCuotas] = useState(false)

  const [validateAlicuotaPlan,setValidateAlicuotaPlan] = useState(false)

  const [validateApplyEnrollment,setValidateApplyEnrollment] = useState(false)

  const [validateApplyfees,setValidateApplyfees] = useState(false)

  const [validateApplyExam,setValidateApplyExam] = useState(false)

  const [plans,setPlans] = useState({
    name: "",
    tipoPlan: "",
    modalidadPlan: "",
    cantCuotas: "",
    alicuotaPlan: "",
    applyEnrollment: "",
    applyfees: "",
    applyExam: ""

  })

let flag = 1
  const handleChangePlan = (name) => (event) => {
    setPlans({ ...plans, [name]: event.target.value });
 
    if (name === "name") {
      setValidatePlanName(false);
    }
    if (name === "tipoPlan") {
      setValidateTipoPlan(false);
    }
    if (name === "cantCuotas") {
      setValidateCantCuotas(false);
    }
    if (name === "alicuotaPlan") {

      let valor = event.target.value
    
      let valorParse = parseFloat(valor).toFixed(2)
      console.log(valorParse)

    setPlans({ ...plans, ["alicuotaPlan"]: valorParse });


      setValidateAlicuotaPlan(false);
    }

    if (name === "modalidadPlan") {
      setValidateModalidadPlan(false);
    }
    
    if (name === "applyEnrollment") {
      setValidateApplyEnrollment(false);
    }

    if (name === "applyfees") {
      setValidateApplyfees(false);
    }

    if (name === "applyExam") {
      setValidateApplyExam(false);
    }


  }




  const clickSubmitPlan = (event) => {
    event.preventDefault();



    if (!plans.name) {
      setValidatePlanName(true);
    }
    if (!plans.tipoPlan) {
      setValidateTipoPlan(true);
    }
    if (!plans.cantCuotas) {
      setValidateCantCuotas(true);
    }
    if (!plans.alicuotaPlan) {
      setValidateAlicuotaPlan(true);
    }

    if (!plans.modalidadPlan) {
      setValidateModalidadPlan(true);
    }
    
    if (!plans.applyEnrollment) {
      setValidateApplyEnrollment(true);
    }

    if (!plans.applyfees) {
      setValidateApplyfees(true);
    }

    if (!plans.applyExam) {
      setValidateApplyExam(true);
    }



      if (plans.name && plans.tipoPlan && plans.cantCuotas && plans.alicuotaPlan && plans.modalidadPlan ){
              
      let val =  {
          "alicuotaPlan": parseFloat(parseFloat(plans.alicuotaPlan).toFixed(2)),
          "applyEnrollment": checkedMatricula ,
          "applyExam": checkedExamen,
          "applyFee": checkedCuota,
          "cantCuotas": plans.cantCuotas,
          "id_institution": 1,
          "modalidadPlan": plans.modalidadPlan,
          "name": plans.name,
          "tipoPlan": plans.tipoPlan
        }

          console.log(val)
        create(val)
        .then(data => {
          
          if(data === true){

            toast.success("Plan agregado");
            changeToggle();
          }
          if(data === false){

            toast.error("Error al agregar");
            changeToggle();
          }

        })

      }

    
  };


    return ( 
        <>
      <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Agregar Plan</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>

          <div className="formulario__body ">
            <div>
              <form className="input-form">
          
                <label>Nombre de plan</label>
                <input

                onChange={handleChangePlan("name")}
                values={plans.name}
                  type="text"
                  placeholder="Nombre"
                     className={
                  validatePlanName ? "form-control is-invalid" : "form-control"
                }
                />
                 {validatePlanName && (
                <p className="text-danger my-1">El nombre es obligatorio</p>
              )}

               <label className="mb-3">Tipo de plan</label>
                <Select
                  className={
                  validateTipoPlan ? "form-control is-invalid" : "form-control"
                }
                  value={plans.tipoPlan}
                  onChange={handleChangePlan("tipoPlan")}
                >

                  
                  <option value="">-- Seleccione plan --</option>
   
                  <option value="INTERES">INTERES</option>
                  <option value="DESCUENTO">DESCUENTO</option>
                </Select>
                {validateTipoPlan && (
                <p className="text-danger my-1">El tipo de plan es obligatorio</p>
              )}


               <label className="mb-3">Modalidad</label>

                <Select
                  className={
                  validateTipoPlan ? "form-control is-invalid" : "form-control"
                }
                  value={plans.modalidadPlan}
                  onChange={handleChangePlan("modalidadPlan")}
                >
 

              
                  
                  <option value="">-- Seleccione modalidad --</option>
   
                  <option value="MONTO_UNICO_FINANCIADO">MONTO UNICO FINANCIADO</option>
                  <option value="MONTO_PERIODICO">MONTO PERIODICOS</option>
 
 
                </Select>

                {validateModalidadPlan && (
                <p className="text-danger my-1">La modalidad es obligatoria</p>
              )}



                <label>Cantidad de cuotas</label>
                <input

                  value={plans.cantCuotas}
                  onChange={handleChangePlan("cantCuotas")}
                  type="number"
                  className={
                  validateCantCuotas ? "form-control is-invalid" : "form-control"
                }
                  placeholder="Cantidad de cuotas"
                />

                 {validateCantCuotas && (
                <p className="text-danger my-1">La cantidad de cuotas es obligatorio</p>
              )}

                <label>Porcentaje</label>
                <input
                 
                 value={plans.alicuotaPlan}
                  onChange={handleChangePlan("alicuotaPlan")}
                  type="number"
                      className={
                  validateAlicuotaPlan ? "form-control is-invalid" : "form-control"
                }
                  placeholder="Porcentaje"
                />
 {validateAlicuotaPlan && (
                <p className="text-danger my-1">La alicuota es obligatorio</p>
              )}


          <div className="col-12 d-flex justify-content-between p-0 mt-3">
            <div className="col-5 p-0 d-flex flex-wrap align-content-center"><label>Conceptos sobre los que aplica cuota</label></div>
              <div className="col-6 d-flex flex-column mt-3">
              <div className="d-flex">
              <input 
              onChange={handleChangePlan("applyEnrollment")}

              onChange={() => setCheckedMatricula(!checkedMatricula)}
              value={plans.applyEnrollment}
              
              className="" type="checkbox"

              />



              <label className="mx-1">Matricula</label>
              </div>
             

              <div className="d-flex mt-3">
              <input 
              onChange={handleChangePlan("applyFee")}

              onChange={() => setCheckedCuota(!checkedCuota)}
              value={plans.applyFee}
              
              className="" type="checkbox"

              />
              <label className="mx-1">Cuota</label>
              
              </div>
             
              <div className="d-flex mt-3">
              <input
              
              onChange={handleChangePlan("applyExam")}
              value={plans.applyExam}
              onChange={() => setCheckedExamen(!checkedExamen)}
              className="" type="checkbox"

              />
                <label className="mx-1">Derecho de Examen</label>
            
              </div>



            </div>

            </div>
              </form>
            </div>

            <div className="formulario__footer">
              <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>

              <ButtonCrear onClick={clickSubmitPlan}>Crear</ButtonCrear>
            </div>
          </div>
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default NuevoPlan;