export const create = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/plans`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        console.log(response)
        return response.ok
    })
    .catch(err => console.log(err))
}

export const cancelarCuotas = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/fees/cancel`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        console.log(response)
        return response
    })
    .catch(err => console.log(err))
}




export const simulatePost = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/fees/simulate`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        console.log(response)
        return response.json()
    })
    .catch(err => console.log(err))
}



export const deletePlan = (post/*,token*/) => {

    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/plans/remove`,{
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        console.log(response)
        return response
    })
   
    .catch(err => console.log(err))
}

export const getPlanes = () => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/plans`,{
         method: "GET"
     })
     .then(response => {
         return response.json()
     })
     .catch(err => console.log(err))
 }

 export const getPlanId = (id) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/plans/${id}`,{
         method: "GET"
     })
     .then(response => {
         return response.json()
     })
     .catch(err => console.log(err))
 }


 export const updatePlan = (post) => {
     
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/plans/update`,{
        method: "POST",
        headers: {
            Accept: 'application/json',
           "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        console.log(response)
        return response
    })
    .catch(err => console.log(err))
}


export const getCuotas = () => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/fees`,{
         method: "GET"
     })
     .then(response => {
         return response.json()
     })
     .catch(err => console.log(err))
 }



 export const getEstudiantesCursos = () => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/divition/student`,{
         method: "GET"
     })
     .then(response => {
         return response.json()
     })
     .catch(err => console.log(err))
 }


 export const createFees = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/fees`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        console.log(response)
        return response
    })
    .catch(err => console.log(err))
}

export const envioEmail = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/email`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        console.log(response)
        return response
    })
    .catch(err => console.log(err))
}

