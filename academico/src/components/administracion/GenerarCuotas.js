import React, { useState, useEffect } from 'react';
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import Moment from 'react-moment'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Spinner } from 'react-bootstrap'
import 'react-tabs/style/react-tabs.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import { URL } from '../../valores'
import {
  AgregarFormularioAlumno, ButtonCrear, ButtonCancelar,
  TextFormulario, TextInfo, ContainerMenu, SubTextInfo, Linea
} from '../styled-component/Navbar'
import _ from 'lodash'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import { useDatos } from '../hooks/useDatos'
import styled from 'styled-components'
import { ContainerCuotas, Container3 } from '../styled-component/slidebar'
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { ToastContainer, toast } from 'react-toastify'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import 'react-toastify/dist/ReactToastify.min.css'
import filterFactory, { dateFilter, Comparator, selectFilter } from 'react-bootstrap-table2-filter'
import { getAlumnos, getAlumnoId } from '../Alumnos/apiAlumnos'
import { getCursos ,getCursoId } from '../Cursos/apiCursos'
import {getPlanId ,getCuotas , getEstudiantesCursos,  getPlanes , simulatePost, createFees} from './apiAdministracion'

const Select = styled.select`
  width: 70%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  height: 35px;
  border-radius: 4px;
`;


const ButtonAgregar = styled.button`

width: 143px;
height: 42px;
border-radius: 6px;
background-color: #009DDC;
font-size: 11px;
line-height: 15px;
color: #ffffff;
font-weight: 900;
font-family: "Lato";
cursor: pointer;
align-self: center;

`


const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`;

const GenerarCuotas = () => {
  const { SearchBar } = Search;

  const [validar,setValidar] = useState(false)
  const [resultado , setResultado] = useState([])
  const [cuotaEstudiante,setCuotaEstudiante] = useState([])
  const [confirmar, setConfirmar] = useState(false)
  const [eliminar, setEliminar] = useState(false)
  const [alumno, setAlumno] = useState([])
  const [alumnoReset,setAlumnoReset] = useState([])
  const [actualizar,setActualizar] = useState(false)
  const [listaAlumnos, setListaAlumnos] = useState()
  const [toggle, setToggle] = useState(false)
  const [allAlumnos, setAllAlumnos] = useState()
  const [toggleAlumno, setToggleAlumno] = useState(false)
  const [detalleAlumno, setDetalleAlumno] = useState()
  const [toggleModificar, setModificar] = useState(false)
  const [toggleModificarHorario, setModificarHorario] = useState(false)
  const [agregarPlan, setAgregarPlan] = useState(false)
  const [desde,setDesde] = useState('')
  const [hasta,setHasta] = useState('')
  const [planes,setPlanes] = useState([])
  const [valuePlanes,setValuePlanes] = useState()
  const [toggleSimulate,setToggleSimulate] = useState(false)
  const [toggleGenerate ,setToggleGenerate] = useState(false)
  const [refresh,setRefresh] = useState(false)
  
  ///nuevo state

  const [plan,setPlan] = useState('')
  const [cuota,setCuota] = useState('')
  const [matricula,setMatricula] = useState('')
  const [examen,setExamen] = useState('')
  const [dia,setDia] = useState('')
  const [simulatePlan,setSimulatePlan] = useState('')
  const [informacion,setInformacion] = useState()
  const [resultadoSimulate,setResultadoSimulate] = useState(true)
  const [respuesta,setRespuesta] = useState([])
  const [seleccionados,setSeleccionados] = useState([])

  const [datos,setDatos] = useState({
    examen: '',
    cuota: '',
    matricula: '',
    dia: '',
    plan: ''
   })

   const [update,setUpdate] = useState(false)
   const [datosAlumnos,setDatosAlumnos] = useState([])
  const [validatePlan,setValidatePlan] = useState(false)
  const [validateCuota,setValidateCuota] = useState(false)
  const [validateMatricula,setValidateMatricula] = useState(false)
  const [validateExamen,setValidateExamen] = useState(false)
  const [validateDia,setValidateDia] = useState(false)
  const [selectPlan,setSelectPlan] = useState({

  }) 
  const [arrHandle,setArrHandle]  = useState([])
  const [errores,setErrores] = useState([])
  const [validarGenerate,setValidarGenerate] = useState(false)
  const [noMostrar,setNoMostrar] = useState(false)
  const [respuestaOk,setRespuestaOk] = useState(false)

  const changeToggleSimulate = () => {
    setToggleSimulate(prevLogin => !prevLogin)
    setRespuesta([])
    setResultadoSimulate(true)
  }


  const changeToggle = () => setToggle(prevLogin => !prevLogin)

  const changeTogglePlan = () => setAgregarPlan(prevLogin => !prevLogin)

  const changeToggleGenerate = () =>{
    setToggleGenerate(prevLogin => !prevLogin)
  
  }
  const changeToggleModificar = () => setModificar(prevLogin => !prevLogin)

  const changeToggleModificarHorario = () => setModificarHorario(prevLogin => !prevLogin)


  const changeToggleAlumno = () => setToggleAlumno(prevLogin => !prevLogin)
  
console.log(seleccionados)
  useEffect(() => {


    console.log(seleccionados)

    const getAll = async () => {
       const resultPlanes = await getPlanes()
      console.log(resultPlanes)
       setPlanes(resultPlanes)

       const result = await getEstudiantesCursos()
      console.log(result)
      let arr = []

      let cont  = 0

      for (let data of result){


        let obj = {
          id: cont++,
          idStudent: data.idStudent,
          name: data.name,
          surname: data.surname,
          idDivition: data.idDivition,
          nameDivition: data.nameDivition
        }

        arr.push(obj)
      }
    

    
    
      setAlumno(arr)
    }


    const getAllUpdate = async () => {
      const resultPlanes = await getPlanes()
     console.log(resultPlanes)
      setPlanes(resultPlanes)

      const result = await getEstudiantesCursos()
     console.log(result)
     let arr = []

     let cont  = 0

     for (let data of result){


       let obj = {
         id: cont++,
         idStudent: data.idStudent,
         name: data.name,
         surname: data.surname,
         idDivition: data.idDivition,
         nameDivition: data.nameDivition
       }

       arr.push(obj)
     }
   

   
   
     setDatosAlumnos(arr)
   }


    console.log(alumno)
    getAll()
/*
    if(update){
      console.log('eeeesss')

      setSeleccionados([])
      getAllUpdate()
    }
*/
  }, [])



  const getAllReset = async () => {
    const resultPlanes = await getPlanes()
   console.log(resultPlanes)
    setPlanes(resultPlanes)

    const result = await getEstudiantesCursos()
   console.log(result)
   let arr = []

   let cont  = 0

   for (let data of result){


     let obj = {
       id: cont++,
       idStudent: data.idStudent,
       name: data.name,
       surname: data.surname,
       idDivition: data.idDivition,
       nameDivition: data.nameDivition
     }

     arr.push(obj)
   }
 

 
 
   setAlumnoReset(arr)
   setActualizar(true)
 }



const clickGenerar = () => {
  setNoMostrar(false)
  setDatos({
    examen: '',
    cuota: '',
    matricula: '',
    dia: '',
    plan: ''
   })

   console.log(seleccionados)
  changeToggleGenerate()  

  for(let val of arrHandle){
    let result = seleccionados.find(value => value.id === parseInt(Object.keys(val)[0],10) )

    if(result !== undefined){
      result.idPlan = isNaN(parseInt(Object.values(val)[0],10)) ? '' : parseInt(Object.values(val)[0],10) 
    }

}

setErrores([])
for(let voidPlan of seleccionados){
        console.log(voidPlan)   
      if(voidPlan.idPlan === ''){
        errores.push(voidPlan.id)
      }
}

console.log(seleccionados)
console.log(errores)

if(seleccionados.length > 0 ){
   console.log(seleccionados)
}

if(errores.length === 0 && seleccionados.length > 0 ){
  setValidarGenerate(true)

}else{
  setSeleccionados([])
  setValidarGenerate(false)
}



  
}



const limpiarTabla = () => {
   changeToggleGenerate()
   setUpdate(true)
   window.location.reload(false);

  }


const clickPostGenerar = async () => {

  if(!datos.dia){
    setValidateDia(true)
  }
  if(!datos.examen){
    setValidateExamen(true)
  }
  if(!datos.cuota){
    setValidateCuota(true)
  }
  if(!datos.matricula){
    setValidateMatricula(true)
  }



  if(datos.dia && datos.examen && datos.cuota && datos.matricula ){
    setValidar(true)
    setNoMostrar(true)
      let result = []
    for(let seleccionado of seleccionados){

      const val = {
        "contentRequest": {
          "endDayFee": parseInt(datos.dia,10),
          "enrollmenFeeDate": datos.matricula,
          "enrollment": true,
          "enrollmentDate": datos.cuota,
          "exam": true,
          "examDate": datos.examen,
          "fee": true
        },
        "idDivition": seleccionado.idDivition,
        "idPlan": seleccionado.idPlan,
        "idStudent": seleccionado.idStudent,
        "nameStudent": seleccionado.nameStudent
      }

      result.push(val)
    

    }

    const respuesta = await createFees(result)
  
    if(respuesta){
  
      setValidar(false)
     
      if(respuesta.ok === true){
        getAllReset()
        console.log('entro ok true')
        setUpdate(true)
      setRespuestaOk(true)
      setRefresh(true)


    }
    if(respuesta.ok === false){

      console.log('entro ok true')
      setRespuestaOk(false)
    }
  }else{
    setRespuestaOk(false)
  }

  
  }else{
    console.log('no pasa')
  }


}


const handleChangePlan = (name) => (event) => {
    setSelectPlan({ ...selectPlan, [name]: event.target.value });
    
    
    arrHandle.push({[name]:event.target.value})
    
    }

  const clickSearch = async (idStudent,idCurso) => {
   
    changeToggle()
    setValidar(true)


    const resultadoGet = await axios.get(`http://vps-1924366-x.dattaweb.com:8003/api/institutions/1/fees?dateFrom=${desde}&dateTo=${hasta}&idDivition=${idCurso}&idStudent=${idStudent}`)
   
     console.log(resultadoGet)
    let result = []
  
    for  await (let val of resultadoGet.data){


      if(val.idStudent && val.idDivition && val.idPlan){


        const studentGet =  await getAlumnoId(val.idStudent)


         const cursoGet = await  getCursoId(val.idDivition)

          const planGet = await  getPlanId(val.idPlan)


          let nuevaCuota = {
            
            active: val.active,
            description: val.description,
            endDate: val.endDate,
            finalPrice: val.finalPrice,
            id: val.id,
            idDivition: cursoGet.name,
            idInstitution: val.idInstitution,
            idPlan: planGet.name,
            idStudent: `${studentGet.name} ${studentGet.surname}`,
            price: val.price,
            rate: val.rate,
            rateType: val.rateType
          }
          console.log('nuevo',nuevaCuota)
          result.push(nuevaCuota)

      }
    


    }

    console.log('result',result)
    setValidar(false)
    setResultado(result)
    
      
  }


  const rowEvents = {
    onClick: (e, row, rowIndex) => {
      console.log(row)
      setInformacion(row)
    },
    onMouseEnter: (e, row, rowIndex) => {
      console.log(row)
      setInformacion(row)
    },
  };


  const handleChange = (name) => (event) => {
    setDatos({ ...datos, [name]: event.target.value });
 
    if (name === "dia") {
      setValidateDia(false);
    }

    if (name === "examen") {
      setValidateExamen(false);
    }


    if (name === "cuota") {
      setValidateCuota(false);
    }

    if (name === "matricula") {
      setValidateMatricula(false);
    }

    if(name === "plan"){
      setValidatePlan(false)
    }
 
  }

  const clickSimulate =   async () => {

    if(!datos.dia){
      setValidateDia(true)
    }
    if(!datos.examen){
      setValidateExamen(true)
    }
    if(!datos.cuota){
      setValidateCuota(true)
    }
    if(!datos.matricula){
      setValidateMatricula(true)
    }

    if(!datos.plan){
      setValidatePlan(true)
    }


    if(datos.dia && datos.examen && datos.cuota && datos.matricula && datos.plan){
    setResultadoSimulate(false)
    setValidar(true)



      const val = {
        "contentRequest": {
          "endDayFee": datos.dia,
          "enrollmenFeeDate": datos.matricula,
          "enrollment": true,
          "enrollmentDate": datos.cuota,
          "exam": true,
          "examDate": datos.examen,
          "fee": true
        },
        "idDivition": informacion.idDivition,
        "idPlan": parseInt(datos.plan,10),
        "idStudent": informacion.idStudent,
        "nameStudent": informacion.name
      }

console.log(datos.plan)
      console.log('informacion simullate',informacion)

      console.log('resultado de simulate -----',matricula,examen,dia,cuota)
  
  
      console.log(val)
      const result  = await simulatePost(val)
      
      setValidar(false)
      setRespuesta(result)
      console.log('resultado del la llamada',result)
  


  

    }



  }



  const selectOptions = {
    0: 'todas',
    1: 'no canceladas',
  };

const  handleOnSelectAll = (isSelect, rows) => {
    setSeleccionados([])
  
    let result = []
    if(isSelect){

           for( let val of rows){
             let fila = {
               id : val.id,
               idDivition: val.idDivition,
               idPlan: '',
               idStudent: val.idStudent,
             nameStudent: val.name
               
           
                   }

                   result.push(fila)
   
           }

           console.log('looop',result)
           setSeleccionados(result)
     
    }
    
    if(!isSelect){
       setSeleccionados([])
    }

   
    
  }

  const handleOnSelect = (row, isSelect,rowIndex) => {
       
console.log(row)
      if(isSelect){

        let val = {
    id : row.id,
    idDivition: row.idDivition,
    idPlan: '',
    idStudent: row.idStudent,
  nameStudent: `${row.name} ${row.surname}`
    
  
        }
        seleccionados.push(val)
        
        
        console.log(seleccionados)
        
  
      }
  
      if(!isSelect){
  
          console.log(row)
          let resultado = seleccionados.filter((value) => value.id !== rowIndex)
          setSeleccionados(resultado)
         console.log(seleccionados)
      }
      
  

    

  }

  


 
  const selectRow = {
    mode: 'checkbox',
      clickToSelect: false,
    onSelect:  handleOnSelect,
     onSelectAll:  handleOnSelectAll
      
  
  
  };

  const selectRowReset = {
    mode: 'checkbox',
      clickToSelect: false,
    onSelect:  handleOnSelect,
     onSelectAll:  handleOnSelectAll
      
  
  
  };




const columnsSimulate = [{
  dataField: 'description',
  text: 'Concepto'
}, {
  dataField: 'endDate',
  text: 'Vencimiento'
}, {
  dataField: 'price',
  text: 'Valor'
},
{
  dataField: 'rateType',
  text: 'Tipo Plan'
}, {
  dataField: 'rate',
  text: 'Porcentaje'
},
{
  dataField: 'finalPrice',
  text: 'Importe a Pagar'
}

];
  
  const columnsAdministracion = [

    {
      dataField: "name", text: 'Nombre', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
    {
      dataField: "surname", text: 'Apellido', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    }
    , {
      dataField: "nameDivition", text: 'Curso', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
    {
      dataField: "", text: 'Plan',
       formatter: (rowContent, row) => {
       console.log('acaaaa.....',row.id + 10)
        return (
          <div className="">
            <Select  
            
            className={`${ errores.includes(row.id) ? 'form-control is-invalid' : 'form-control'}`}
            onChange={handleChangePlan(`${row.id}`)}
            
            >
                <option  value="">Elegir plan</option>

                { planes.length ?  planes.map((item,i)=>(

                  <option key={i} value={item.id_plan}  >{item.name}</option>

                  ))
                  :
                  <option  value="">No hay planes</option>

                  }
              </Select>
          </div>
        )
      }
    }
 ,
    {
      dataField: 'link',
      text: 'Accion',
      formatter: (rowContent, row) => {
       console.log('contenido de la fila',row)
        return (
          <div className="generar">

            <i className="far fa-eye" onClick={changeToggleSimulate}>

            </i>
            <i  onClick={e => clickSearch(row.idStudent,row.idDivition)} className="fas fa-search  mr-3 text-primary"></i>

          </div>
        )
      }
    }
  ]


  

  const columnsCuota = [

   
    {
      dataField: "idStudent", text: 'Alumno/s', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
    {
      dataField: "idDivition", text: 'Curso', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    }
    , {
      dataField: "description", text: 'Cuota', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
   
  
    {
      dataField: "rate", text: 'Porcentaje', sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    }, {
      dataField: "rateType", text: 'Tipo de Plan'
      , sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
      {
        dataField: "finalPrice", text: 'Imp .a pagar'
        , sort: true, searchable: true,
        sortCaret: (order, column) => {
          if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
          else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
          else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
          return null;
        }
    },
    {
      dataField: "idPlan", text: 'Plan'
      , sort: true, searchable: true,
      sortCaret: (order, column) => {
        if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
        else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
        else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
        return null;
      }
    },
      {
        dataField: "endDate", text: 'Vencimiento'
        , sort: true, searchable: true,
        sortCaret: (order, column) => {
          if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
          else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
          else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
          return null;
        }
      },

    {
      dataField: 'link',
      text: 'Acciones',
      formatter: (rowContent, row) => {
        return (
          <>
            <i class="fas fa-pencil-alt mr-3 text-success"></i>

          </>
        )
      }
    },
  ]





  const options = {
    paginationSize: 3,
    sizePerPage: 10,
    pageStartIndex: 0,
    withFirstAndLast: false,
    firstPageText: 'First',
    prePageText: 'Anterior',
    nextPageText: 'Siguiente',

    showTotal: false,
    disablePageTitle: true
  }




  const cambio = () => {
    setEliminar(false)
    setConfirmar(true)
  }






  


  return (
    <>
      <Navbar />
      <div className="dashboard">

        <ToastContainer />


        {toggle && <AgregarShadow onClick={changeToggle} />}

        {toggleModificar && <AgregarShadow onClick={changeToggleModificar} />}

        {toggleModificarHorario && <AgregarShadow onClick={changeToggleModificarHorario} />}


        {toggleAlumno && <AgregarShadow onClick={changeToggleAlumno} />}


      {toggleSimulate &&    <AgregarShadow onClick={changeToggleSimulate}/> }


      {toggleGenerate &&    <AgregarShadow /> }

        {eliminar &&

          <SweetAlert
            primary
            showCancel
            cancelBtnBsStyle="secondary"
            style={{ fontSize: '12px', fontWeight: '700', width: '500px', height: '240px' }}
            onConfirm={() => cambio()}
            onCancel={() => setEliminar(false)}
            confirmBtnText="Aceptar"

            title="Estas seguro de realizar esta accion?"
            focusCancelBtn
          >
            <p style={{ fontSize: '14px', margin: '5px' }} >Esta apunto de realizar una accion irreversible</p>

            <p style={{ fontSize: '14px', marginBottom: '20px' }} >Confirma tu accion</p>

          </SweetAlert>



        }









{toggle  &&
<ContainerCuotas>

<div className="header-cuotas d-flex justify-content-between">
<p className="info-descripcion" >Cuotas</p>
<div className="delete " onClick={changeToggle}><i class="mr-2 fas fa-times"></i></div>    
</div>


{validar ? 

<div className="d-flex justify-content-center align-items-center mt-5 spinner ">


<Spinner animation="grow" variant="primary" size="xl" />

</div>
:
<div className="scroll-cuotas">
<BootstrapTable keyField='id' data={ resultado }

pagination={paginationFactory(options)}
 columns={columnsCuota} />
 </div>

}


</ContainerCuotas>}


{toggleGenerate  &&
<ContainerCuotas>

<div className="mx-2 my-3 d-flex justify-content-between">
<p className="info-descripcion" >Generar cuotas</p>
<div className="delete " onClick={changeToggleGenerate}><i class="mr-2 fas fa-times"></i></div>    
</div>
<div>
{noMostrar && 
<div>
{validar  ?

<div className="d-flex justify-content-center align-items-center mt-5 spinner ">


<Spinner animation="grow" variant="primary" size="xl" />

</div>

:
<div>


  {respuestaOk ?
<>
  <div className="bg-success col-12 d-flex   justify-content-center ">
    <div>
   <p className="text-center text-light font-weight-bolder mt-1 px-2 py-4">operacion realizada!</p>
   </div>
   <div>
   <i class="far fa-check-circle text-light pt-4"></i>
  </div>
  </div>
  <div>
    <p className="text-dark mx-3 mt-3" >Alumnos a los que se le generaron cuotas:</p>

    <ul className="listAlumno">

      { 
          
        seleccionados.length   ? seleccionados.map((item,i) =>(
          <li className="mx-3 mt-1" key={i}>- {item.nameStudent} </li>
        ))

        :
        <p>No hay alumnos</p>
      }
    </ul>
  </div>

  <div className="col-12  p-3 m-0 d-flex justify-content-end">

<ButtonAgregar onClick={limpiarTabla} >Salir</ButtonAgregar>
</div>

  </>
  :
  <p className="text-center text-danger p-5">error en la operacion

  </p>
  }

</div>

}

</div>
}



 
  {!noMostrar &&
  <div>
{  validarGenerate  ?

 <div>
<div className="d-flex flex-column justify-content-center p-5 generar-cuota">

<div className="d-flex  justify-content-center mb-3 ">

<label className="col-3 pt-2">Matricula</label>
<input
onChange={handleChange('matricula')}

type="date"
className={
validateMatricula ? "form-control is-invalid" : "form-control"
}
/>

</div>

<div className="d-flex justify-content-center mb-3">
<label className="col-3 pt-2">Cuota 1</label>
<input
type="date"
onChange={handleChange('cuota')}
className={
validateCuota ? "form-control is-invalid" : "form-control"
}
/>


</div>


<div className="d-flex justify-content-center mb-3">
<label className="col-3 pt-2">Der. Examen</label>
<input
type="date"
onChange={handleChange('examen')}
className={
validateExamen ? "form-control is-invalid" : "form-control"
}
/>


</div>


<div className="d-flex justify-content-center">
<label className="col-3 pt-2">Dias Sig</label>
<input
type="number"
onChange={handleChange('dia')}
value={datos.dia < 32 && datos.dia}
className={
validateDia ? "form-control is-invalid" : "form-control"
}
min="1" max="99"
placeholder="ingrese los dias"
/>


</div>



</div>


<div className="col-12  p-3 d-flex justify-content-end">

<ButtonAgregar onClick={clickPostGenerar} >Genear cuota/s</ButtonAgregar>
</div>

 </div>

:

<div className="col-12 p-5 ">
    <p className="text-center text-danger">Debe llenar los campos para continuar!</p>
</div> 

}
</div>
}

</div>
 
</ContainerCuotas>
}



{toggleSimulate  &&
<ContainerCuotas>

<div className="header-cuotas d-flex justify-content-between">
<p className="info-descripcion" >Simular cuota</p>
<div className="delete " onClick={changeToggleSimulate}><i class="mr-2 fas fa-times"></i></div>    
</div>



{validar  ? 

<div className="d-flex justify-content-center align-items-center mt-5 spinner ">


<Spinner animation="grow" variant="primary" size="xl" />

</div>
:
<div>
{!resultadoSimulate &&

<div className="scroll-cuotas">
<BootstrapTable keyField='description' data={ respuesta } columns={ columnsSimulate } />

<div className="col-12  p-1 d-flex justify-content-end">

<ButtonAgregar onClick={changeToggleSimulate} >Cancelar</ButtonAgregar>
</div>
</div>
}
</div>

}



{ resultadoSimulate && 

<div>


<div className="d-flex flex-column justify-content-center p-5 generar-cuota">

                          <div className="d-flex  justify-content-center mb-3 ">

                          <label className="col-3 pt-2">Seleccionar plan</label>

                          <Select
                                      onChange={handleChange('plan')}
                                      className={

                                        validatePlan ? "form-control is-invalid" : "form-control"                                      }
                                      >
                                          <option  value="">Elegir plan</option>

                                          { planes.length ?  planes.map((item,i)=>(

                                            <option key={i} value={item.id_plan}  >{item.name}</option>

                                            ))
                                            :
                                            <option  value="">No hay plan</option>

                                            }
                                        </Select>

                          </div>

                    
                      <div className="d-flex  justify-content-center mb-3 ">

                      <label className="col-3 pt-2">Matricula</label>
                        <input
                        onChange={handleChange('matricula')}

                          type="date"
                          className={
                  validateMatricula ? "form-control is-invalid" : "form-control"
                }
                        />

                      </div>

                      <div className="d-flex justify-content-center mb-3">
                      <label className="col-3 pt-2">Cuota 1</label>
                        <input
                          type="date"
                          onChange={handleChange('cuota')}
                          className={
                  validateCuota ? "form-control is-invalid" : "form-control"
                }
                        />


                      </div>
                     
                        
                    <div className="d-flex justify-content-center mb-3">
                    <label className="col-3 pt-2">Der. Examen</label>
                        <input
                          type="date"
                          onChange={handleChange('examen')}
                          className={
                  validateExamen ? "form-control is-invalid" : "form-control"
                }
                        />


                    </div>


                    <div className="d-flex justify-content-center">
                    <label className="col-3 pt-2">Dias Sig</label>
                        <input
                          type="number"
                          onChange={handleChange('dia')}
                          className={
                  validateDia ? "form-control is-invalid" : "form-control"
                }
                          placeholder="ingrese los dias"
                        />


                    </div>



                    </div>


                    <div className="col-12  p-3 d-flex justify-content-end">

<ButtonAgregar onClick={clickSimulate} >Simular cuota/s</ButtonAgregar>
</div>
                    </div>
}




</ContainerCuotas>}




        {alumno ?

          <Tabs>
            <TabList>

              <Tab>Generar cuotas</Tab>

            </TabList>

            <TabPanel className="alumno">




              <ToolkitProvider keyField="id" data={/*update ? datosAlumnos : */alumno} columns={columnsAdministracion} search>
                {(props) => (
                  <div>

                    <div className="header-descripcion d-flex justify-content-between mb-4">

                      <SearchBar placeholder="Buscar Alumno" {...props.searchProps} />


                    </div>

                      {alumno ?
                      
                      <BootstrapTable
                      {...props.baseProps}

                      rowEvents={rowEvents}
                      selectRow={selectRow}
                      filter={filterFactory()}
                      pagination={paginationFactory(options)}
                    />

                      
                      :
                      
                      <div className="d-flex justify-content-center align-items-center mt-5 spinner ">


                        <Spinner animation="grow" variant="primary" size="xl" />

                    </div>

                      
                      }                    




                    <div className="d-flex justify-content-between valor-padding">

                    <div>

                    </div>

                      <ButtonAgregar onClick={clickGenerar}>Generar cuota/s</ButtonAgregar>

                    </div>

                  </div>
                )}
              </ToolkitProvider>



            </TabPanel >

          </Tabs>


          :

          <div className="d-flex justify-content-center align-items-center mt-5 spinner ">


            <Spinner animation="grow" variant="primary" size="xl" />

          </div>

        }

      </div>
    </>
  );
}

export default GenerarCuotas;