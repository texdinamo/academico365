import {useState,useEffect} from 'react'
import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'
  import {updatePlan} from './apiAdministracion'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";


const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarPlan = ({changeToggle,alumnoId,alumno}) => {



  const [checkedMatricula, setCheckedMatricula] = useState();

  const [checkedCuota, setCheckedCuota] = useState();


  const [checkedExamen, setCheckedExamen] = useState();

  const [values,setValues] = useState({
    name: "",
    tipoPlan: "",
    modalidadPlan: "",
    cantCuotas: "",
    alicuotaPlan: "",
    applyEnrollment: "",
    applyfees: "",
    applyExam: ""

  })

  console.log('holaaaa',alumnoId)


  useEffect(()=>{

    
  console.log('holaaaa',alumnoId)


    if(alumno){

      setCheckedCuota(alumnoId.applyFee)
      setCheckedMatricula(alumnoId.applyEnrollment)
      setCheckedExamen(alumnoId.applyExam)

      setValues({

        name: alumnoId.name,
        tipoPlan: alumnoId.tipoPlan,
        modalidadPlan: alumnoId.modalidadPlan,
        cantCuotas: alumnoId.cantCuotas,
        alicuotaPlan: parseFloat(parseFloat(alumnoId.alicuotaPlan).toFixed(2)),
        applyEnrollment: alumnoId.applyEnrollment,
        applyfees: alumnoId.applyfees,
        applyExam: alumnoId.applyExam
    
      })
      
    }

  },[alumnoId])
  console.log(values)



  const clickSubmit = () => {

    const val =  {
      "alicuotaPlan": parseInt(values.alicuotaPlan,10),
      "applyEnrollment": checkedMatricula,
      "applyExam": checkedExamen,
      "applyFee": checkedCuota,
      "cantCuotas": values.cantCuotas,
      "id_institution": 1,
      "id_plan": alumnoId,
      "modalidadPlan": values.modalidadPlan,
      "name": values.name,
      "tipoPlan": values.tipoPlan
    }


    

    console.log(val)
    updatePlan(val)
    .then(data => {
      console.log(data)

      if(data){
      if(data.ok === true){

        toast.success("Alumno modificado");
        changeToggle();
      }
      if(data.ok === false){

        toast.error("Error al modificar");
        changeToggle();
      }
    }
    else{
      toast.error("Problema con la api");
      changeToggle();
    }

    })

  }

    const handleChange = name => e => {
      setValues({...values, [name]: e.target.value})

      if (name === "alicuotaPlan") {

        let valor = e.target.value
        let valorParse = parseFloat(valor).toFixed(2)
  
  
      setValues({ ...values, ["alicuotaPlan"]: valorParse });
  
  
      }
      
  }





    return ( 
        <>
        <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Modificar Plan</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
       
            <div className="formulario__body ">
            <div>
              <form className="input-form">
          
                <label>Nombre de plan</label>
                <input
                  onChange={handleChange("name")}
                
                  type="text"
                  className="input"
                  placeholder="Nombre"
                  defaultValue={alumnoId ? alumnoId.name : 'cargando'}
                />
               <label className="mb-3">Tipo de plan</label>
                <Select
                  name="bloodtype"
                  onChange={handleChange("tipoPlan")}
                
                  defaultValue={alumnoId ? alumnoId.tipoPlan : 'cargando'}
                >
                  <option value="">-- Seleccione plan --</option>
   
                  <option value="INTERES">INTERES</option>
 
                  <option value="DESCUENTO">DESCUENTO</option>
                </Select>



               <label className="mb-3">Modalidad</label>

                  <Select
                   
                    defaultValue={ alumnoId ? alumnoId.modalidadPlan : 'cargando'}
                    onChange={handleChange("modalidadPlan")}
                  >



                    
                    <option value="">-- Seleccione modalidad --</option>

                    <option value="MONTO_UNICO_FINANCIADO">MONTO UNICO FINANCIADO</option>
                    <option value="MONTO_PERIODICO">MONTOS PERIODICOS</option>


                  </Select>


                <label>Cantidad de cuotas</label>
                <input
                  onChange={handleChange("cantCuotas")}
                
                  defaultValue={alumnoId ? alumnoId.cantCuotas : 'cargando'}
                  type="number"
                  className="input"
                  placeholder="Cantidad de cuotas"
                />

                <label>Porcentaje</label>
                <input
                  onChange={handleChange("alicuotaPlan")}
                
                    defaultValue={alumnoId ? parseFloat(alumnoId.alicuotaPlan).toFixed(2)  : 'cargando'}
                  type="number"
                  className="input"
                  placeholder="Alicuotas del plan"
                />

            <div className="col-12 d-flex justify-content-between p-0 mt-3">
            <div className="col-5 p-0 d-flex flex-wrap align-content-center"><label>Conceptos sobre los que aplica cuota</label></div>
            
              
              <div className="col-6 d-flex flex-column mt-3">
              <div className="d-flex">

              <input className="" type="checkbox"
                            onChange={handleChange("applyEnrollment")}

                onChange={() => setCheckedMatricula(!checkedMatricula)}

               defaultChecked={alumnoId ? alumnoId.applyEnrollment : false}
              />
                <label className="mx-1">Matricula</label>
              </div>
              <div className="d-flex mt-3">

              <input className="" type="checkbox"
onChange={handleChange("applyFee")}

onChange={() => setCheckedCuota(!checkedCuota)}

defaultChecked={alumnoId ? alumnoId.applyFee : false}
              />
               <label className="mx-1">Cuota</label>
              </div>
              <div className="d-flex mt-3">
              <input className="" type="checkbox"
onChange={handleChange("applyExam")}
              onChange={() => setCheckedExamen(!checkedExamen)}
              
defaultChecked={alumnoId ? alumnoId.applyExam : false}
              />

                <label className="mx-1">Derecho de examen</label>
              </div>
            
            </div>
            </div>
              </form>
            </div>

            <div className="formulario__footer">
              <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>

              <ButtonCrear onClick={clickSubmit}>Modificar</ButtonCrear>
            </div>
          </div>
          
        
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default ModificarPlan;


