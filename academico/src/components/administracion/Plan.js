import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import Moment from "react-moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
import { URL } from "../../valores";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { useDatos } from "../hooks/useDatos";
import { getPlanId } from './apiAdministracion'

const Plan = (props) => {
  const [alumno, setAlumno] = useState();

  const [alumnoId, setAlumnoId] = useState();

  useEffect(() => {

      if(props.location.state.fromDashboard){
  
        getPlanId(props.location.state.fromDashboard).then(data => {
          console.log(data)
        
          if(data){
            
            setAlumno(data)
  
          }else{
            setAlumno(false)
          }
          
    })

    }
  


  }, []);

  console.log("allumno", alumno);

  return (
    <>
    <Navbar/>
    <div className="dashboard">
      {alumno ? (
        <Tabs>
          <TabList>
            <Tab>Informacion del plan</Tab>

          </TabList>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Informacion del plan</div>
            
            <div className="datos-usuario d-flex">
              <div className="datos">
               

                <div className="d-flex">
                  <h5 className="info-descripcion">Nombre:</h5>
                  <p>{alumno.name}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Tipo de plan:</h5>
                  <p>{alumno.tipoPlan}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Modalida de plan:</h5>
                  <p>{alumno.modalidadPlan}</p>
                </div>


                <div className="d-flex">
                  <h5 className="info-descripcion">Porcentaje:</h5>
                  <p>{alumno.alicuotaPlan} % </p>
                </div>


                <div className="d-flex">
                  <h5 className="info-descripcion">Cantidad de cuotas:</h5>
                  <p>{alumno.cantCuotas} </p>
                </div>

        
              </div>

              <div className="datos ml-4">
              <div className="d-flex">
                  <h5 className="info-descripcion">Matricula:</h5>

                    <p>{alumno.applyEnrollment ? "Aplica" : "No aplica"}</p>
                  
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Precio:</h5>
                  <p>{alumno.applyFee ?"Aplica" : "No aplica"}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Derecho a examen:</h5>
                  <p>{alumno.applyExam ? "Aplica" : "No aplica"}</p>
                </div>

          
              </div>

            </div>



          </TabPanel>

       
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>  
      )}
      </div>
    </>
  );
};

export default withRouter(Plan);
