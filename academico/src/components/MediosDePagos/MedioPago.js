import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import Moment from "react-moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
import { URL } from "../../valores";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { useDatos } from "../hooks/useDatos";
import { getMedioPagoId } from './apiMediosPagos'
import styled from "styled-components";



const MedioPago = (props) => {
  const [medio, setMedio] = useState();

  useEffect(() => {    
    
    getMedioPagoId(props.location.state.fromDashboard).then(data => {
      console.log("esto es la data" + data)
     setMedio(data)

    })

  }, []);

  console.log("medios", medio);

  return (
    <>
    <Navbar/>
    <div className="dashboard">
      {medio ? (
        <Tabs>
          <TabList>
            <Tab>Información</Tab>            
          </TabList>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Información</div>

            <div className="datos-usuario d-flex">
              <div className="datos">
                <div className="d-flex">
                  <h5 className="info-descripcion">Nombre:</h5>
                  <p>{medio.name}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Detalle:</h5>
                  <p>{medio.metadata}</p>
                </div>
               
                <div className="d-flex">
                  <h5 className="info-descripcion">Estado:</h5>
                  <p>{medio.status === "ACTIVE" && "ACTIVO"}</p>
                  <p>{medio.status === "INACTIVE" && "INACTIVO"}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Acción de cobranza:</h5>
                  <p>{medio.action}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">¿Cuando?:</h5>
                  <p>{medio.apply === "AL_VENCIMIENTO_DE_CUOTA" && "AL VENCIMIENTO DE CUOTA"}</p>
                  <p>{medio.apply === "SIEMPRE" && "SIEMPRE"}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Porcentaje:</h5>
                  <p>{medio.percentage}</p>
                </div>
                </div>
                
              </div>              
          </TabPanel>         
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>
      )}
      </div>
    </>
  );
};

export default withRouter(MedioPago);