import {useState,useEffect} from 'react'
import { updateMedioPago } from './apiMediosPagos'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'

const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarMediosPagos = ({changeToggle,institucion,institucionId}) => {
    const [medio, setMedio] = useState({
        name: "",
        metadata: "",
        apply: "",
        id: "",
        action: "",
        status: "",
        percentage:"",
        idInstitution: ""
      });

  useEffect(()=>{

    if(institucion){

      console.log('Soy la institucion', institucion)         
      
      setMedio({
        name: institucion.name,
        detalle: institucion.metadata,        
        id: institucion.id,
        action: institucion.action,
        status: institucion.status,
        percentage: institucion.percentage,
        apply: institucion.apply,
        idInstitution: institucion.idInstitution
      });    
    }

  },[institucion])
console.log('ss',medio)

    const clickSubmit = () => {
      const val =  {        
        "id" : medio.id,
        "action": medio.action,
        "metadata": medio.detalle,
        "name": medio.name,
        "status": medio.status,
        "percentage": medio.percentage,
        "apply": medio.apply,
        "idInstitution": medio.idInstitution
      }
      updateMedioPago(val)
      .then(data => {
        console.log(data)

        if(data){
        if(data.ok === true){
          toast.success("Medio de Pago modificada");
          changeToggle();
        }
        if(data.ok === false){
          toast.error("Error al modificar");
          changeToggle();
        }
      }
      else{
        toast.error("Problema con la api");
        changeToggle();
        }
      })

    }

    const handleChange = name => e => {
      setMedio({...medio, [name]: e.target.value})    
    }

    return ( 
        <>
           <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Modificar un Medio de Pago</TextFormulario>
            </div>
            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre</label>
              <input
                onChange={handleChange("name")}
                defaultValue={medio.name}
                type="text"
                className="input"
                placeholder="Nombre"
              />              
              <label>Detalle</label>
              <input
                onChange={handleChange("detalle")}
                defaultValue={medio.detalle}
                type="text"
                className="input"
                placeholder="Notas"
              />
              <label className="mb-3">Estado</label>
              <Select
                  className="select"                  
                  onChange={handleChange("status")}
                  defaultValue={medio.status}  >                   
                  <option value="">-- Seleccione Estado --</option>                  
                  <option value="ACTIVE">ACTIVO</option>
                  <option value="INACTIVE">INACTIVO</option>
                </Select>            
                
              <label className="mb-3">Acción de Cobranza</label>
              <Select
                  className="select"
                  defaultValue={medio.action}
                  onChange={handleChange("action")}>
                  
                  <option value="">-- Seleccione la Accion --</option>
                  <option value="NINGUNA">Ninguna</option>
                  <option value="RECARGO">Recargo</option>
                  <option value="DESCUENTO">Descuento</option>
                </Select>               

              <label className="mb-3">¿Cuando?</label>
              <Select
                  className="select"
                  defaultValue={medio.apply}
                  onChange={handleChange("apply")} 
               >                  
                  <option value=""> Seleccione la correcta </option>   
                  <option value="AL_VENCIMIENTO_DE_CUOTA">Al vencimiento</option>
                  <option value="SIEMPRE">Siempre</option>
                </Select>             

               <label>Porcentaje</label>
                <input                 
                 defaultValue={medio.percentage}
                  onChange={handleChange("percentage")}
                  type="number"
                  className="input"
                  placeholder="Porcentaje"
                />           
                           
            </form>
          </div>

          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmit}>Guardar</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>    
        </>
     );
}
 
export default ModificarMediosPagos;