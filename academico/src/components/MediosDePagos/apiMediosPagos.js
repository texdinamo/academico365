export const create = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/paymentMethods`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const updateMedioPago= (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/paymentMethods/update`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const deleteMedioPago = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/paymentMethods/remove`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}

export const getMediosPagos = () => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/paymentMethods`, {
        method: "GET"
    })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .catch(err => console.log(err))
}

export const getMedioPagoId = (id) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/paymentMethods/${id}`, {
        method: "GET"
    })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .catch(err => console.log(err))
}

