import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { create } from './apiMediosPagos'
import moment from 'moment'

import {
    AgregarShadow,
    AgregarFormulario,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    Select,
    Linea,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    AgregarFormularioAlumno,
  } from "../styled-component/Navbar";

const NuevoMedioPago = ({changeToggle}) => {   

    const [validateMetadataMedioPago, setValidateMetadata] = useState(false);
    const [validateNameMedioPago, setValidateNameMedioPago] = useState(false);
    const [validateStateMedioPago, setValidateStateMedioPago] = useState(false);
    const [validateActionMedioPago, setValidateActionMedioPago] = useState(false);
    const [validateApplyMedioPago, setValidateApplyMedioPago] = useState(false);
    const [validatePercentageMedioPago, setValidatePercentageMedioPago] = useState(false);  
    const [medio, setMedio] = useState({
        name: "",
        metadata: "",
        apply: "",        
        action: "",
        status: "",
        percentage:"",                 
               
      });

      console.log(medio)

      const handleChangeMediosPago = (name) => (event) => {
        setMedio({ ...medio, [name]: event.target.value });    
        if (name === "name") {
          setValidateNameMedioPago(false);
        }    
        if (name === "metadata") {
          setValidateMetadata(false);
        } 
        if (name === "status") {
            setValidateStateMedioPago(false);
        }        
        if (name === "action") {
            setValidateActionMedioPago(false);
        }
        if (name === "apply") {
            setValidateApplyMedioPago(false);
        }        
        if (name === "percentage") {
            let valor = event.target.value
    
            let valorParse = parseFloat(valor).toFixed(0)
            console.log(valorParse)
      
          setMedio({ ...medio, ["percentage"]: valorParse });     
      
            setValidatePercentageMedioPago(false);
          }        
      };

      const clickSubmitMediosPago = (event) => {
        event.preventDefault(event);
        if (!medio.name) {
          setValidateNameMedioPago(true);
        }
        if (!medio.metadata) {
            setValidateMetadata(true);
          }
        if (!medio.status) {
            setValidateStateMedioPago(true);
          }
        if (!medio.action) {
            setValidateActionMedioPago(true);
          }
        if (!medio.apply) {
            setValidateApplyMedioPago(true);
          }
        if (!medio.percentage) {
            setValidatePercentageMedioPago(true);
          }
        if (
          medio.name &&
          medio.metadata &&
          medio.status &&
          medio.action &&
          medio.apply &&
          medio.percentage          
        ) {

          console.log('medioPago...',medio)

        const val =  {
           
            "idInstitution": 1,
            "name": medio.name,
            "metadata": medio.metadata,            
            "status": medio.status,
            "action": medio.action,
            "apply": medio.apply,
            "percentage": medio.percentage
          }  
          create(val)
          .then(data => {
  
            if(data){
              console.log(data)
              if(data.ok === true){
    
                toast.success("Medio de pago creada");
                changeToggle();
              }
              if(data.ok === false){
                
                toast.error("Error al crear");
                changeToggle();
              }    
            }else{
              toast.error("Error en la api");
              changeToggle();
            
            }           
          })
        }
      };
        
    return ( 
        <>
                <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Crear un nuevo medio de pago</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>

          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre</label>
              <input
                onChange={handleChangeMediosPago("name")}
                value={medio.name}
                type="text"
                className={
                  validateNameMedioPago ? "form-control is-invalid" : "form-control"
                }
                placeholder="Nombre"
              />
              {validateNameMedioPago && (
                <p className="text-danger my-1">El nombre es obligatorio</p>
              )}

              <label>Notas</label>
              <input
                onChange={handleChangeMediosPago("metadata")}
                value={medio.metadata}
                type="text"
                className={
                  validateMetadataMedioPago ? "form-control is-invalid" : "form-control"
                }
                placeholder="Notas"
              />
              {validateMetadataMedioPago && (
                <p className="text-danger my-1">El detalle es obligatorio</p>
              )}           

              <label className="mb-3">Estado</label>
              <Select
                  className={
                  validateStateMedioPago ? "form-control is-invalid" : "form-control"
                }
                  value={medio.status}
                  onChange={handleChangeMediosPago("status")}
              >                  
                  <option value="">-- Seleccione Estado --</option>                  
                  <option value="ACTIVE">ACTIVO</option>
                  <option value="INACTIVE">INACTIVO</option>
                </Select>
                {validateStateMedioPago && (
                <p className="text-danger my-1">El Estado de plan es obligatorio</p>
              )}

              <label className="mb-3">Acción de Cobranza</label>
              <Select
                  className={
                  validateActionMedioPago ? "form-control is-invalid" : "form-control"
                }
                  value={medio.action}
                  onChange={handleChangeMediosPago("action")}
                   >                  
                  <option value="">-- Seleccione la Acción --</option>
                  <option value="NINGUNA">Ninguna</option>
                  <option value="RECARGO">Recargo</option>
                  <option value="DESCUENTO">Descuento</option>
                </Select>
                {validateActionMedioPago && (
                <p className="text-danger my-1">El Acción de Cobranza es obligatorio</p>
              )}

              <label className="mb-3">¿Cuando?</label>
              <Select
                  className={
                  validateApplyMedioPago ? "form-control is-invalid" : "form-control"
                }
                  value={medio.apply}
                  onChange={handleChangeMediosPago("apply")}
              >                  
                  <option value=""> Seleccione la correcta </option>   
                  <option value="AL_VENCIMIENTO_DE_CUOTA">Al vencimiento de cuota</option>
                  <option value="SIEMPRE">Siempre</option>
                </Select>
                {validateApplyMedioPago && (
                <p className="text-danger my-1">Este campo es obligatorio</p>
              )}


               <label>Porcentaje</label>
                <input
                 
                 value={medio.percentage}
                  onChange={handleChangeMediosPago("percentage")}
                  type="number"
                      className={
                  validatePercentageMedioPago ? "form-control is-invalid" : "form-control"
                }
                  placeholder="Porcentaje"
                />
               {validatePercentageMedioPago && (
                <p className="text-danger my-1">La porcentaje es obligatorio</p>
              )}
              
            </form>
          </div>

          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmitMediosPago}>Crear</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default NuevoMedioPago;