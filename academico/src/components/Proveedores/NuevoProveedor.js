import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { create } from './apiProveedores'
import moment from 'moment'

import {
    AgregarShadow,
    AgregarFormulario,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    Select,
    Linea,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    AgregarFormularioAlumno,
  } from "../styled-component/Navbar";
  
const NuevoProveedor = ({changeToggle}) => { 
   
    const [validateCiudad, setValidateCiudad] = useState(false);
    const [validateCodPostal, setValidateCodPostal] = useState(false);
    const [validateDocumentType, setValidateDocumentType] = useState(false);
    const [validateDomicilio, setValidateDomicilio] = useState(false);
    const [validateEmail, setValidateEmail] = useState(false);  
    const [validateNroComInd, setValidateNroComInd] = useState(false);
    const [validateNroIIBB, setValidateNroIIBB] = useState(false);
    const [validateProvincia, setValidateProvincia] = useState(false);
    const [validateRazonSocial, setValidateRazonSocial] = useState(false);
    const [validateResponsable, setValidateResponsable] = useState(false);
    const [validateTelefono, setValidateTelefono] = useState(false);
    const [validateDocument, setValidateDocument] = useState(false);
    const [validateWeb, setValidateWeb] = useState(false);     
    const [proveedor, setProveedores] = useState({               
        ciudad: "",        
        codPostal: "",
        documentType:"",
        document: "",        
        domicilio: "",
        email: "",
        nroComInd:"",        
        nroIIBB:"",
        provincia:"",
        razonSocial:"",
        responsable:"",
        telefono: "",
        web:""    
    });

    console.log(proveedor)

    const handleChangeProveedor = (name) => (event) => {
        setProveedores({ ...proveedor, [name]: event.target.value });    
                
        if (name === "ciudad") {
            setValidateCiudad(false);
        }        
        if (name === "codPostal") {
            setValidateCodPostal(false);
        }
        if (name === "document") {
            setValidateDocumentType(false);
        }
        if (name === "document") {
          setValidateDocument(false);
        }
        if (name === "domicilio") {
            setValidateDomicilio(false);
        }
        if (name === "email") {
            setValidateEmail(false);
        }
        if (name === "nroComInd") {
            setValidateNroComInd(false);
        }
        if (name === "nroIIBB") {
            setValidateNroIIBB(false);
        }
        if (name === "provincia") {
            setValidateProvincia(false);
        }
        if (name === "razonSocial") {
            setValidateRazonSocial(false);
        }
        if (name === "responsable") {
            setValidateResponsable(false);
        }
        if (name === "telefono") {
          setValidateTelefono(false);              
        }            
      };

    const clickSubmitProveedor = (event) => {
        event.preventDefault(event);    
        
        if (!proveedor.ciudad) {
            setValidateCiudad(true);
        }
        if (!proveedor.codPostal) {
            setValidateCodPostal(true);
        }
        if (!proveedor.documentType) {
            setValidateDocumentType(true);
        }
        if (!proveedor.document) {
          setValidateDocument(true);
        }
        if (!proveedor.domicilio) {
            setValidateDomicilio(true);
        }
        if (!proveedor.email) {
            setValidateEmail(true);
        }
        if (!proveedor.nroComInd) {
            setValidateNroComInd(true);
        }
        if (!proveedor.nroIIBB) {
            setValidateNroIIBB(true);
        }
        if (!proveedor.provincia) {
            setValidateProvincia(true);
        }
        if (!proveedor.razonSocial) {
            setValidateRazonSocial(true);
        }
        if (!proveedor.responsable) {
            setValidateResponsable(true);
        }
        if (!proveedor.telefono) {
            setValidateTelefono(true);
        }        
        if (
          proveedor.ciudad &&
          proveedor.codPostal &&
          proveedor.documentType &&
          proveedor.document &&
          proveedor.domicilio &&
          proveedor.email &&
          proveedor.nroComInd &&
          proveedor.nroIIBB &&
          proveedor.provincia &&
          proveedor.razonSocial &&
          proveedor.responsable &&
          proveedor.telefono                  
                    
        ) {
          console.log("proveedor", proveedor)
        
    const val =  {
           
        "ciudad": proveedor.ciudad,
        "codPostal":proveedor.codPostal,
        "documentType": proveedor.documentType,
        "document": proveedor.document,
        "domicilio":proveedor.domicilio,
        "email": proveedor.email,
        "nroComInd": proveedor.nroComInd,
        "nroIIBB": proveedor.nroIIBB,
        "provincia": proveedor.provincia,
        "razonSocial": proveedor.razonSocial,
        "responsable": proveedor.responsable,
        "telefono":proveedor.telefono,
        "web": proveedor.web     
          }     
          console.log(val, "este es el valor")    

          create(val)
          .then(data => {
  
            if(data){
              console.log(data)
              if(data.ok === true){    
                toast.success("Proveedor creado");
                changeToggle();
              }
              if(data.ok === false){                
                toast.error("Error al crear");
                changeToggle();
              }    
            }else{
              toast.error("Error en la api");
              changeToggle();            
            }           
          })
        }
      };    
    
    return ( 
        <>
                <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Crear un nuevo proveedor</TextFormulario>
            </div>
            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="formulario__body ">
            <form className="input-form">
              <label>Razón Social</label>
              <input
                onChange={handleChangeProveedor("razonSocial")}
                value={proveedor.razonSocial}
                type="text"
                className={
                  validateRazonSocial ? "form-control is-invalid" : "form-control"
                }
                placeholder="Razón Social"
              />
              {validateRazonSocial && (
                <p className="text-danger my-1">La razón social es obligatorio</p>
              )}          
             <label>Tipo de Documento</label>   
             <div className= "form-group row">         
             <div className="col-sm-4">
              <Select
                  className={
                  validateDocumentType ? "form-control is-invalid" : "form-control"
                }
                  value={proveedor.documentType}
                  onChange={handleChangeProveedor("documentType")}
              >                 
                  <option value="">-- Seleccione--</option>                  
                  <option value="CUIT">CUIT</option>
                  <option value="CUIL">CUIL</option>
                  <option value="DNI">DNI</option>                 
                </Select>
                {validateDocumentType && (
                <p className="text-danger my-1">El tipo de documento es obligatorio</p>
              )}</div>
              <div className="col-sm-8">
              <input
                onChange={handleChangeProveedor("document")}
                value={proveedor.document}
                type="number"
                className={
                  validateDocument ? "form-control is-invalid" : "form-control"
                }
                placeholder="Número de documento"
              />
              {validateDocument && (
                <p className="text-danger my-1">El Número de documento es obligatorio</p>
              )}
            </div>
            </div>
            
              <label>Responsable</label>
              <Select 
                  className={
                  validateResponsable ? "form-control is-invalid" : "form-control"
                }
                  value={proveedor.responsable}
                  onChange={handleChangeProveedor("responsable")}
              >                 
                  <option value="">-- Seleccione responsable --</option>                  
                  <option value="1">Responsable Inscripto</option>
                  <option value="4">Exento</option>
                  <option value="5">Consumidor Final</option>
                  <option value="6">Responsable Monotributo</option>
                </Select>
                {validateResponsable && (
                <p className="text-danger my-1">El tipo responsable es obligatorio</p>
              )}
              <label>Número de Ingresos Brutos</label>
              <input
                onChange={handleChangeProveedor("nroIIBB")}
                value={proveedor.nroIIBB}
                type="number"
                className={
                  validateNroIIBB ? "form-control is-invalid" : "form-control"
                }
                placeholder="Número de ingresos brutos"
              />
              {validateNroIIBB && (
                <p className="text-danger my-1">El número de ingresos brutos es obligatorio</p>
              )}
              <label>Número de Comercio e Industria</label>
              <input
                onChange={handleChangeProveedor("nroComInd")}
                value={proveedor.nroComInd}
                type="number"
                className={
                  validateNroComInd ? "form-control is-invalid" : "form-control"
                }
                placeholder="Número de comercio e industria"
              />
              {validateNroComInd && (
                <p className="text-danger my-1">El número de comercio e industria es obligatorio</p>
              )}
              <label>Domicilio</label>
              <input
                onChange={handleChangeProveedor("domicilio")}
                value={proveedor.domicilio}
                type="text"
                className={
                  validateDomicilio ? "form-control is-invalid" : "form-control"
                }
                placeholder="Dirección y Número"
              />
              {validateDomicilio && (
                <p className="text-danger my-1">El domicilio es obligatorio</p>
              )}
              <label>Código Postal</label>
              <input
                onChange={handleChangeProveedor("codPostal")}
                value={proveedor.codPostal}
                type="text"
                className={
                  validateCodPostal ? "form-control is-invalid" : "form-control"
                }
                placeholder="Código Postal"
              />
              {validateCodPostal && (
                <p className="text-danger my-1">El código postal es obligatorio</p>
              )}              
            <label>Ciudad</label>
              <input
                onChange={handleChangeProveedor("ciudad")}
                value={proveedor.ciudad}
                type="text"
                className={
                  validateCiudad ? "form-control is-invalid" : "form-control"
                }
                placeholder="Ciudad"
              />
              {validateCiudad && (
                <p className="text-danger my-1">El CUIT es obligatorio</p>
              )}
            <label>Provincia</label>
              <input
                onChange={handleChangeProveedor("provincia")}
                value={proveedor.provincia}
                type="text"
                className={
                  validateProvincia ? "form-control is-invalid" : "form-control"
                }
                placeholder="Provincia"
              />
              {validateProvincia && (
                <p className="text-danger my-1">La ciudad es obligatorio</p>
              )}
              <label>Teléfono</label>
              <input
                onChange={handleChangeProveedor("telefono")}
                value={proveedor.telefono}
                type="tel"
                className={
                  validateTelefono ? "form-control is-invalid" : "form-control"
                }
                placeholder="Teléfono"
              />
              {validateTelefono && (
                <p className="text-danger my-1">El Teléfono es obligatorio</p>
              )}
              <label>Email</label>
              <input
                onChange={handleChangeProveedor("email")}
                value={proveedor.email}
                type="text"
                className={
                  validateTelefono ? "form-control is-invalid" : "form-control"
                }
                placeholder="Email"
              />
              {validateTelefono && (
                <p className="text-danger my-1">El email es obligatorio</p>
              )}              
              <label>Web</label>
              <input
                onChange={handleChangeProveedor("web")}
                value={proveedor.web}
                type="text"
                className={ "form-control"
                }
                placeholder="Web"
              />
                    
              </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmitProveedor}>Crear</ButtonCrear>
            
          </div>
        </AgregarFormularioAlumno>
        </>
     );
     
}
 
export default NuevoProveedor;