import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import Moment from "react-moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
import { URL } from "../../valores";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { useDatos } from "../hooks/useDatos";
import { getProveedorId } from './apiProveedores'
import styled from "styled-components";

const ButtonAgregar = styled.button`
  width: 143px;
  height: 42px;
  border-radius: 6px;
  background-color:  #009DDC;
  font-size: 11px;
  line-height: 15px;
  color: #ffffff;
  font-weight: 900;
  font-family: "Lato";
  cursor: pointer;
  align-self: center;
`;

const Proveedor = (props) => {
const [proveedor, setProveedor] = useState();

  useEffect(() => {
    getProveedorId(props.location.state.fromDashboard).then(data => {     
     setProveedor(data)
    })
  }, []);

  return (
    <>
    <Navbar/>
    <div className="dashboard">
      {proveedor ? (
        <Tabs>
          <TabList>
            <Tab>Información</Tab>           
          </TabList>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Información</div>
            <div className="datos-usuario d-flex">
              <div className="datos">
                <div className="d-flex">
                  <h5 className="info-descripcion">Razón Social:</h5>
                  <p>{proveedor.razonSocial}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Tipo de Documento:</h5>
                  <p>{proveedor.documentType}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Número de Documento:</h5>
                  <p>{proveedor.document}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Responsable:</h5>
                  <p>{proveedor.responsable === "1" && "Responsable Inscripto"}</p>
                  <p>{proveedor.responsable === "4" && "Responsable Exento"}</p>
                  <p>{proveedor.responsable === "5" && "Consumidor Final"}</p>
                  <p>{proveedor.responsable === "6" && "Responsable Monotributo"}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Número de Ingreso Brutos:</h5>
                  <p>{proveedor.nroIIBB}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Número de Comercio e Industria:</h5>
                  <p>{proveedor.nroComInd}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Domicilio:</h5>
                  <p>{proveedor.domicilio}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Código Postal:</h5>
                  <p>{proveedor.codPostal}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Ciudad:</h5>
                  <p>{proveedor.ciudad}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Provincia:</h5>
                  <p>{proveedor.provincia}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Teléfono:</h5>
                  <p>{proveedor.telefono}</p>
                </div>       
                <div className="d-flex">
                  <h5 className="info-descripcion">Email:</h5>
                  <p>{proveedor.email}</p>                 
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Web:</h5>
                  <p>{proveedor.web === "string" && " no hay web" || proveedor.web }</p>                 
                </div>

              </div>              
            </div>
          </TabPanel>          
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>
      )}
      </div>
    </>
  );
};

export default withRouter(Proveedor);