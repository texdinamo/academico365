import {useState,useEffect} from 'react'
import { updateProveedor } from './apiProveedores'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import moment from 'moment'
import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
    FormularioRow,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'

const Select = styled.select`
width: 100%;
color: #BBC5D5;
border: 1px solid #BBC5D5;
padding: 10px;
border-radius: 4px;
font-size: 13px;
`;

const Select1 = styled.select`
width: 100%;
color: #BBC5D5;
border: 1px solid #BBC5D5;
padding: 10px;
border-radius: 4px;
font-size: 13px;
position: relative;
height:44.8px;
top: 10px;
bottom: -10px
`;

const divStyle = {
  position: "relative",    
  top: "5px",
  bottom: "5px"
}


const ModificarProveedor = ({changeToggle,proveedor,proveedorId}) => {
    const [supplier, setSupplier] = useState({
       
        ciudad: "",        
        codPostal: "",
        documentType: "",
        document: "",        
        domicilio: "",
        email: "",
        nroComInd:"",        
        nroIIBB:"",
        provincia:"",
        razonSocial:"",
        responsable:"",
        telefono: "",
        web:""       
      });

  useEffect(()=>{

    if(proveedor){    
        
      setSupplier({
        ciudad: proveedor.ciudad,        
        codPostal: proveedor.codPostal,
        document: proveedor.documentType,  
        document: proveedor.document,        
        domicilio: proveedor.domicilio,
        email: proveedor.email,
        nroComInd: proveedor.nroComInd,        
        nroIIBB: proveedor.nroIIBB,
        provincia: proveedor.provincia,
        razonSocial: proveedor.razonSocial,
        responsable: proveedor.responsable,
        telefono: proveedor.telefono,
        web:proveedor.web,
        id:proveedor.id    
      });    
    }

  },[proveedor])

  const clickSubmit = () => {

      const val =  {
        "id":supplier.id,        
       "ciudad": supplier.ciudad,
       "codPostal":supplier.codPostal,
       "documentType": supplier.documentType,
       "document": supplier.document,
       "domicilio":supplier.domicilio,
       "email": supplier.email,
       "nroComInd": supplier.nroComInd,
       "nroIIBB": supplier.nroIIBB,
       "provincia": supplier.provincia,
       "razonSocial": supplier.razonSocial,
       "responsable": supplier.responsable,
       "telefono":supplier.telefono,       
       "web": supplier.web     
        }
        console.log(val, "este es el valor modificado");
        
      updateProveedor(val)
      .then(data => {
        console.log(data)

        if(data){
        if(data.ok === true){
          toast.success("Proveedor modificado");
          changeToggle();
        }
        if(data.ok === false){
          toast.error("Error al modificar");
          changeToggle();
        }
      }
      else{
        toast.error("Problema con la api");
        changeToggle();
      }
      })
    }
    const handleChange = name => e => {
      setSupplier({...supplier, [name]: e.target.value})
  }


    return ( 
        <>
           <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Modificar un Punto de Venta</TextFormulario>
            </div>
            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="formulario__body ">
            <form className="input-form">
              <label>Razón Social</label>
              <input
                onChange={handleChange("razonSocial")}
                defaultValue={supplier.razonSocial}
                type="text"
                className="input"
                placeholder="Razón Social"
              />              
              <label>Tipo de Documento</label>
              <div className="form-group row" style={divStyle}>
              <div className="col-sm-4">
              <Select1
                  className="select"                  
                  onChange={handleChange("documentType")}
                  defaultValue={supplier.documentType}  >                   
                  <option value="">-- Seleccione--</option>                  
                  <option value="CUIT">CUIT</option>
                  <option value="CUIL">CUIL</option>
                  <option value="DNI">DNI</option>                  
                </Select1>
               </div>
                <div className="col-sm-8">
              <input
                onChange={handleChange("document")}
                defaultValue={supplier.document}
                type="number"
                className="input"
                placeholder="Número de documento"                                
              />
             </div>
             </div>
              <label>Responsable</label>
              <Select
                  className="select"                  
                  onChange={handleChange("responsable")}
                  defaultValue={supplier.responsable}  >                   
                  <option value="">-- Seleccione responsable --</option>                  
                  <option value="1">Responsable Inscripto</option>
                  <option value="4">Exento</option>
                  <option value="5">Consumidor Final</option>
                  <option value="6">Responsable Monotributo</option>
                </Select>               
              <label>Número de Ingreso Bruto</label>
              <input
                onChange={handleChange("nroIIBB")}
                defaultValue={supplier.nroIIBB}
                type="text"
                className="input"
                placeholder="Número de Ingreso Bruto"
              />
              <label>Número de Comercio e Industria</label>
              <input
                onChange={handleChange("nroComInd")}
                defaultValue={supplier.nroComInd}
                type="text"
                className="input"
                placeholder="Número de Comercio e Industria"
              />
               <label>Domicilio</label>
              <input
                onChange={handleChange("domicilio")}
                defaultValue={supplier.domicilio}
                type="text"
                className="input"
                placeholder="Domicilio"
              />
               <label>Código Postal</label>
              <input
                onChange={handleChange("codPostal")}
                defaultValue={supplier.codPostal}
                type="text"
                className="input"
                placeholder="Código Postal"
              />
               <label>Ciudad</label>
              <input
                onChange={handleChange("ciudad")}
                defaultValue={supplier.ciudad}
                type="text"
                className="input"
                placeholder="Ciudad"
              />              
              <label>Provincia</label>
              <input
                onChange={handleChange("provincia")}
                defaultValue={supplier.provincia}
                type="text"
                className="input"
                placeholder="Provincia"
              />
              <label>Teléfono</label>
              <input
                onChange={handleChange("telefono")}
                defaultValue={supplier.telefono}
                type="number"
                className="input"
                placeholder="Teléfono"
              />
              <label>Email</label>
              <input
                onChange={handleChange("email")}
                defaultValue={supplier.email}
                type="text"
                className="input"
                placeholder="Email"
              />
              <label>Web</label>
              <input
                onChange={handleChange("web")}
                defaultValue={supplier.web === "string" && " no hay web" || supplier.web }
                type="text"
                className="input"
                placeholder="Web"
              />      
              </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmit}>Guardar</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>    
        </>
     );
}
 
export default ModificarProveedor;