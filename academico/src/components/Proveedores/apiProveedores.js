export const create = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/supplier/create`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const updateProveedor = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/supplier/update`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const deleteProveedor = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/supplier/remove`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}

export const getProveedores = () => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/supplier`, {
        method: "GET"
    })
        .then(response => {            
            return response.json()
        })
        .catch(err => console.log(err))
}

export const getProveedorId = (id) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/supplier/${id}`, {
        method: "GET"
    })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .catch(err => console.log(err))
}