import React,{useState,useEffect} from 'react';
import Navbar from '../estructura/Navbar'
import {Link} from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import axios from 'axios'
import {URL} from '../../valores'
import {Spinner} from 'react-bootstrap'
import 'react-tabs/style/react-tabs.css';
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import {AgregarFormularioAlumno,ButtonAgregar,ButtonCrear,ButtonCancelar,
  TextFormulario,TextInfo,ContainerMenu,SubTextInfo,Linea} from '../styled-component/Navbar'
import styled from 'styled-components'
import {ToastContainer, toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.min.css'
import ModificarProveedor from './ModificarProveedor'
import { getProveedores,getProveedorId ,deleteProveedor } from './apiProveedores'

const Select = styled.select`

width: 100%;
color: #BBC5D5;
border: 1px solid #BBC5D5;
padding: 10px;
border-radius: 4px;
`
export const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`;

const Proveedores = (props) => {
  const { SearchBar } = Search;
  const [proveedorId,setProveedorId] = useState();
  const [confirmar, setConfirmar] = useState(false);
  const [eliminar, setEliminar] = useState(false);
  const [values,setValues] = useState();
  const [toggle , setToggle] = useState(false);
  const [proveedores, setProveedores] = useState([]);
  const [proveedor , setProveedor] = useState();
  const [confirmarError, setConfirmarError] = useState(false);
  const changeToggle = () => setToggle(prevLogin => !prevLogin);
  const changeEliminar = () => setEliminar(prevLogin => !prevLogin);
  const changeConfirmar = () => setConfirmar(prevLogin => !prevLogin); 
  const cambio = async () => {
  setEliminar(false);
  console.log('punto de venta',proveedorId)
  const  val = {
    "id": proveedorId
}

  deleteProveedor(val).then(data => { 
      if(data){      
      if(data.ok === true){
        console.log('entro')
        setConfirmar(true);      
      }
      if(data.ok === false){       
        console.log('no entro')
        setConfirmarError(true)
      }
    }else{
      setConfirmarError(true)
    }
    })    
}

useEffect(() => {
  getProveedores().then(data => {
  setProveedores(data)
  })
  if(proveedorId){
  getProveedorId(proveedorId).then(data => {      
      setProveedor(data)
})
  }

}, [proveedorId]);  
    proveedores.map(stat => { 
    if(stat.responsable === "1"){
     return stat.responsable = "Responsable Inscripto"
   }else if (stat.responsable === "4"){
     return stat.responsable = "Exento"
   } else if (stat.responsable === "5"){
    return stat.responsable = "Consumidor Final"
  }else if (stat.responsable === "6"){
    return stat.responsable = "Responsable Monotributo"}  
  });

  const columns = [
    {dataField: "id", text: 'Id',sort: true,  searchable: true,
  
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  },
    {dataField: "razonSocial", text: 'Razón social',sort: true,  searchable: true,
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  
  },
    {dataField: "documentType", text: 'Tipo de Documento',sort: true,  searchable: true,
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  },
  {dataField: "document", text: 'Número de Documento',sort: true,  searchable: true,
  sortCaret: (order, column) => {
    if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
    else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
    else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
    return null;
  }
},

    {dataField: "ciudad", text: 'Ciudad',sort: true,  searchable: true,
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
    }
  },
    {dataField: "responsable", text: 'Responsable',sort: true,  searchable: true,
    sortCaret: (order, column) => {
      if (!order) return (<span className="inicio-arrow" > <i class="fas fa-caret-down"></i>  <i class="fas fa-caret-up"></i></span>);
      else if (order === 'asc') return (<span> <i class="fas fa-caret-down"></i>  <font color="red"><i class="fas fa-caret-up color"></i></font></span>);
      else if (order === 'desc') return (<span> <font color="red"> <i class="fas fa-caret-down color"></i></font>  <i class="fas fa-caret-up"></i></span>);
      return null;
      }
},
    
  {
    dataField: "link",
    text: "Accion",
    formatter: (rowContent, row) => {
      return (
        <>
          <i
            className="fas fa-pencil-alt mr-3 text-success"
            onClick={changeToggle}
          ></i>

          <Link
            to={{
              pathname: "/administracion/proveedores/proveedor",
              state: { fromDashboard: row.id },
            }}
            className="link-i"
          >
            <i className="far fa-eye"></i>
          </Link>

          <i
            className="far fa-trash-alt ml-3 text-danger"
            onClick={changeEliminar}

          ></i>
        </>
      );
    },
  }
  ]

  const rowEvents = {
    onClick: (e, row, rowIndex) => {
     setProveedorId(row.id)
      console.log(row.id)
    },
    onMouseEnter: (e, row, rowIndex) => {     
      setProveedorId(row.id)
    }
  
  };

  const options = {
    paginationSize: 3,
    pageStartIndex: 0,
    // alwaysShowAllBtns: true, // Always show next and previous button
    // withFirstAndLast: false, // Hide the going to First and Last page button
    // hideSizePerPage: true, // Hide the sizePerPage dropdown always
    // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
    withFirstAndLast: false,
    firstPageText: 'First',
    prePageText: 'Anterior',
    nextPageText: 'Siguiente',  
    showTotal: false,
    disablePageTitle: true
  }    

    return ( 
      <>
        <Navbar/>
        <div className="dashboard">
          
      <ToastContainer/>
      <ToolkitProvider keyField="id" data={proveedores} columns={columns} search>
        {(props) => (
          <div>
            <div className="barra">
              <i className="fas fa-search ml-3 mt-2"></i>
              <SearchBar placeholder="Buscar proveedor" {...props.searchProps} />
            </div>
            <BootstrapTable
              rowEvents={rowEvents}
              {...props.baseProps}
              pagination={paginationFactory(options)}
            />
          </div>
        )}
      </ToolkitProvider>  

      {toggle && <AgregarShadow onClick={changeToggle} /> } 

      {eliminar && (
        <SweetAlert
          primary
          showCancel
          cancelBtnBsStyle="secondary"
          style={{
            fontSize: "12px",
            fontWeight: "700",
            width: "500px",
            height: "240px",
          }}
          onConfirm={() => cambio()}
          onCancel={() => setEliminar(false)}
          confirmBtnText="Aceptar"
          title="Estas seguro de realizar esta accion?"
          focusCancelBtn
        >
          <p style={{ fontSize: "14px", margin: "5px" }}>
            Esta apunto de realizar una accion irreversible
          </p>

          <p style={{ fontSize: "14px", marginBottom: "20px" }}>
            Confirma tu accion
          </p>
        </SweetAlert>
      )}

      {confirmar && (
        <SweetAlert
          success
          title="Eliminado"
          onConfirm={() => changeConfirmar()}
        ></SweetAlert>
      )}

{confirmarError && (
        <SweetAlert
          danger
          title="Error"
          onConfirm={() => setConfirmarError(false)}
        ></SweetAlert>
      )}
      {toggle  &&
      (

<ModificarProveedor changeToggle={changeToggle} proveedorId={proveedorId}  proveedor={proveedor} />
)      
      }             
        </div>
        </>
     );
}

export default Proveedores;
