import {useEffect} from 'react';
//import Plot from 'react-plotly.js';
import { Title, Box, BoxBegin, BoxEnd, Text, Number, NumberDanger, Panel, PanelHead, Imagen } from '../styled-component/Escritorio'
import incremento from '../../img/increase.svg'
import noincremento from '../../img/decrease.svg'
import Navbar from '../estructura/Navbar'

const Escritorio = (props) => {

    useEffect(()=>{
    },[])

    const config = { displayModeBar: false }
    return (
        <>
            <Navbar></Navbar>
            <div className="dashboard">
                <Title>Estadisticas</Title>
                <div className="d-flex">
                    <BoxBegin>
                        <Text >Alumnos totales</Text>
                        <Number>28,345</Number>
                    </BoxBegin>
                    <Box>
                        <Text>Matriculas pendientes</Text>
                        <NumberDanger>120</NumberDanger>
                    </Box>
                    <Box>
                        <Text>Cursos aprobados</Text>
                        <Number>89 <Imagen src={incremento} /></Number>
                    </Box>
                    <Box>
                        <Text>Nuevas materias</Text>
                        <Number>46% <Imagen src={noincremento} /></Number>
                    </Box>
                    <BoxEnd>
                        <Text>Nuevas materias</Text>
                        <Number>46% <Imagen src={noincremento} /></Number>
                    </BoxEnd>
                </div>
                <Panel>
                    <PanelHead>
                        Estadisticas
            </PanelHead>
                </Panel>
            </div>
        </>
    );
}

export default Escritorio;