import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import Moment from "react-moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
import { URL } from "../../valores";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { useDatos } from "../hooks/useDatos";
import { getCuentaContableId } from './apiCuentasContables'
import styled from "styled-components";

const CuentaContable = (props) => {
const [cuenta, setCuenta] = useState();

  useEffect(() => {  
    getCuentaContableId(props.location.state.fromDashboard).then(data => {
      console.log("esto es la data" + data)
     setCuenta(data)
    })
  }, []);

  console.log("medios", cuenta);

  return (
    <>
    <Navbar/>
    <div className="dashboard">
      {cuenta ? (
        <Tabs>
          <TabList>
            <Tab>Información</Tab>            
          </TabList>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Información</div>
            <div className="datos-usuario d-flex">
              <div className="datos">
                <div className="d-flex">
                  <h5 className="info-descripcion">Nombre de Cuenta Contable:</h5>
                  <p>{cuenta.accountName}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Número de Cuenta Contable:</h5>
                  <p>{cuenta.accountCode}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Nota:</h5>
                  <p>{cuenta.note}</p>
                </div>             
              </div>                
            </div>              
          </TabPanel>         
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>
      )}
      </div>
    </>
  );
};

export default withRouter(CuentaContable);