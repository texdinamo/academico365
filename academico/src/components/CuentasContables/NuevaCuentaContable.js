import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { create } from './apiCuentasContables'
import moment from 'moment'

import {
    AgregarShadow,
    AgregarFormulario,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    Select,
    Linea,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    AgregarFormularioAlumno,
  } from "../styled-component/Navbar";

const NuevoCuentaContable = ({changeToggle}) => {   

    const [validateAccountName, setValidateAccountName] = useState(false);
    const [validateAccountCode, setValidateAccountCode] = useState(false);
    const [validateNote, setValidateNote] = useState(false);    
    const [cuenta, setCuenta] = useState({
        accountName: "",
        accountCode: "",
        note: ""              
      });

      console.log(cuenta)

      const handleChangeCuentaContable = (name) => (event) => {
        setCuenta({ ...cuenta, [name]: event.target.value });    
        if (name === "accountName") {
          setValidateAccountName(false);
        }    
        if (name === "accountCode") {
          setValidateAccountCode(false);
        }         
      };

      const clickSubmitCuentaContable = (event) => {
        event.preventDefault(event);
        if (!cuenta.accountName) {
          setValidateAccountName(true);
        }
        if (!cuenta.accountCode) {
            setValidateAccountCode(true);
          }               
        if (
          cuenta.accountName &&
          cuenta.accountCode &&
          cuenta.note         
        ) {

          console.log('medioPago...',cuenta)

        const val =  {           
            "institutionId": 1,
            "accountName": cuenta.accountName,
            "accountCode": cuenta.accountCode,            
            "note": cuenta.note            
          }  
          create(val)
          .then(data => {
  
            if(data){
              console.log(data)
              if(data.ok === true){
    
                toast.success("Cuenta Contable creada");
                changeToggle();
              }
              if(data.ok === false){
                
                toast.error("Error al crear");
                changeToggle();
              }    
            }else{
              toast.error("Error en la api");
              changeToggle();
            
            }           
          })
        }
      };
        
    return ( 
        <>
                <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Crear una nueva cuenta contable</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>

          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre</label>
              <input
                onChange={handleChangeCuentaContable("accountName")}
                value={cuenta.accountName}
                type="text"
                className={
                  validateAccountName ? "form-control is-invalid" : "form-control"
                }
                placeholder="Nombre"
              />
              {validateAccountName && (
                <p className="text-danger my-1">El nombre de cuenta es obligatorio</p>
              )}

              <label>Número de Cuenta Contable</label>
              <input
                onChange={handleChangeCuentaContable("accountCode")}
                value={cuenta.accountCode}
                type="number"
                className={
                  validateAccountCode ? "form-control is-invalid" : "form-control"
                }
                placeholder="Número de cuenta contable"
              />
              {validateAccountCode && (
                <p className="text-danger my-1">El Número de cuenta es obligatorio</p>
              )}
               <label>Nota</label>
                <input                 
                 value={cuenta.note}
                  onChange={handleChangeCuentaContable("note")}
                  type="text"
                  className= {  "form-control"}                    
                  placeholder="Nota"
                />             
            </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmitCuentaContable}>Crear</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default NuevoCuentaContable;