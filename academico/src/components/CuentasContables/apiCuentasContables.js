export const create = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/contableaccount/create`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const updateCuentaContable = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/contableaccount/update`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const deleteCuentaContable = (post) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/contableaccount/remove`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}

export const getCuentasContables = () => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/contableaccounts`, {
        method: "GET"
    })
        .then(response => {            
            return response.json()
        })
        .catch(err => console.log(err))
}

export const getCuentaContableId = (id) => {
    return fetch(`${process.env.REACT_APP_ACCOUNTING_URL}/contableaccount/${id}`, {
        method: "GET"
    })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .catch(err => console.log(err))
}