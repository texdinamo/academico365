import {useState,useEffect} from 'react'
import { updateCuentaContable } from './apiCuentasContables'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'
import CuentaContable from './CuentaContable';

const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarCuentaContable = ({changeToggle,cuentaContable,cuentaContableId}) => {
    const [cuenta, setCuenta] = useState({
        accountName: "",
        note: "",
        accountCode: "",
        institutionId: ""
        
      });

  useEffect(()=>{

    if(cuentaContable){

      console.log('Soy la cuenta', cuenta)         
      
      setCuenta({
        accountCode: cuentaContable.accountCode,
        note: cuentaContable.note,        
        id: cuentaContable.id,        
        accountName: cuentaContable.accountName,
        institutionId: cuentaContable.institutionId
      });    
    }

  },[cuentaContable])
console.log('ss',cuenta)

    const clickSubmit = () => {
      const val =  {        
        "id" : cuenta.id,
        "accountName": cuenta.accountName,
        "note": cuenta.note,
        "accountCode": cuenta.accountCode,        
        "institutionId": cuenta.institutionId
      }

      updateCuentaContable(val)
      .then(data => {
        console.log(data)
        if(data){
        if(data.ok === true){
          toast.success("Medio de Pago modificada");
          changeToggle();
        }
        if(data.ok === false){
          toast.error("Error al modificar");
          changeToggle();
        }
      }
      else{
        toast.error("Problema con la api");
        changeToggle();
        }
      })

    }

    const handleChange = name => e => {
      setCuenta({...cuenta, [name]: e.target.value})    
    }

    return ( 
        <>
           <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Modificar una Cuenta Contable</TextFormulario>
            </div>
            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre</label>
              <input
                onChange={handleChange("accountName")}
                defaultValue={cuenta.accountName}
                type="text"
                className="input"
                placeholder="Nombre"
              />
               <label>Número de Cuenta Contable</label>
              <input
                onChange={handleChange("accountCode")}
                defaultValue={cuenta.accountCode}
                type="number"
                className="input"
                placeholder="Número de Cuenta Contable"
              />              
              <label>Nota</label>
              <input
                onChange={handleChange("note")}
                defaultValue={cuenta.note}
                type="text"
                className="input"
                placeholder="Notas"
              />              
            </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmit}>Guardar</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>    
        </>
     );
}
 
export default ModificarCuentaContable;