export const create = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/content`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const updateMateria = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/content/update`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            return response
        })
        .catch(err => console.log(err))
}

export const deleteMaterias = (post) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/content/remove`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
            //Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(post)
    })
        .then(response => {
            console.log(response)
            return response
        })
        .catch(err => console.log(err))
}


export const getMaterias = () => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/content`, {
        method: "GET"
    })
        .then(response => {
            return response.json()
        })
        .catch(err => console.log(err))
}

export const getMateriaId = (id) => {
    return fetch(`${process.env.REACT_APP_INSTITUTION_URL}/1/content/${id}`, {
        method: "GET"
    })
        .then(response => {
            return response.json()
        })
        .catch(err => console.log(err))
}