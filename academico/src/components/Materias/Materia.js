import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import Moment from "react-moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
import { URL } from "../../valores";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { useDatos } from "../hooks/useDatos";
import { getMateriaId } from './apiMaterias'
import styled from "styled-components";

const ButtonAgregar = styled.button`
  width: 143px;
  height: 42px;
  border-radius: 6px;
  background-color:  #009DDC;
  font-size: 11px;
  line-height: 15px;
  color: #ffffff;
  font-weight: 900;
  font-family: "Lato";
  cursor: pointer;
  align-self: center;
`;

const Materia = (props) => {
  const [materia, setMateria] = useState();

  useEffect(() => {


    getMateriaId(props.location.state.fromDashboard).then(data => {
     
     setMateria(data)

    })

  }, []);

  console.log("allumno", materia);

  return (
    <>
    <Navbar/>
    <div className="dashboard">
      {materia ? (
        <Tabs>
          <TabList>
            <Tab>Informacion</Tab>

            <Tab>Docentes</Tab>
          </TabList>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Informacion</div>

            <div className="datos-usuario d-flex">
              <div className="datos">
                <div className="d-flex">
                  <h5 className="info-descripcion">Nombre:</h5>
                  <p>{materia.name}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Notas:</h5>
                  <p>{materia.metadata}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Fecha de baja:</h5>

                  <Moment format="YYYY/MM/DD">
                    <p>{materia.fecha_baja}</p>
                  </Moment>
                </div>
              </div>

              <div></div>
            </div>
          </TabPanel>

          <TabPanel className="alumno">
            <div className="header-descripcion d-flex justify-content-between">
              <div>
                <p>Docentes</p>
              </div>
              <div>
                <ButtonAgregar>Añadir Docente</ButtonAgregar>
              </div>
            </div>
          </TabPanel>
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>
      )}
      </div>
    </>
  );
};

export default withRouter(Materia);
