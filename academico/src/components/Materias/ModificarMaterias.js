import {useState,useEffect} from 'react'
import { updateMateria } from './apiMaterias'
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import moment from 'moment'
import {
    AgregarFormularioAlumno,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    Linea,
  } from "../styled-component/Navbar";
  import styled from 'styled-components'



const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarMaterias = ({changeToggle,alumno,alumnoId}) => {
    const [materia, setMateria] = useState({
        name: "",
        detalle: "",
        fechaDeBaja: "",
        id: "",
        idInstitution: ""
      });



  useEffect(()=>{

    if(alumno){
      

      console.log('soy alumno',alumno)
      let fecha = typeof alumno.fecha_baja === 'string' ?  alumno.fecha_baja.split('T') : alumno.fecha_baja
      
      setMateria({
        name: alumno.name,
        detalle: alumno.metadata,
        fechaDeBaja: Array.isArray(fecha) ? fecha[0] : fecha ,
        id: alumno.id,
        idInstitution: alumno.idInstitution
      });


    
    }

  },[alumno])
console.log('ss',materia)

    const clickSubmit = () => {



      const val =  {
        "fecha_baja": materia.fechaDeBaja ,
        "id" : materia.id,
        "idInstitution": materia.idInstitution,
        "metadata": materia.detalle,
        "name": materia.name
      }



      updateMateria(val)
      .then(data => {
        console.log(data)

        if(data){
        if(data.ok === true){

          toast.success("Materia modificada");
          changeToggle();
        }
        if(data.ok === false){

          toast.error("Error al modificar");
          changeToggle();
        }
      }
      else{
        toast.error("Problema con la api");
        changeToggle();
      }

      })

    }

    const handleChange = name => e => {
      setMateria({...materia, [name]: e.target.value})

      if (name === "fechaDeBaja") {


          const nuevaFecha = moment(e.target.value).format()
          
      setMateria({ ...materia, ['fechaDeBaja']: nuevaFecha});
      }


  }


    return ( 
        <>
           <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Modificar una materia</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>

          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre</label>
              <input
                onChange={handleChange("name")}
                defaultValue={materia.name}
                type="text"
                className="input"
                placeholder="Nombre"
              />
              
              <label>Notas</label>
              <input
                onChange={handleChange("detalle")}
                defaultValue={materia.detalle}
                type="text"
                className="input"
                placeholder="Notas"
              />
              

             
            </form>
          </div>

          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmit}>Crear</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>
    
        </>
     );
}
 
export default ModificarMaterias;