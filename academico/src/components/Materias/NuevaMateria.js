import React, { useState, useMemo, useRef, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { create } from './apiMaterias'
import moment from 'moment'

import {
    AgregarShadow,
    AgregarFormulario,
    ButtonAgregar,
    ButtonCrear,
    ButtonCancelar,
    Select,
    Linea,
    TextFormulario,
    TextInfo,
    ContainerMenu,
    SubTextInfo,
    AgregarFormularioAlumno,
  } from "../styled-component/Navbar";


const NuevaMateria = ({changeToggle}) => {


    const [validateDetalle, setValidateDetalle] = useState(false);

    const [validateNameCurso, setValidateNameCurso] = useState(false);
    const [validateFechaDeBaja, setValidateFechaDeBaja] = useState(false);
   

    const [materia, setMateria] = useState({
        name: "",
        detalle: "",
        fechaDeBaja: ""
      });

console.log(materia)

      const handleChangeCurso = (name) => (event) => {
        setMateria({ ...materia, [name]: event.target.value });
    
        if (name === "name") {
          setValidateNameCurso(false);
        }
      
    
        if (name === "detalle") {
          setValidateDetalle(false);
        }
    
        if (name === "fechaDeBaja") {

          setValidateFechaDeBaja(false);

            const nuevaFecha = moment(event.target.value).format()
            
        setMateria({ ...materia, ['fechaDeBaja']: nuevaFecha});
        }
      };

      const clickSubmitCurso = (event) => {
        event.preventDefault();
    
        if (!materia.name) {
          setValidateNameCurso(true);
        }

        if (!materia.detalle) {
            setValidateDetalle(true);
          }
          

        if (!materia.fechaDeBaja) {
          setValidateFechaDeBaja(true);
        }
    
    
        if (
        
          materia.name &&
          materia.detalle 
        ) {

          console.log('materiaaa',materia)

        const val =  {
           
            "idInstitution": 1,
            "metadata": materia.detalle,
            "name": materia.name
          }

          create(val)
          .then(data => {
  
            if(data){
              console.log(data)
              if(data.ok === true){
    
                toast.success("Materia creada");
                changeToggle();
              }
              if(data.ok === false){
    
                toast.error("Error al crear");
                changeToggle();
              }
    
            }else{
              toast.error("Error en la api");
              changeToggle();
            
            }
           
          })


        }
      };
    
    
    return ( 
        <>
                <AgregarFormularioAlumno>
          <div className="formulario__head">
            <div>
              <TextFormulario>Crear una materia</TextFormulario>
            </div>

            <div className="delete" onClick={changeToggle}>
              <i class="fas fa-times"></i>
            </div>
          </div>

          <div className="formulario__body ">
            <form className="input-form">
              <label>Nombre</label>
              <input
                onChange={handleChangeCurso("name")}
                value={materia.name}
                type="text"
                className={
                  validateNameCurso ? "form-control is-invalid" : "form-control"
                }
                placeholder="Nombre"
              />
              {validateNameCurso && (
                <p className="text-danger my-1">El nombre es obligatorio</p>
              )}
              <label>Notas</label>
              <input
                onChange={handleChangeCurso("detalle")}
                value={materia.detalle}
                type="text"
                className={
                  validateDetalle ? "form-control is-invalid" : "form-control"
                }
                placeholder="Notas"
              />
              {validateDetalle && (
                <p className="text-danger my-1">El detalle es obligatorio</p>
              )}
              
            </form>
          </div>

          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear onClick={clickSubmitCurso}>Crear</ButtonCrear>
          </div>
        </AgregarFormularioAlumno>
        </>
     );
}
 
export default NuevaMateria;