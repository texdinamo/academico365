import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { withRouter } from "react-router-dom";
import axios from "axios";
import Moment from "react-moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Spinner } from "react-bootstrap";
import "react-tabs/style/react-tabs.css";
// import { URL } from "../../valores";
// import BootstrapTable from "react-bootstrap-table-next";
// import paginationFactory from "react-bootstrap-table2-paginator";
// import { useDatos } from "../hooks/useDatos";

const Alumno = (props) => {
  const [alumno, setAlumno] = useState();
  const [fecha,setFecha] = useState('')
  // const [alumnoId, setAlumnoId] = useState();

  useEffect(() => {
    console.log(props.location.state.fromDashboard);
    axios
      .get(
        `${process.env.REACT_APP_STUDENT_URL}/${props.location.state.fromDashboard}`
      )
      .then((res) => {
        console.log(res);
        setAlumno(res.data);
      });

      if(alumno){
        let fechaAlumno = alumno.birthDate.split('-')

      
        let nuevaFecha = `${fechaAlumno[0]}/${fechaAlumno[1]}/${fechaAlumno[2]}`
        setFecha(nuevaFecha)
      }
  }, []);


  return (
    <>
    <Navbar/>
    <div className="dashboard">
      {alumno ? (
        <Tabs>
          <TabList>
            <Tab>Informacion Personal</Tab>
            <Tab>Cursos</Tab>
            <Tab>Tutores</Tab>
            <Tab>Cuotas</Tab>
            <Tab>Datos de facturacion</Tab>
            
          </TabList>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Informacion Personal</div>

            <div className="datos-usuario d-flex">
              <div className="datos">
                <div className="d-flex">
                  <h5 className="info-descripcion">Legajo:</h5>{" "}
                  <p>{alumno.docket}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Nombre:</h5>
                  <p>{alumno.name}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Apellido:</h5>
                  <p>{alumno.surname}</p>
                </div>

                <div className="d-flex ">
                  <h5 className="info-descripcion">Dni:</h5>
                  <p>{alumno.document}</p>
                </div>

                <div className="d-flex ">
                  <h5 className="info-descripcion">Cuil:</h5>
                  <p>{alumno.cuil}</p>
                </div>


                <div className="d-flex ">
                  <h5 className="info-descripcion">Fecha de nacimiento:</h5>

                    <p>{ alumno.birthDate }</p>
                  
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Sexo:</h5>
                  <p>{alumno.gender}</p>
                </div>
              </div>

              <div className="datos ml-4">
                <div className="d-flex">
                  <h5 className="info-descripcion mb-3">Nacionalidad:</h5>
                  <p>{alumno.nationality}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion mb-3">Tipo de sangre:</h5>
                  <p>{alumno.bloodtype}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Domiciolio:</h5>
                  <p>{alumno.address}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Barrio:</h5>
                  <p>{alumno.neighborhood}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Ciudad:</h5>
                  <p>{alumno.city}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Pais:</h5>
                  <p>{alumno.country}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Barrio:</h5>
                  <p>{alumno.neighborhood}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Provincia:</h5>
                  <p>{alumno.province}</p>
                </div>
              </div>

              <div className="datos ml-4">
                <div className="d-flex">
                  <h5 className="info-descripcion">Codigo postal:</h5>
                  <p>{alumno.zipCode}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">email:</h5>
                  <p>{alumno.email}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Telefono:</h5>
                  <p>{alumno.phoneNumber}</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Razon social:</h5>
                  <p>{alumno.businessName ? alumno.businessName : 'No ingresado' }</p>
                </div>
              </div>

          

              


          </div>

          </TabPanel>

          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Cursos</div>
            <div className=" datos datos-usuario">
            <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Nombre de curso:</h5>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Año:</h5>
                </div>

            </div>

          </TabPanel>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Tutores</div>
            <div className="datos datos-usuario">
            <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Apellido:</h5>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Nombre:</h5>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Vinculo:</h5>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Telefono:</h5>
                </div>
                  <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Email:</h5>
                </div>

            </div>
          </TabPanel>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Cuotas</div>
            <div className="datos datos-usuario">


                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Curso:</h5>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Año:</h5>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Detalle:</h5>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Importe:</h5>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Saldo:</h5>
                </div>

            </div>
          </TabPanel>
          <TabPanel className="alumno">
            <div className="header-descripcion mb-4">Datos de facturacion</div>
           

            <div className="datos datos-usuario">
                <div className="d-flex">
                  <h5 className="info-descripcion mb-2">Cuit:</h5>
                  <p>{alumno.cuit ? alumno.cuit : 'No ingresado' }</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Domicilio de facturacion:</h5>
                  <p>{alumno.billingAddress ? alumno.billingAddress : 'No ingresado' }</p>
                </div>

                <div className="d-flex">
                  <h5 className="info-descripcion">Ciudad de facturacion:</h5>
                  <p>{alumno.billingCity ? alumno.billingCity :  'No ingresado'}</p>
                </div>
                <div className="d-flex">
                  <h5 className="info-descripcion">Provincia de facturacion:</h5>
                  <p>{alumno.billingProvince ? alumno.billingProvince : 'No ingresado' }</p>
                </div>
            
              <div className="d-flex">
                  <h5 className="info-descripcion">Codigo postal de facturacion:</h5>
                  <p>{alumno.billingZip ? alumno.billingZip : 'No ingresado' }</p>
                </div>
          </div>
          
          </TabPanel>
        </Tabs>
      ) : (
        <div className="d-flex justify-content-center align-items-center mt-5 spinner ">
          <Spinner animation="grow" variant="primary" size="xl" />
        </div>
      )}
      </div>
    </>
  );
};

export default withRouter(Alumno);
