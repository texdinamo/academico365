export const create = (post) => {
    return fetch(`${process.env.REACT_APP_STUDENT_URL}`, {
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        return response
    })
    .catch(err => console.log(err))
}


export const updateAlumno = (post) => {
    return fetch(`${process.env.REACT_APP_STUDENT_URL}/update`,{
        method: "POST",
        headers: {
            Accept: 'application/json',
           "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        return response
    })
    .catch(err => console.log(err))
}

export const deleteAlumnos = (post/*,token*/) => {
    return fetch(`${process.env.REACT_APP_STUDENT_URL}/remove`,{
        method: "POST",
        headers: {
            Accept: 'application/json',
            "Content-type": "application/json"
        },
        body: JSON.stringify(post)
    })
    .then(response =>{
        return response
    })
    .catch(err => console.log(err))
}

export const getAlumnos = () => {
    return fetch(`${process.env.REACT_APP_STUDENT_URL}`,{
         method: "GET"
     })
     .then(response => {
         console.log(response)
         return response.json()
     })
     .catch(err => console.log(err))
 }

 export const getAlumnoId = (id) => {
    return fetch(`${process.env.REACT_APP_STUDENT_URL}/${id}`,{
         method: "GET"
     })
     .then(response => {
         return response.json()
     })
     .catch(err => console.log(err))
 }