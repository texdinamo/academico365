import { useState, useEffect } from 'react'
import { updateAlumno } from './apiAlumnos'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import {
  AgregarFormularioAlumno,
  ButtonCrear,
  ButtonCancelar,
  TextFormulario,
  Linea,
} from "../styled-component/Navbar";
import styled from 'styled-components'



const Select = styled.select`
  width: 100%;
  color: #bbc5d5;
  border: 1px solid #bbc5d5;
  padding: 10px;
  border-radius: 4px;
`;

const ModificarAlumnos = ({ changeToggle, alumno, alumnoId }) => {
  const [validarEmail,setValidarEmail] = useState(false)
  const [values, setValues] = useState({
    docket: "",
    name: "",
    surname: "",
    document: "",
    birthDate: "",
    gender: "",
    nationality: "",
    bloodtype: "",
    studyLevel: "",
    address: "",
    neighborhood: "",
    city: "",
    province: "",
    country: "",
    zipCode: "",
    phoneNumber: "",
    email: "",
    businessName: "",
    cuit: "",
    cuil: "",
    billingAddress: "",
    billingCity: "",
    billingProvince: "",
    billingZip: "",
  });

  useEffect(() => {

    if (alumno) {


      console.log('soy values', values)
      console.log('soy alumno', alumno)

      setValues({
        id: alumno.id,
        docket: alumno.docket,
        name: alumno.name,
        surname: alumno.surname,
        document: alumno.document,
        birthDate: alumno.birthDate,
        gender: alumno.gender,
        nationality: alumno.nationality,
        bloodtype: alumno.bloodtype,
        studyLevel: alumno.studyLevel,
        address: alumno.address,
        neighborhood: alumno.neighborhood,
        city: alumno.city,
        province: alumno.province,
        country: alumno.country,
        zipCode: alumno.zipCode,
        phoneNumber: alumno.phoneNumber,
        email: alumno.email,
        businessName: alumno.businessName,
        cuit: alumno.cuit,
        billingAddress: alumno.billingAddress,
        billingCity: alumno.billingCity,
        billingProvince: alumno.billingProvince,
        billingZip: alumno.billingZip,
        cuil: alumno.cuil
      });



    }

  }, [alumno])
  const clickSubmit = () => {

    if(validarEmail === false){
      updateAlumno(values)
      .then(data => {
        console.log(data)
        if (data) {
          if (data.ok === true) {
            toast.success("Alumno modificado");
            changeToggle();
          }
          if (data.ok === false) {
            toast.error("Error al modificar");
            changeToggle();
          }
        }
        else {
          toast.error("Problema con la api");
          changeToggle();
        }
      })

    }
    
  }

  const handleChange = name => e => {
    setValues({ ...values, [name]: e.target.value })

    if(name === "email"){

      if(e.target.value.indexOf('@') !== -1 &&  e.target.value.indexOf('.') !== -1 ){
        setValidarEmail(false)
     }else{
      setValidarEmail(true)
    }

      

    }



  }


  return (
    <>
      <AgregarFormularioAlumno>
        <div className="formulario__head">
          <div>
            <TextFormulario>Modificar Alumno</TextFormulario>
          </div>

          <div className="delete" onClick={changeToggle}>
            <i class="fas fa-times"></i>
          </div>
        </div>

        <div className="formulario__body ">
          <div>
            <form className="input-form">
              <p className="mt-2 subtitulo">Datos personales</p>
              <Linea />
              <label>Nombre</label>
              <input
                onChange={handleChange("name")}
                defaultValue={alumno ? alumno.name : false}
                type="text"
                className="input"
                placeholder="Nombre"
              />
              <label>Apellido</label>
              <input
                onChange={handleChange("surname")}
                defaultValue={alumno ? alumno.surname : false}
                type="text"
                className="input"
                placeholder="Apellido"
              />
              <label>Dni</label>
              <input
                onChange={handleChange("document")}
                defaultValue={alumno ? alumno.document : false}
                type="number"
                className="input"
                placeholder="Dni"
              />
               <label>Cuil</label>
              <input
                onChange={handleChange("cuil")}
                defaultValue={alumno ? alumno.cuil : false}
                type="number"
                className="input"
                placeholder="Cuil"
              />
              <label>Fecha de nacimiento</label>
              <input
                onChange={handleChange("birthDate")}
                defaultValue={alumno ? alumno.birthDate : false}
                type="date"
                className="input"
                placeholder="Cuil"
              />

              <label>Sexo</label>
              <div className="radio d-flex justify-content-center">
                <label>Masculino</label>
                <input
                  type="radio"
                  defaultChecked={alumno ? alumno.gender === "masculino" : false}
                  name="gender"
                  value="masculino"
                  onChange={handleChange("gender")}
                />

                <label>Femenino</label>

                <input
                  type="radio"
                  defaultChecked={alumno ? alumno.gender === "femenino" : false}
                  name="gender"
                  value="femenino"
                  onChange={handleChange("gender")}
                />
              </div>

              <label>Nacionalidad</label>
              <input
                onChange={handleChange("nationality")}
                defaultValue={alumno ? alumno.nationality : false}
                type="text"
                className="input"
                placeholder="Apellido"
              />

              <label className="mb-3">Tipo de sangre</label>
              <Select
                name="bloodtype"
                defaultValue={alumno ? alumno.bloodtype : false}
                onChange={handleChange("bloodtype")}
              >
                <option value="">-- Seleccione --</option>
                <option value="No sabe">NO SABE</option>
                <option value="O NEGATIVO">O NEGATIVO</option>
                <option value="O POSITIVO"> O POSITIVO</option>

                <option value="AB NEGATIVO">AB NEGATIVO</option>
                <option value="AB POSITIVO">AB POSITIVO</option>
              </Select>

              <p className="mt-4 subtitulo">Contactos</p>
              <Linea />

              <label>Domicilio</label>
              <input
                onChange={handleChange("address")}
                defaultValue={alumno ? alumno.address : false}
                type="text"
                className="input"
                placeholder="Domicilio"
              />

              <label>Barrio</label>
              <input
                onChange={handleChange("neighborhood")}
                defaultValue={alumno ? alumno.neighborhood : false}
                type="text"
                className="input"
                placeholder="Barrio"
              />

              <label>Ciudad</label>
              <input
                onChange={handleChange("city")}
                defaultValue={alumno ? alumno.city : false}
                type="text"
                className="input"
                placeholder="Ciudad"
              />

              <label className="mb-3">Pais</label>
              <Select
                name="country"
                defaultValue={alumno ? alumno.country : false}
                onChange={handleChange("country")}
              >
                <option value="">-- Seleccione pais--</option>
                <option value="argentina">Argentina</option>
                <option value="bolivia">Bolivia</option>
                <option value="chile">Chile</option>
                <option value="peru">Perú</option>
                <option value="venezuela">Venezuela</option>
              </Select>

              <label>Provincia</label>
              <input
                onChange={handleChange("province")}
                defaultValue={alumno ? alumno.province : false}
                type="text"
                className="input"
                placeholder="Provincia"
              />

              <label>Codigo postal</label>
              <input
                onChange={handleChange("zipCode")}
                defaultValue={alumno ? alumno.zipCode : false}
                type="number"
                className="input"
                placeholder="Cod postal"
              />

              <label>Telefono</label>
              <input
                onChange={handleChange("phoneNumber")}
                defaultValue={alumno ? alumno.phoneNumber : false}
                type="number"
                className="input"
                placeholder="Telefono"
              />

              <label>Email</label>
              <input
                onChange={handleChange("email")}
                defaultValue={alumno ? alumno.email : false}
                type="email"
                className="input"
                placeholder="Email"
              />

{validarEmail && (
                <p className="text-danger my-1">El Email debe contener @ y .</p>
              )}

              <p className="mt-4 subtitulo">Escolares</p>
              <Linea />

              <label>Legajo</label>
              <input
                onChange={handleChange("docket")}
                defaultValue={alumno ? alumno.docket : false}
                type="number"
                className="input"
                placeholder="Legajo"
              />

              <label className="mb-3">Nivel de estudio</label>
              <Select
                name="studyLevel"
                defaultValue={alumno ? alumno.studyLevel : false}
                onChange={handleChange("studyLevel")}
              >
                <option value="">-- Seleccione nivel --</option>
                <option value="primario">Primario</option>
                <option value="secundario">Secundario</option>

                <option value="universitario">Universitario</option>
              </Select>

              <p className="mt-4 subtitulo">Facturacion</p>
              <Linea />


              <label>Razón social</label>
              <input
                onChange={handleChange("businessName")}
                defaultValue={alumno ? alumno.businessName : false }
                type="text"
                className="input"
                
                placeholder="Razón social"
              />

          <label>Cuit</label>
              <input
                onChange={handleChange("cuit")}
                defaultValue={alumno ? alumno.cuit : false}
                type="number"
                className="input"
                
                placeholder="Cuit"
              />
              

              <label>Domicilio de facturacion</label>
              <input
                onChange={handleChange("billingAddress")}
                defaultValue={alumno ? alumno.billingAddress : false}
                type="text"
                className="input"
                placeholder="Domicilio"
              />

              <label>Ciudad de facturacion</label>
              <input
                onChange={handleChange("billingCity")}
                defaultValue={alumno ? alumno.billingCity : false}
                type="text"
                className="input"
                placeholder="Ciudad"
              />

              <label>Provincia de facturacion</label>
              <input
                onChange={handleChange("billingProvince")}
                defaultValue={alumno ? alumno.billingProvince : false}
                type="text"
                className="input"
                placeholder="Provincia"
              />

              <label>Codigo postal</label>
              <input
                onChange={handleChange("billingZip")}
                defaultValue={alumno ? alumno.billingZip : false}
                type="text"
                className="input"
                placeholder="Cod postal"
              />
            </form>
          </div>

          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>

            <ButtonCrear onClick={clickSubmit}>Modificar</ButtonCrear>
          </div>
        </div>
      </AgregarFormularioAlumno>
    </>
  );
}

export default ModificarAlumnos;