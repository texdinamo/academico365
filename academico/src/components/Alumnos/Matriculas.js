import React, { useState, useEffect } from "react";
import axios from "axios";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import * as ReactBootstrap from "react-bootstrap";
import down from "../../img/down.png";
import styled from "styled-components";

const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
  left: 0px;
`;

const Matriculas = () => {
  const [toggle, setToggle] = useState(false);
  const [matriculas, setMatriculas] = useState([]);

  const [alumnos, setAlumnos] = useState();

  const changeToggle = () => setToggle((prevLogin) => !prevLogin);

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_STUDENT_URL}`).then((res) => {
      //// aca esta la respuesta en la consola del navegador
      console.log("respuesta de la endpoint!!", res);
      setMatriculas(res.data);
    });
  }, []);

  const columns = [
    { dataField: "name", text: "name" },
    { dataField: "surname", text: "surname" },
    { dataField: "gender", text: "gender" },
  ];

  console.log(matriculas[1]);

  return (
    <>
      <Tabs>
        <TabList>
          <Tab>
            <></>
          </Tab>
          <Tab>Cursos</Tab>
          <Tab>Materias</Tab>
          <Tab>Otros</Tab>
        </TabList>

        <TabPanel>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum
          </p>
        </TabPanel>
        <TabPanel>
          <BootstrapTable
            keyField="name"
            data={Matriculas}
            columns={columns}
            pagination={paginationFactory()}
          />
        </TabPanel>
        <TabPanel>
          <h3>Materias</h3>
        </TabPanel>
        <TabPanel>
          <h3>Otros</h3>
        </TabPanel>
      </Tabs>
    </>
  );
};

export default Matriculas;

/*
 {toggle && <AgregarShadow onClick={changeToggle}/>}
        <table>
  <thead>
    <tr>
        <th>ID</th>
      <th>Nombre</th>
      <th className="th-menor"><img src={down}/></th>
      <th>Apellido</th>
      <th>DNI</th>
      <th className="th-mayor" >Acciones</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>5969849749694</td>
      <td>Nombre</td>
      <td className="espacio-menor"></td>
      <td>Apellido</td>
      <td>37596244</td>
      <td  className="d-flex justify-content-center"> 
      <div className="btn mx-3 text-success "><i class="fas fa-pencil-alt"></i></div>
      <div className="btn mx-3"><i class="far fa-eye"></i></div>
      <div  onClick={changeToggle} className="btn mx-3 text-danger"><i class="far fa-trash-alt"></i></div>  
      </td>
    </tr>
    <tr>
      <td>5969849749694</td>
      <td>Nombre</td>
      <td className="espacio-menor"></td>
      <td>Apellido</td>
      <td>37596244</td>
      <td  className="d-flex justify-content-center"> 
      <div className="btn mx-3 text-success "><i class="fas fa-pencil-alt"></i></div>
      <div className="btn mx-3"><i class="far fa-eye"></i></div>
      <div  onClick={changeToggle} className="btn mx-3 text-danger"><i class="far fa-trash-alt"></i></div>  
        </td>
    </tr>
    <tr>
      <td>5969849749694</td>
      <td>Nombre</td>
      <td className="espacio-menor"></td>
      <td>Apellido</td>
      <td>37596244</td>
      <td  className="d-flex justify-content-center"> 
      <div className="btn mx-3 text-success "><i class="fas fa-pencil-alt"></i></div>
      <div className="btn mx-3"><i class="far fa-eye"></i></div>
      <div  onClick={changeToggle} className="btn mx-3 text-danger"><i class="far fa-trash-alt"></i></div>  
     </td>
    </tr>
    <tr>
      <td>5969849749694</td>
      <td>Nombre</td>
      <td className="espacio-menor"></td>
      <td>Apellido</td>
      <td>37596244</td>
      <td  className="d-flex justify-content-center"> 
      <div className="btn mx-3 text-success "><i class="fas fa-pencil-alt"></i></div>
      <div className="btn mx-3"><i class="far fa-eye"></i></div>
      <div  onClick={changeToggle} className="btn mx-3 text-danger"><i class="far fa-trash-alt"></i></div>  
     </td>
    </tr>
    <tr>
      <td>5969849749694</td>
      <td>Nombre</td>
      <td className="espacio-menor"></td>
      <td>Apellido</td>
      <td>37596244</td>
      <td  className="d-flex justify-content-center"> 
      <div className="btn mx-3 text-success "><i class="fas fa-pencil-alt"></i></div>
      <div className="btn mx-3"><i class="far fa-eye"></i></div>
      <div  onClick={changeToggle} className="btn mx-3 text-danger"><i class="far fa-trash-alt"></i></div>  
     </td>
    </tr>
    <tr>
      <td>5969849749694</td>
      <td>Nombre</td>
      <td className="espacio-menor"></td>
      <td>Apellido</td>
      <td>37596244</td>
      <td  className="d-flex justify-content-center"> 
      <div className="btn mx-3 text-success "><i class="fas fa-pencil-alt"></i></div>
      <div className="btn mx-3"><i class="far fa-eye"></i></div>
      <div  onClick={changeToggle} className="btn mx-3 text-danger"><i class="far fa-trash-alt"></i></div>  
    </td>
    </tr>
  </tbody>
</table>


<div className="botones">

    <div className="d-flex">
        <div className="botones__big mx-1">
            Anterior
        </div>

        <div className="botones__small noactive mx-1">
            1
        </div>

        <div className="botones__small noactive mx-1">
            2
        </div>

        <div className="botones__small active mx-1">
            3
        </div>

        <div className="botones__big ml-1">
             Siguiente
        </div>
    </div>

</div>
*/
