import React, { useState, useEffect } from "react";
import Navbar from '../estructura/Navbar'
import { Link } from "react-router-dom";
import SweetAlert from "react-bootstrap-sweetalert";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import "react-tabs/style/react-tabs.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { deleteAlumnos , getAlumnos, getAlumnoId } from './apiAlumnos'
import styled from "styled-components";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import ModificarAlumnos from '../Alumnos/ModificarAlumnos'



export const AgregarShadow = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0px;
  left: 0px;
  background-color: black;
  opacity: 0.6;
  z-index: 10000;
`;

const Alumnos = () => {
  const { SearchBar } = Search;



  const [alumnoId, setAlumnoId] = useState();
  const [confirmar, setConfirmar] = useState(false);
  const [confirmarError, setConfirmarError] = useState(false);
  const [eliminar, setEliminar] = useState(false);
  const [values, setValues] = useState();
  const [toggle, setToggle] = useState(false);
  const [alumnos, setAlumnos] = useState([]);
  
  const [alumno, setAlumno] = useState();


  const changeToggle = () => setToggle((prevLogin) => !prevLogin);

  const changeEliminar = () => setEliminar((prevLogin) => !prevLogin);

  const changeConfirmar = () => setConfirmar((prevLogin) => !prevLogin);

 

  const cambio = async () => {
    setEliminar(false);

    let post = {
        "studentId": alumnoId.id
    }

    deleteAlumnos(post).then(data => {
        
      console.log(data)

        if(data){

        
        if(data.ok === true){

          setConfirmar(true);
        
        }

        if(data.ok === false){
            setConfirmarError(true)
        }
      }else{
        setConfirmarError(true)
      }
      })
      
  }


  useEffect(() => {

  

    getAlumnos().then(data => {
      console.log(data)
      setAlumnos(data)
    })

    if(alumnoId){

      getAlumnoId(alumnoId.id).then(data => {

        setAlumno(data)
      
  })

    }

  }, [confirmar,alumnoId]);





  const columns = [
    {
      dataField: "docket",
      text: "Legajo",
      sort: true,
      searchable: true,

      sortCaret: (order, column) => {
        if (!order)
          return (
            <span className="inicio-arrow">
              {" "}
              <i className="fas fa-caret-down"></i> <i className="fas fa-caret-up"></i>
            </span>
          );
        else if (order === "asc")
          return (
            <span>
              {" "}
              <i className="fas fa-caret-down"></i>{" "}
              <font color="red">
                <i className="fas fa-caret-up color"></i>
              </font>
            </span>
          );
        else if (order === "desc")
          return (
            <span>
              {" "}
              <font color="red">
                {" "}
                <i className="fas fa-caret-down color"></i>
              </font>{" "}
              <i className="fas fa-caret-up"></i>
            </span>
          );
        return null;
      },
    },
    {
      dataField: "name",
      text: "Nombre",
      sort: true,
      searchable: true,
      sortCaret: (order, column) => {
        if (!order)
          return (
            <span className="inicio-arrow">
              {" "}
              <i className="fas fa-caret-down"></i> <i className="fas fa-caret-up"></i>
            </span>
          );
        else if (order === "asc")
          return (
            <span>
              {" "}
              <i className="fas fa-caret-down"></i>{" "}
              <font color="red">
                <i className="fas fa-caret-up color"></i>
              </font>
            </span>
          );
        else if (order === "desc")
          return (
            <span>
              {" "}
              <font color="red">
                {" "}
                <i className="fas fa-caret-down color"></i>
              </font>{" "}
              <i className="fas fa-caret-up"></i>
            </span>
          );
        return null;
      },
    },

    {
      dataField: "surname",
      text: "Apellido",
      sort: true,
      searchable: true,
      sortCaret: (order, column) => {
        if (!order)
          return (
            <span className="inicio-arrow">
              {" "}
              <i className="fas fa-caret-down"></i> <i className="fas fa-caret-up"></i>
            </span>
          );
        else if (order === "asc")
          return (
            <span>
              {" "}
              <i className="fas fa-caret-down"></i>{" "}
              <font color="red">
                <i className="fas fa-caret-up color"></i>
              </font>
            </span>
          );
        else if (order === "desc")
          return (
            <span>
              {" "}
              <font color="red">
                {" "}
                <i className="fas fa-caret-down color"></i>
              </font>{" "}
              <i className="fas fa-caret-up"></i>
            </span>
          );
        return null;
      },
    },
    {
      dataField: "document",
      text: "Dni",
      sort: true,
      searchable: true,
      sortCaret: (order, column) => {
        if (!order)
          return (
            <span className="inicio-arrow">
              {" "}
              <i className="fas fa-caret-down"></i> <i className="fas fa-caret-up"></i>
            </span>
          );
        else if (order === "asc")
          return (
            <span>
              {" "}
              <i className="fas fa-caret-down"></i>{" "}
              <font color="red">
                <i className="fas fa-caret-up color"></i>
              </font>
            </span>
          );
        else if (order === "desc")
          return (
            <span>
              {" "}
              <font color="red">
                {" "}
                <i className="fas fa-caret-down color"></i>
              </font>{" "}
              <i className="fas fa-caret-up"></i>
            </span>
          );
        return null;
      },
    },
    {
      dataField: "link",
      text: "Accion",
      formatter: (rowContent, row) => {
        return (
          <>
            <i
              className="fas fa-pencil-alt mr-3 text-success"
              onClick={changeToggle}
            ></i>

            <Link
              to={{
                pathname: "/alumnos/alumno",
                state: { fromDashboard: row.id },
              }}
              className="link-i"
            >
              <i className="far fa-eye"></i>
            </Link>

            <i
              className="far fa-trash-alt ml-3 text-danger"
              onClick={changeEliminar}

            ></i>
          </>
        );
      },
    },
  ];

  const rowEvents = {
    onClick: (e, row, rowIndex) => {
      console.log(row,e)
      setAlumnoId(row);
    },
    onMouseEnter: (e, row, rowIndex) => {
      setAlumnoId(row);
      console.log(row,e)
    },
  };

  const options = {
    paginationSize: 3,
    pageStartIndex: 0,
    // alwaysShowAllBtns: true, // Always show next and previous button
    // withFirstAndLast: false, // Hide the going to First and Last page button
    // hideSizePerPage: true, // Hide the sizePerPage dropdown always
    // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
    withFirstAndLast: false,
    firstPageText: "First",
    prePageText: "Anterior",
    nextPageText: "Siguiente",

    showTotal: false,
    disablePageTitle: true,
  };

  return (
<>

<Navbar  />
    <div className="dashboard">
      <ToastContainer />
      <ToolkitProvider keyField="id" data={alumnos} columns={columns} search>
        {(props) => (
          <div>
            <div className="barra">
              <i className="fas fa-search ml-3 mt-2"></i>
              <SearchBar placeholder="Buscar Alumno" {...props.searchProps} />
            </div>
            <BootstrapTable
              rowEvents={rowEvents}
              {...props.baseProps}
              pagination={paginationFactory(options)}
            />
          </div>
        )}
      </ToolkitProvider>

      {toggle && <AgregarShadow onClick={changeToggle} />}

      {eliminar && (
        <SweetAlert
          primary
          showCancel
          cancelBtnBsStyle="secondary"
          style={{
            fontSize: "12px",
            fontWeight: "700",
            width: "500px",
            height: "240px",
          }}
          onConfirm={() => cambio()}
          onCancel={() => setEliminar(false)}
          confirmBtnText="Aceptar"
          title="Estas seguro de realizar esta accion?"
          focusCancelBtn
        >
          <p style={{ fontSize: "14px", margin: "5px" }}>
            Esta apunto de realizar una accion irreversible
          </p>

          <p style={{ fontSize: "14px", marginBottom: "20px" }}>
            Confirma tu accion
          </p>
        </SweetAlert>
      )}

      {confirmar && (
        <SweetAlert
          success
          title="Eliminado"
          onConfirm={() => changeConfirmar()}
        ></SweetAlert>
      )}

{confirmarError && (
        <SweetAlert
          danger
          title="Error"
          onConfirm={() => setConfirmarError(false)}
        ></SweetAlert>
      )}

{toggle &&  (
      <ModificarAlumnos changeToggle={changeToggle} alumnoId={alumnoId}  alumno={alumno} />
      )}
      
    </div>
    </>
  );
};

export default Alumnos;
