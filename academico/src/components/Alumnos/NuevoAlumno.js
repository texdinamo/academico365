import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import {
  // AgregarShadow,
  // AgregarFormulario,
  // ButtonAgregar,
  ButtonCrear,
  ButtonCancelar,
  Select,
  Linea,
  TextFormulario,
  // TextInfo,
  // ContainerMenu,
  // SubTextInfo,
  AgregarFormularioAlumno,
} from "../styled-component/Navbar";
import { create } from '../Alumnos/apiAlumnos.js'
// import Alumnos from './Alumnos'


const NuevoAlumno = ({ changeToggle }) => {
  const [disabledButton, setDisabledButton] = useState(false)
  const [validateNombre, setValidateNombre] = useState(false);
  const [validateSurname, setValidateSurname] = useState(false);
  const [validateDocument, setValidateDocument] = useState(false);
  const [validateBirthDate, setValidateBirthDate] = useState(false);
  const [validateGender, setValidateGender] = useState(false);
  const [validateNationality, setValidateNationality] = useState(false);
  const [validateBloodtype, setValidateBloodtype] = useState(false);
  const [validateStudylevel, setValidateStudylevel] = useState(false);
  const [validateAddress, setValidateAddress] = useState(false);
  const [validateNeighborhood, setValidateNeighborhood] = useState(false);
  const [validateCity, setValidateCity] = useState(false);
  const [validateProvince, setValidateProvince] = useState(false);
  const [validateCountry, setValidateCountry] = useState(false);
  const [validateZipcode, setValidateZipcode] = useState(false);
  const [validatePhonenumber, setValidatePhonenumber] = useState(false);
  const [validateEmail, setValidateEmail] = useState(false);
  // const [validateBusinessname, setValidateBusinessname] = useState(false);
  const [validateDocket, setValidateDocket] = useState(false);
  // const [validateCuit, setValidateCuit] = useState(false);
  const [validateBillingaddress, setValidateBillingaddress] = useState(false);
  const [validateBillingcity, setValidateBillingcity] = useState(false);
  const [validateBillingprovince, setValidateBillingprovince] = useState(false);
  const [validateBillingzip, setValidateBillingzip] = useState(false);
  const [validarEmail,setValidarEmail] = useState(false)
  const [validateCuil,setValidateCuil] = useState(false)
  const [values, setValues] = useState({
    docket: "",
    name: "",
    surname: "",
    document: "",
    birthDate: "",
    gender: "",
    nationality: "",
    bloodtype: "",
    studyLevel: "",
    address: "",
    neighborhood: "",
    city: "",
    province: "",
    country: "",
    zipCode: "",
    phoneNumber: "",
    email: "",
    businessName: "",
    cuit: "",
    cuil: "",
    billingAddress: "",
    billingCity: "",
    billingProvince: "",
    billingZip: "",
  });
  useEffect(() => {

  }, [])
  console.log(values)

  const handleChange = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value });
    if (name === "name") {
      setValidateNombre(false);
    }
    if (name === "surname") {
      setValidateSurname(false);
    }
    if (name === "document") {
      setValidateDocument(false);
    }
    if (name === "birthDate") {
      setValidateBirthDate(false);
    }
    if (name === "gender") {
      setValidateGender(false);
    }
    if (name === "nationality") {
      setValidateNationality(false);
    }
    if (name === "bloodtype") {
      setValidateBloodtype(false);
    }
    if (name === "address") {
      setValidateAddress(false);
    }
    if (name === "neighborhood") {
      setValidateNeighborhood(false);
    }
    if (name === "city") {
      setValidateCity(false);
    }
    if (name === "province") {
      setValidateProvince(false);
    }
    if (name === "country") {
      setValidateCountry(false);
    }
    if (name === "zipCode") {
      setValidateZipcode(false);
    }
    if (name === "phoneNumber") {
      setValidatePhonenumber(false);
    }
    if (name === "email") {


       if(event.target.value.indexOf('@') !== -1 &&  event.target.value.indexOf('.') !== -1 ){
          setValidarEmail(false)
       }else{
        setValidarEmail(true)
      }
  
       setValidateEmail(false);
    
    
    }
    if (name === "docket") {
      setValidateDocket(false);
    }
    if (name === "studyLevel") {
      setValidateStudylevel(false);
    }
    if (name === "billingAddress") {
      setValidateBillingaddress(false);
    }
    if (name === "billingCity") {
      setValidateBillingcity(false);
    }
    if (name === "billingProvince") {
      setValidateBillingprovince(false);
    }
    if (name === "billingZip") {
      setValidateBillingzip(false);
    }
    if (name === "cuil") {
      setValidateCuil(false);



    }
  };

  const clickSubmit = (event) => {
    event.preventDefault();
    if (!values.name) {
      setValidateNombre(true);
    }
    if (!values.surname) {
      setValidateSurname(true);
    }
    if (!values.document) {
      setValidateDocument(true);
    }
    if (!values.birthDate) {
      setValidateBirthDate(true);
    }
    if (!values.nationality) {
      setValidateNationality(true);
    }
    if (!values.bloodtype) {
      setValidateBloodtype(true);
    }
    if (!values.address) {
      setValidateAddress(true);
    }
    if (!values.neighborhood) {
      setValidateNeighborhood(true);
    }
    if (!values.city) {
      setValidateCity(true);
    }
    if (!values.province) {
      setValidateProvince(true);
    }
    if (!values.country) {
      setValidateCountry(true);
    }
    if (!values.zipCode) {
      setValidateZipcode(true);
    }
    if (!values.phoneNumber) {
      setValidatePhonenumber(true);
    }
    if (!values.email) {
      setValidateEmail(true);
    }
    if (!values.docket) {
      setValidateDocket(true);
    }
    if (!values.studyLevel) {
      setValidateStudylevel(true);
    }
    if (!values.billingAddress) {
      setValidateBillingaddress(true);
    }
    if (!values.billingCity) {
      setValidateBillingcity(true);
    }
    if (!values.billingProvince) {
      setValidateBillingprovince(true);
    }
    if (!values.billingZip) {
      setValidateBillingzip(true);
    }
    if (!values.gender) {
      setValidateGender(true);
    }

    if (!values.cuil) {
      setValidateCuil(true);
    }


    if (
      values.address &&
      values.gender &&
      values.studyLevel &&
      values.docket &&
      values.email &&
      values.phoneNumber &&
      values.zipCode &&
      values.country &&
      values.province &&
      values.city &&
      values.neighborhood &&
      values.bloodtype &&
      values.nationality &&
      values.birthDate &&
      values.name &&
      values.surname &&
      values.document &&
      values.cuil &&
      validarEmail === false
    ) {

      setDisabledButton(true)
      let val = {
        "address": values.address,
        "billingAddress": values.billingAddress,
        "billingCity": values.billingCity,
        "billingProvince": values.billingProvince,
        "billingZip": values.billingZip,
        "birthDate": values.birthDate,
        "bloodType": values.bloodtype,
        "businessName": values.businessName,
        "city": values.city,
        "country": values.country,
        "cuil": values.cuil,
        "cuit":  isNaN(parseInt(values.cuit,10)) ? 0 : parseInt(values.cuit,10)  ,
        "docket": values.docket,
        "document": values.document,
        "email": values.email,
        "gender": values.gender,
        "nacionality": values.nationality,
        "name": values.name,
        "neighborhood": values.neighborhood,
        "phoneNumber": values.phoneNumber,
        "province": values.province,
        "studyLevel": values.studyLevel,
        "surname": values.surname,
        "zipCode": values.zipCode
      }

      console.log('vall',val)
      create(val)
        .then(data => {
          if (data.ok === true) {
            toast.success("Alumno añadido correctamente!");
            changeToggle();
          }
          if (data.ok === false) {
            toast.error("Ocurrió un error al añadir el alumno. Intenta más tarde");
            changeToggle();
          }
        })
    }
  };

  return (
    <>
      <AgregarFormularioAlumno>
        <div className="formulario__head">
          <div>
            <TextFormulario>Añadir nuevo Alumno</TextFormulario>
          </div>
          <div className="delete" onClick={changeToggle}>
            <i class="fas fa-times"></i>
          </div>
        </div>
        <div className="formulario__body ">
          <div>
            <form className="input-form">
              <p className="subtitulo">Datos personales</p>
              <Linea />
              <label>Nombre</label>
              <input
                onChange={handleChange("name")}
                value={values.name}
                type="text"
                className={
                  validateNombre ? "form-control is-invalid" : "form-control"
                }
                placeholder="Nombre"
              />
              {validateNombre && (
                <p className="text-danger my-1">El nombre es obligatorio</p>
              )}
              <label>Apellido</label>
              <input
                onChange={handleChange("surname")}
                value={values.surname}
                type="text"
                className={
                  validateSurname ? "form-control is-invalid" : "form-control"
                }
                placeholder="Apellido"
              />
              {validateSurname && (
                <p className="text-danger my-1">El apellido es obligatorio</p>
              )}
              <label>Dni</label>
              <input
                onChange={handleChange("document")}
                value={values.document}
                type="number"
                className={
                  validateDocument
                    ? "form-control is-invalid"
                    : "form-control"
                }
                placeholder="DNI"
              />



              
<label>Cuil</label>
<input
                onChange={handleChange("cuil")}
                value={values.cuil}
                type="number"
                className={
                  validateCuil
                    ? "form-control is-invalid"
                    : "form-control"
                }
                placeholder="CUIL"
              />

{validateCuil && (
                <p className="text-danger my-1">El Cuil es obligatorio</p>
              )}


              <label>Fecha de nacimiento</label>
              <input
                onChange={handleChange("birthDate")}
                value={values.birthDate}
                type="date"
                className={
                  validateBirthDate
                    ? "form-control is-invalid"
                    : "form-control"
                }
                placeholder="Fecha de nacimiento"
              />
              {validateBirthDate && (
                <p className="text-danger my-1">
                  La fecha de nacimineto es obligatoria
                </p>
              )}

              <label>Sexo</label>
              <div className="radio d-flex justify-content-center">
                <label>Masculino</label>
                <input
                  type="radio"
                  name="gender"
                  value="masculino"
                  checked={values.gender === "masculino"}
                  onChange={handleChange("gender")}
                />
                <label>Femenino</label>
                <input
                  type="radio"
                  name="gender"
                  value="femenino"
                  checked={values.gender === "femenino"}
                  onChange={handleChange("gender")}
                />
              </div>
              {validateGender && (
                <p className="text-danger my-1">El sexo es obligatorio</p>
              )}
              <label>Nacionalidad</label>
              <input
                onChange={handleChange("nationality")}
                value={values.nationality}
                type="text"
                className={
                  validateNationality
                    ? "form-control is-invalid"
                    : "form-control"
                }
                placeholder="Nacionalidad"
              />
              {validateNationality && (
                <p className="text-danger my-1">
                  La nacionalidad es obligatoria
                </p>
              )}
              <label className="mb-3">Tipo de sangre</label>
              <Select
                name="bloodtype"
                value={values.bloodtype}
                onChange={handleChange("bloodtype")}
                style={{ borderColor: validateBloodtype ? "red" : "" }}
              >
                <option value="">Seleccione tipo de sangre</option>
                <option value="No sabe">No sabe</option>
                <option value="O NEGATIVO">O Negativo</option>
                <option value="O POSITIVO">O Positivo</option>
                <option value="A NEGATIVO">A Negativo</option>
                <option value="A POSITIVO">A Positivo</option>
                <option value="B NEGATIVO">B Negativo</option>
                <option value="B POSITIVO">B Positivo</option>
                <option value="AB NEGATIVO">AB Negativo</option>
                <option value="AB POSITIVO">AB Positivo</option>
              </Select>
              {validateBloodtype && (
                <p className="text-danger my-1">
                  El tipo de sangre es obligatorio
                </p>
              )}
              <p className="mt-4 subtitulo">Datos de contacto</p>
              <Linea />
              <label>Domicilio</label>
              <input
                onChange={handleChange("address")}
                value={values.address}
                type="text"
                className={
                  validateAddress ? "form-control is-invalid" : "form-control"
                }
                placeholder="Domicilio"
              />
              {validateAddress && (
                <p className="text-danger my-1">
                  La direccion es obligatoria
                </p>
              )}
              <label>Barrio</label>
              <input
                onChange={handleChange("neighborhood")}
                value={values.neighborhood}
                type="text"
                className={
                  validateNeighborhood
                    ? "form-control is-invalid"
                    : "form-control"
                }
                placeholder="Barrio"
              />
              {validateNeighborhood && (
                <p className="text-danger my-1">El barrio es obligatorio</p>
              )}
              <label>Ciudad</label>
              <input
                onChange={handleChange("city")}
                value={values.city}
                type="text"
                className={
                  validateCity ? "form-control is-invalid" : "form-control"
                }
                placeholder="Ciudad"
              />
              {validateCity && (
                <p className="text-danger my-1">La ciudad es obligatoria</p>
              )}
              <label className="mb-3">Pais</label>
              <Select
                name="country"
                value={values.country}
                onChange={handleChange("country")}
                style={{ borderColor: validateCountry ? "red" : "" }}
              >
                <option value="">Seleccione país</option>
                <option value="argentina">Argentina</option>
                <option value="bolivia">Bolivia</option>
                <option value="chile">Chile</option>
                <option value="peru">Perú</option>
                <option value="venezuela">Venezuela</option>
              </Select>
              {validateCountry && (
                <p className="text-danger my-1">El pais es obligatorio</p>
              )}
              <label>Provincia</label>
              <input
                onChange={handleChange("province")}
                value={values.province}
                type="text"
                className={
                  validateProvince
                    ? "form-control is-invalid"
                    : "form-control"
                }
                placeholder="Provincia"
              />
              {validateProvince && (
                <p className="text-danger my-1">La provincia obligatoria</p>
              )}
              <label>Código postal</label>
              <input
                onChange={handleChange("zipCode")}
                value={values.zipCode}
                type="number"
                className={
                  validateZipcode ? "form-control is-invalid" : "form-control"
                }
                placeholder="Código postal"
              />
              {validateZipcode && (
                <p className="text-danger my-1">
                  El código postal es obligatorio
                </p>
              )}
              <label>Teléfono</label>
              <input
                onChange={handleChange("phoneNumber")}
                value={values.phoneNumber}
                type="number"
                className={
                  validatePhonenumber
                    ? "form-control is-invalid"
                    : "form-control"
                }
                placeholder="Teléfono"
              />
              {validatePhonenumber && (
                <p className="text-danger my-1">El télefono es obligatorio</p>
              )}

              <label>Email</label>
              <input
                onChange={handleChange("email")}
                value={values.email}
                type="email"
                className={
                  validateEmail ? "form-control is-invalid" : "form-control"
                }
                placeholder="Email"
              />
              {validateEmail && (
                <p className="text-danger my-1">El Email es obligatorio</p>
              )}

        {validarEmail && (
                <p className="text-danger my-1">El Email debe contener @ y .</p>
              )}


              <p className="mt-4 subtitulo">Datos escolares</p>
              <Linea />

              <label>Legajo</label>
              <input
                onChange={handleChange("docket")}
                value={values.docket}
                type="number"
                className={
                  validateDocket ? "form-control is-invalid" : "form-control"
                }
                placeholder="Legajo"
              />
              {validateDocket && (
                <p className="text-danger my-1">El legajo es obligatorio</p>
              )}
              <label className="mb-3">Nivel de estudios</label>
              <Select
                name="studyLevel"
                value={values.studyLevel}
                onChange={handleChange("studyLevel")}
                style={{ borderColor: validateStudylevel ? "red" : "" }}
              >
                <option value="">Seleccione nivel</option>
                <option value="primario">Primario</option>
                <option value="secundario">Secundario</option>
                <option value="universitario">Universitario</option>
              </Select>
              {validateStudylevel && (
                <p className="text-danger my-1">
                  El Nivel de estudios es obligatorio
                </p>
              )}
              <p className="mt-4 subtitulo">Datos de Facturación</p>
              <Linea />

              <label>Razón social</label>
              <input
                onChange={handleChange("businessName")}
                value={values.businessName}
                type="text"
                className="form-control"
                
                placeholder="Razón social"
              />

          <label>Cuit</label>
              <input
                onChange={handleChange("cuit")}
                value={values.cuit}
                type="number"
                className="form-control"
                
                placeholder="Cuit"
              />
              

              <label>Domicilio de facturación</label>
              <input
                onChange={handleChange("billingAddress")}
                value={values.billingAddress}
                type="text"
                className="form-control"
                
                placeholder="Domicilio de facturación"
              />
             
              <label>Ciudad de facturación</label>
              <input
                onChange={handleChange("billingCity")}
                value={values.billingCity}
                type="text"
                className="form-control"
                placeholder="Ciudad de facturación"
              />
              
              <label>Provincia de facturación</label>
              <input
                onChange={handleChange("billingProvince")}
                value={values.billingProvince}
                type="text"
                className="form-control"
                placeholder="Provincia de facturación"
              />
              
              <label>Código postal de facturación</label>
              <input
                onChange={handleChange("billingZip")}
                value={values.billingZip}
                type="text"
                className="form-control"
                placeholder="Código postal de facturación"
              />

              
            </form>
          </div>
          <div className="formulario__footer">
            <ButtonCancelar onClick={changeToggle}>Cancelar</ButtonCancelar>
            <ButtonCrear disabled={disabledButton} onClick={clickSubmit}>Añadir nuevo alumno</ButtonCrear>
          </div>
        </div>
      </AgregarFormularioAlumno>
    </>
  );
}

export default NuevoAlumno;